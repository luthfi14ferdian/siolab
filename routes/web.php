<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/testUpload', 'InstruksiKerjaController@indexUpload')->name('file.index');

Route::get('/testUploadC', 'InstruksiKerjaController@createUpload');

Route::get('/testUploadD', 'InstruksiKerjaController@destroyUpload')->name('file.destroy');
Route::get('/testUploadE', 'InstruksiKerjaController@destroyUpload')->name('file.edit');

Route::post('/testUpload-', 'InstruksiKerjaController@storeUpload')->name('file.store');
