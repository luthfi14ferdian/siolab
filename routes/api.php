<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('user', function (Request $request) {
    return $request->user();
});

# Register and login

Route::post('register', 'UserController@register');
Route::post('login', 'LoginController@login');

Route::group(['middleware' => ['cors', 'auth:api']], function () {
    Route::get('daftar-user', 'ProjectController@getUser');

    # Management for admin
    Route::group(['prefix' => 'adminPanel'], function () {
        Route::get('list-user', 'UserController@index');
        Route::get('detail-user', 'UserController@getDetailUser');
        Route::put('edit-user', 'UserController@editUserAdmin');
        Route::put('approve-user', 'UserController@approveUser');
        Route::put('change-password-admin', 'UserController@changePasswordAdmin');
    });

    # Project management
    Route::group(['prefix' => 'project'], function () {
        Route::get('', 'ProjectController@getListJenisProject');
        Route::get('detail/{id}', 'ProjectController@getDetailProject');
        Route::post('tambah-jenis-proyek', 'ProjectController@createJenisProject');
        Route::post('tambah-proyek', 'ProjectController@createProject');
        Route::get('list-project', 'ProjectController@getListProject');
        Route::put('pilih-project', 'ProjectController@chooseProject');
    });

    # User management
    Route::get('my-profile', 'UserController@getUserLogin');
    Route::put('change-password', 'UserController@changePassword');
    Route::get('logouts', 'LoginController@logout');
    Route::put('edit-user', 'UserController@editUser');

    #Info Page
    Route::get('info', 'InfoPageController@index');
    Route::get('info-detail', 'InfoPageController@detail');
    Route::get('info-kategori', 'InfoPageController@kategori');
    Route::post('info-add', 'InfoPageController@store');
    Route::put('info-edit', 'InfoPageController@edit');
    Route::delete('info-delete', 'InfoPageController@delete');

    # Material and produk
    Route::get('getProduk', 'MaterialController@getProduk');

    # Nota antar divisi
    Route::group(['prefix' => 'nota-antar-divisi'], function () {
        Route::get('get-all-nad', 'NadController@index');
        Route::post('buat-nad', 'NadController@createNad');
        Route::put('ubah-nad', 'NadController@editNad');
        Route::get('get-nad', 'NadController@getNad');
        Route::delete('delete-nad', 'NadController@deleteNad');
        Route::put('approval-nad', 'NadController@approvalNad');

        #dokumen nad
        Route::post('add-dokumen', 'DokumenNadController@createDocNad');
        Route::get('get-all-nad-dokumen', 'DokumenNadController@index');
        Route::post('edit-dokumen', 'DokumenNadController@editDocNad');
        Route::delete('delete-dokumen', 'DokumenNadController@deleteDocNad');
    });

    # Dokumen Serah Terima
    Route::group(['prefix' => 'dokumen-serah-terima'], function () {
        Route::post('create', 'DokumenSerahTerimaController@createDst');
        Route::get('all', 'DokumenSerahTerimaController@index');
        Route::get('detail/{id}', 'DokumenSerahTerimaController@getDetailDst');
        Route::put('detail/{id}', 'DokumenSerahTerimaController@editDst');
        Route::delete('detail/{id}', 'DokumenSerahTerimaController@deleteDst');
        Route::put('detail/{id}/accept', 'DokumenSerahTerimaController@acceptDst');
    });

    Route::group(['prefix' => 'desain/output', "namespace" => "Desain\Output"], function () {

        Route::group(['prefix' => 'fit-3d'], function () {
            Route::get('', 'Fit3DController@index');
            Route::get('detail', 'Fit3DController@show');
            Route::post('create', 'Fit3DController@store');
            Route::put('update', 'Fit3DController@edit');
        });

        Route::group(['prefix' => 'drawing'], function () {
            Route::get('', 'DrawingController@index');
            Route::get('detail', 'DrawingController@show');
            Route::post('create', 'DrawingController@store');
            Route::put('update', 'DrawingController@edit');
        });

        Route::group(['prefix' => 'spesifikasi-produk'], function () {
            Route::get('', 'SpesifikasiProdukController@index');
            Route::get('detail', 'SpesifikasiProdukController@show');
            Route::post('create', 'SpesifikasiProdukController@store');
            Route::put('update', 'SpesifikasiProdukController@edit');
        });
    });

    Route::group(['prefix' => 'desain/input', "namespace" => "Desain\Input"], function () {

        Route::group(['prefix' => 'hasil-wawancara'], function () {
            Route::get('', 'HasilWawancaraController@index');
            Route::get('detail', 'HasilWawancaraController@detail');
            Route::post('create', 'HasilWawancaraController@create');
            Route::put('update', 'HasilWawancaraController@update');
            Route::put('delete', 'HasilWawancaraController@delete');
            Route::get('download-list', 'HasilWawancaraController@export_excel');
        });

        Route::group(['prefix' => 'kebutuhan-masalah-wawancara'], function () {
            Route::get('', 'KebutuhanMasalahController@index');
            Route::post('create', 'KebutuhanMasalahController@create');
            Route::put('update', 'KebutuhanMasalahController@update');
            Route::delete('delete', 'KebutuhanMasalahController@delete');
            Route::put('confirm', 'KebutuhanMasalahController@confirm');
        });

        Route::group(['prefix' => 'studi-literatur'], function () {
            Route::get('', 'StudiLiteraturController@index');
            Route::get('detail', 'StudiLiteraturController@show');
            Route::post('create', 'StudiLiteraturController@store');
            Route::put('update', 'StudiLiteraturController@edit');
            Route::get('download-list', 'StudiLiteraturController@export_excel');
        });

        Route::group(['prefix' => 'produk-benchmark'], function () {
            Route::get('', 'ProdukBenchmarkController@index');
            Route::get('detail', 'ProdukBenchmarkController@show');
            Route::post('create', 'ProdukBenchmarkController@store');
            Route::put('update', 'ProdukBenchmarkController@edit');
            Route::get('download-list', 'ProdukBenchmarkController@export_excel');
        });
    });

    Route::group(['prefix' => 'desain/proses/dokumen-benchmarking', "namespace" => "Desain\Proses"], function () {
        Route::get('', 'BenchmarkController@index');
        Route::get('detail', 'BenchmarkController@show');
        Route::post('create', 'BenchmarkController@store');
        Route::put('update', 'BenchmarkController@edit');
        Route::get('download-list', 'BenchmarkController@export_excel');
    });

    Route::group(['prefix' => 'desain/verification', "namespace" => "Desain\Verification"], function () {
        Route::group(['prefix' => 'pengujian-raw-material'], function () {
            Route::get('', 'PengujianRawMaterialController@index');
            Route::get('detail', 'PengujianRawMaterialController@show');
            Route::post('create', 'PengujianRawMaterialController@store');
            Route::put('update', 'PengujianRawMaterialController@edit');
        });

        Route::group(['prefix' => 'pengukuran-prototype'], function () {
            Route::get('', 'PengukuranPrototipeController@index');
            Route::get('detail', 'PengukuranPrototipeController@show');
            Route::post('create', 'PengukuranPrototipeController@store');
            Route::put('update', 'PengukuranPrototipeController@edit');
        });

        Route::group(['prefix' => 'pengujian-prototype'], function () {
            Route::get('', 'PengujianPrototipeController@index');
            Route::get('detail', 'PengujianPrototipeController@show');
            Route::post('create', 'PengujianPrototipeController@store');
            Route::put('update', 'PengujianPrototipeController@edit');
        });

        Route::group(['prefix' => 'pengujian-biocompatibility'], function () {
            Route::get('', 'PengujianBiocompatibilityController@index');
            Route::get('detail', 'PengujianBiocompatibilityController@show');
            Route::post('create', 'PengujianBiocompatibilityController@store');
            Route::put('update', 'PengujianBiocompatibilityController@edit');
        });
    });

    Route::group(['prefix' => 'desain/validation', "namespace" => "Desain\Validation"], function () {
        Route::group(['prefix' => 'user-trial'], function () {
            Route::get('', 'UserTrialController@index');
            Route::get('detail', 'UserTrialController@show');
            Route::post('create', 'UserTrialController@store');
            Route::put('update', 'UserTrialController@edit');
        });

        Route::group(['prefix' => 'uji-klinis'], function () {
            Route::get('', 'UjiKlinisController@index');
            Route::get('detail', 'UjiKlinisController@show');
            Route::post('create', 'UjiKlinisController@store');
            Route::put('update', 'UjiKlinisController@edit');
        });
    });

    Route::group(['prefix' => 'desain/review', "namespace" => "Desain\Review"], function () {
        Route::group(['prefix' => 'usulan-revisi'], function () {
            Route::get('', 'UsulanRevisiController@index');
            Route::get('detail', 'UsulanRevisiController@detail');
            Route::post('create', 'UsulanRevisiController@create');
            Route::put('update', 'UsulanRevisiController@update');
            Route::delete('delete', 'UsulanRevisiController@delete');
            Route::put('approve', 'UsulanRevisiController@approve');
        });

        Route::group(['prefix' => 'poin-usulan-revisi'], function () {
            Route::get('', 'UsulanRevisiItemController@index');
            Route::post('create', 'UsulanRevisiItemController@create');
            Route::put('update', 'UsulanRevisiItemController@update');
            Route::delete('delete', 'UsulanRevisiItemController@delete');
        });

        Route::group(['prefix' => 'form-checklist'], function () {
            Route::get('', 'FormChecklistController@index');
            Route::get('detail', 'FormChecklistController@show');
        });
    });

    Route::group(['prefix' => 'desain/planning', "namespace" => "Desain\Planning"], function () {
        Route::group(['prefix' => 'dev'], function () {
            Route::get('', 'DesainDevPlanningController@index');
            Route::post('add-or-edit', 'DesainDevPlanningController@editOrCreate');
            Route::put('confirm', 'DesainDevPlanningController@confirm');
        });

        Route::group(['prefix' => 'time'], function () {
            Route::get('', 'TimePlanningController@index');
            Route::post('add-or-edit', 'TimePlanningController@editOrCreate');
        });
    });

    #Biocompatibility
    Route::group(['prefix' => 'biocompatibility', "namespace" => "Biocompatibility"], function () {
        #SUBCONTRACTOR
        Route::get('list-subcontractor', 'SubcontractorController@index');
        Route::get('get-subcontractor', 'SubcontractorController@getSubcontractor');

        #PENILAIAN SUBCONTRACTOR
        Route::get('get-penilaian-subcontractor', 'PenilaianSubcontractorController@getPenilaianSubcontractor');

        #DAFTAR PENGUJIAN
        Route::get('daftar-pengujian', 'DaftarPengujianController@index');
        Route::get('detail-pengujian', 'DaftarPengujianController@detailPengujian');

        #DOKUMEN PENGUJIAN
        Route::get('daftar-dokumen-pengujian', 'DokumenPengujianController@index');

        #GAMBAR PENGUJIAN
        Route::get('daftar-gambar-pengujian', 'GambarPengujianController@index');

        #TAHAPAN PENGUJIAN
        Route::get('daftar-tahapan-pengujian', 'TahapanPengujianController@index');

        #PEMROSESAN PENGUJIAN
        Route::get('get-pemrosesan', 'PemrosesanBiocompController@getPemrosesan');
        Route::get('get-detail-pemrosesan', 'PemrosesanBiocompController@getDetailPemrosesan');

        Route::group(["middleware" => ["check-divisi:biocompatibility"]], function () {
            #SUBCONTRACTOR
            Route::post('add-subcontractor', 'SubcontractorController@addSubcontractor');
            Route::put('edit-subcontractor', 'SubcontractorController@editSubcontractor');
            Route::delete('delete-subcontractor', 'SubcontractorController@deleteSubcontractor');
            Route::put('approve-subcontractor', 'SubcontractorController@approveSubcontractor')
                ->middleware(["check-role:koordinator_divisi"]);;

            #PENILAIAN SUBCONTRACTOR
            Route::post('add-penilaian-subcontractor', 'PenilaianSubcontractorController@addPenilaianSubcontractor');
            Route::put('edit-penilaian-subcontractor', 'PenilaianSubcontractorController@editPenilaianSubcontractor');
            Route::delete('delete-penilaian-subcontractor', 'PenilaianSubcontractorController@deletePenilaianSubcontractor');
            Route::put('finish-penilaian-subcontractor', 'PenilaianSubcontractorController@finishPenilaianSubcontractor');

            #PELAYANAN SUBCONTRACTOR
            Route::put('set-pelayanan-subcontractor', 'PenilaianSubcontractorController@setPelayananSubcontractor');
            Route::put('set-kesimpulan-subcontractor', 'PenilaianSubcontractorController@setKesimpulanSubcontractor');

            #DAFTAR PENGUJIAN
            Route::post('add-pengujian', 'DaftarPengujianController@addPengujian');
            Route::put('edit-pengujian', 'DaftarPengujianController@editPengujian');
            Route::delete('delete-pengujian', 'DaftarPengujianController@deletePengujian');
            Route::put('approve-pengujian', 'DaftarPengujianController@approvePengujian');

            #DOKUMEN PENGUJIAN
            Route::post('add-dokumen-pengujian', 'DokumenPengujianController@createDocPengujian');
            Route::put('edit-dokumen-pengujian', 'DokumenPengujianController@editDocPengujian');
            Route::delete('delete-dokumen-pengujian', 'DokumenPengujianController@deleteDocPengujian');

            #GAMBAR PENGUJIAN
            Route::post('add-gambar-pengujian', 'GambarPengujianController@createGambarPengujian');
            Route::put('edit-gambar-pengujian', 'GambarPengujianController@editGambarPengujian');
            Route::delete('delete-gambar-pengujian', 'GambarPengujianController@deleteGambarPengujian');

            #TAHAPAN PENGUJIAN
            Route::post('add-tahapan-pengujian', 'TahapanPengujianController@addTahapan');
            Route::put('edit-tahapan-pengujian', 'TahapanPengujianController@editTahapan');
            Route::delete('delete-tahapan-pengujian', 'TahapanPengujianController@deleteTahapan');

            #PEMROSESAN PENGUJIAN
            Route::post('add-pemrosesan', 'PemrosesanBiocompController@addPemrosesan');
            Route::get('get-pemrosesan', 'PemrosesanBiocompController@getPemrosesan');
            Route::put('edit-pemrosesan', 'PemrosesanBiocompController@editPemrosesan');
            Route::delete('delete-pemrosesan', 'PemrosesanBiocompController@deletePemrosesan');
        });
    });

    # Metrologi
    Route::group(['prefix' => 'metrology'], function () {
        #Batch Management
        Route::get('list-batch', 'DataBatchController@index');
        Route::post('add-batch', 'DataBatchController@addBatchData');
        Route::put('edit-batch', 'DataBatchController@editBatchData');
        Route::get('detail-batch', 'DataBatchController@detailBatch');
        Route::delete('delete-batch', 'DataBatchController@deleteBatch');
        Route::put('confirm-batch', 'DataBatchController@approveBatch');
        Route::put('reject-batch', 'DataBatchController@rejectBatch');
        Route::put('set-judgement', 'DataBatchController@setJudgement');


        #Laporan Batch Management
        Route::post('batch/add-laporan', 'LaporanBatchController@addLaporanBatch');
        Route::put('batch/edit-laporan', 'LaporanBatchController@editLaporanBatch');
        Route::get('batch/list-laporan', 'LaporanBatchController@index');
        Route::get('batch/detail-laporan', 'LaporanBatchController@detailLaporanBatch');
        Route::put('batch/confirm-finish-laporan-stage', 'LaporanBatchController@stageLaporan');
        Route::put('batch/confirm-laporan', 'LaporanBatchController@approveLaporan');
        Route::delete('batch/delete-laporan', 'LaporanBatchController@deleteLaporan');

        #Laporan Alligment Management
        Route::post('batch/add-laporan-gambar', 'AlligmentDimensiController@createAllignmentDimensi');
        Route::get('batch/get-laporan-gambar', 'AlligmentDimensiController@index');
        Route::put('batch/edit-laporan-gambar', 'AlligmentDimensiController@editAlligmentDimensi');
        Route::delete('batch/delete-laporan-gambar', 'AlligmentDimensiController@deleteAllignmentDimensi');

        #Laporan Geometri
        Route::get('batch/get-geometri', 'LaporanGeometriController@index');
        Route::post('batch/add-geometri', 'LaporanGeometriController@addLaporanGeometri');
        Route::put('batch/edit-geometri', 'LaporanGeometriController@editLaporanGeometri');
        Route::delete('batch/delete-geometri', 'LaporanGeometriController@deleteLaporanGeometri');
    });

    Route::group(['prefix' => 'biomechanic', "namespace" => "Biomechanic"], function () {
        #ALAT MANAGEMENT
        Route::get('alat', 'AlatBiomecController@index');
        Route::get('showAlat', 'AlatBiomecController@show');
        Route::get('getAllAlat', 'AlatBiomecController@getAllAlat');
        Route::get('showAlatFromKode', 'AlatBiomecController@showFromKode');

        #KALIBRASI ALAT
        Route::get('getKalibrasi', 'AlatBiomecController@getKalibrasi');

        #INSTRUKSI KERJA MANAGEMENT
        Route::get('instruksiKerja', 'InstruksiKerjaBiomecController@searchFromQuery');
        Route::get('showInstruksiKerja', 'InstruksiKerjaBiomecController@showUmum');

        #TAHAPAN IK
        Route::get('tahapanIK', 'TahapanIKBiomecController@index');

        #PEMROSESAN
        Route::get('pemrosesan', 'PemrosesanBiomecController@index');
        Route::get('showPemrosesan', 'PemrosesanBiomecController@show');

        #PEMROSESAN ALAT
        Route::get('showPemrosesanAlat', 'PemrosesanAlatBiomecController@show');
        Route::get('createPemrosesanAlat', 'PemrosesanAlatBiomecController@create');

        #PEMROSESAN IK
        Route::get('showPemrosesanIK', 'TahapanPemrosesanIkBiomecController@showIK');

        Route::group(["middleware" => ["check-divisi:biomechanic"]], function () {
            #ALAT MANAGEMENT
            Route::put('updateAlat', 'AlatBiomecController@edit');
            Route::post('createAlat', 'AlatBiomecController@store');
            Route::put('deleteAlat', 'AlatBiomecController@destroy');

            #KALIBRASI ALAT
            Route::post('createKalibrasi', 'AlatBiomecController@setKalibrasi');
            Route::put('editKalibrasi', 'AlatBiomecController@editKalibrasi');
            Route::delete('deleteKalibrasi', 'AlatBiomecController@deleteKalibrasi');

            #INSTRUKSI KERJA MANAGEMENT
            Route::put('updateInstruksiKerja', 'InstruksiKerjaBiomecController@edit');
            Route::post('createInstruksiKerja', 'InstruksiKerjaBiomecController@store');
            Route::post('acknowledgeInstruksiKerja', 'InstruksiKerjaBiomecController@acknowledgeItem')
                ->middleware(["check-role:koordinator_divisi"]);
            Route::post('approveInstruksiKerja', 'InstruksiKerjaBiomecController@approveItem')
                ->middleware(["check-role:koordinator_lab"]);
            Route::put('deleteInstruksiKerja', 'InstruksiKerjaBiomecController@destroy');

            #TAHAPAN IK
            Route::put('updateTahapanIK', 'TahapanIKBiomecController@editStepsOfIKItem');
            Route::post('createTahapanIK', 'TahapanIKBiomecController@store');
            Route::put('deleteTahapanIK', 'TahapanIKBiomecController@destroy');

            #PEMROSESAN ALAT
            Route::put('deletePemrosesanAlat', 'PemrosesanAlatBiomecController@destroy');
            Route::post('createPemrosesanAlat', 'PemrosesanAlatBiomecController@store');
            Route::put('updatePemrosesanAlat', 'PemrosesanAlatBiomecController@updatePercobaan');

            #PEMROSESAN IK
            Route::post('createPemrosesanIK', 'TahapanPemrosesanIkBiomecController@storeIK');
            Route::put('selesaikanPemrosesanIK', 'TahapanPemrosesanIkBiomecController@selesaikanPemrosesanIK');
            Route::post('cancelPemrosesanIK', 'TahapanPemrosesanIkBiomecController@cancelPemrosesanIK');
            Route::put('deletePemrosesanIK', 'TahapanPemrosesanIkBiomecController@destroyIK');
            Route::put('updateHasilPemrosesan', 'TahapanPemrosesanIkBiomecController@editHasilPemrosesan');
        });
    });

    # Surface treatment
    Route::group([
        'prefix' => 'surface-treatment',
        "namespace" => "surface_treatment"
    ], function () {
        Route::get('alat', 'AlatController@index');
        Route::get('instruksiKerja', 'InstruksiKerjaController@searchFromQuery');
        Route::get('tahapanIK', 'TahapanIKController@index');
        Route::get('pemrosesan', 'PemrosesanController@index');

        Route::get('showAlat', 'AlatController@show');
        Route::get('showAlatFromKode', 'AlatController@showFromKode');
        Route::get('showInstruksiKerja', 'InstruksiKerjaController@showUmum');
        Route::get('showPemrosesanIK', 'TahapanPemrosesanIkController@showIK');
        Route::get('showPemrosesanAlat', 'PemrosesanAlatController@show');
        Route::get('showPemrosesan', 'PemrosesanController@show');

        Route::get('createPemrosesanAlat', 'PemrosesanAlatController@create');
        Route::get('getAllAlat', 'AlatController@getAllAlat');

        # Protect create, edit, and delete endpoints
        Route::group(["middleware" => ["check-divisi:surface_treatment"]], function () {
            Route::put('updateAlat', 'AlatController@edit');
            Route::put('updateInstruksiKerja', 'InstruksiKerjaController@edit');
            Route::put('updateTahapanIK', 'TahapanIKController@editStepsOfIKItem');
            Route::put('updateHasilPemrosesan', 'TahapanPemrosesanIkController@editHasilPemrosesan');
            Route::put('updatePemrosesanIK', 'TahapanPemrosesanIkController@editDataUmumIk');

            Route::post('createAlat', 'AlatController@store');
            Route::post('createInstruksiKerja', 'InstruksiKerjaController@store');
            Route::post('createTahapanIK', 'TahapanIKController@store');

            Route::post('createPemrosesanIK', 'TahapanPemrosesanIkController@storeIK');
            Route::put('selesaikanPemrosesanIK', 'TahapanPemrosesanIkController@selesaikanPemrosesanIK');
            Route::post('cancelPemrosesanIK', 'TahapanPemrosesanIkController@cancelPemrosesanIK');

            Route::post('acknowledgeInstruksiKerja', 'InstruksiKerjaController@acknowledgeItem')
                ->middleware(["check-role:koordinator_divisi"]);
            Route::post('approveInstruksiKerja', 'InstruksiKerjaController@approveItem')
                ->middleware(["check-role:koordinator_lab"]);

            Route::put('deleteAlat', 'AlatController@destroy');
            Route::put('deleteInstruksiKerja', 'InstruksiKerjaController@destroy');
            Route::put('deleteTahapanIK', 'TahapanIKController@destroy');
            Route::put('deletePemrosesanIK', 'TahapanPemrosesanIkController@destroyIK');
            Route::put('deletePemrosesanAlat', 'PemrosesanAlatController@destroy');

            Route::post('createPemrosesanAlat', 'PemrosesanAlatController@store');
            Route::put('updatePemrosesanAlat', 'PemrosesanAlatController@updatePercobaan');
        });
    });

    # Manufaktur
    Route::group(['prefix' => 'manufaktur/permintaan-material'], function () {
        Route::post('store', 'PermintaanMaterialController@storeMaterialRequest');
        Route::get('permintaan-material', 'PermintaanMaterialController@index');
        Route::put('cancel', 'PermintaanMaterialController@cancelPermintaanMaterial');
        Route::put('update', 'PermintaanMaterialController@updatePermintaanMaterial');
        Route::put('done', 'PermintaanMaterialController@setujuiPermintaanMaterial');
    });

    Route::group(['prefix' => 'manufaktur/log-produksi'], function () {
        Route::get('', 'LogBookProduksiManufakturController@index');
        Route::post('store', 'LogBookProduksiManufakturController@store');
        Route::put('update', 'LogBookProduksiManufakturController@update');
    });

    Route::group(['prefix' => 'manufaktur/log-pengembalian'], function () {
        Route::get('', 'LogBookPengembalianPrototypeController@index');
        Route::post('store', 'LogBookPengembalianPrototypeController@store');
        Route::put('terima', 'LogBookPengembalianPrototypeController@acceptPengembalian');
        Route::put('update', 'LogBookPengembalianPrototypeController@update');
    });

    Route::group(['prefix' => 'manufaktur/car'], function () {
        Route::get('', 'CARController@index');
        Route::put('notes', 'CARController@createNotes');
        Route::put('approve', 'CARController@doApprove');
        Route::put('approve-verifikasi', 'CARController@doApproveVerifikasi');
        Route::put('update-verifikasi', 'CARController@updateVerifikasi');
        Route::put('update', 'CARController@update');
        Route::post('koreksi/store', 'CARController@storeKoreksi');
        Route::post('verifikasi-koreksi/store', 'CARController@storeVerifikasiKoreksi');
        Route::post('verifikasi-akhir/store', 'CARController@storeVerifikasiAkhir');
    });

    Route::group(['prefix' => 'manufaktur/kartu-stock-prototype'], function () {
        Route::post('store', 'KartuStockPrototypeController@store');
        Route::post('', 'KartuStockPrototypeController@index');
    });
});

Route::get('getAllSerahTerima', 'SerahTerimaController@getAllSerahTerima')->middleware('cors', 'auth');
Route::get('getDetailSerahTerima/{idSerahTerima}', 'SerahTerimaController@getDetailSerahTerima')->middleware('cors', 'auth');

Route::post('storePermintaanMaterial', 'PermintaanMaterialController@storeMaterialRequest')->middleware('cors', 'auth');
Route::get('getAllPermintaanMaterial', 'PermintaanMaterialController@getAllPermintaanMaterial')->middleware('cors', 'auth');
Route::get('getDetailPermintaanMaterial', 'PermintaanMaterialController@getDetailPermintaanMaterial')->middleware('cors', 'auth');
Route::put('cancelPermintaanMaterial', 'PermintaanMaterialController@cancelPermintaanMaterial')->middleware('cors', 'auth');
Route::put('updatePermintaanMaterial/{idPermintaan}', 'PermintaanMaterialController@updatePermintaanMaterial')->middleware('cors', 'auth');
Route::put('donePermintaanMaterial/{idPermintaan}', 'PermintaanMaterialController@akhiriPermintaanMaterial')->middleware('cors', 'auth');


// Old NAD route

// Route::get('manufaktur/nota-antar-divisi/nota', 'NotaAntarDivisiController@index')
// Route::post('manufaktur/nota-antar-divisi/store', 'NotaAntarDivisiController@storeNotaAntarDivisi')
// Route::put('manufaktur/nota-antar-divisi/update/{idNota}', 'NotaAntarDivisiController@updateNotaAntarDivisi')
// Route::put('manufaktur/nota-antar-divisi/approve', 'NotaAntarDivisiController@setujuiNotaAntarDivisi')
// Route::get('manufaktur/nota-antar-divisi/nota/get_excel', 'NotaAntarDivisiController@export_excel');
// Route::get('manufaktur/nota-antar-divisi/nota/get_pdf', 'NotaAntarDivisiController@export_pdf');

// Route::get('manufaktur/dokumen-serah-terima', 'SerahTerimaController@index')
// Route::get('manufaktur/dokumen-serah-terima/get_pdf', 'SerahTerimaController@export_pdf');

// Route::get('getAllIncomingNota', 'NotaAntarDivisiController@getAllNotaDariDivisiLain')->middleware('cors', 'auth');
// Route::get('getAllOutgoingNota', 'NotaAntarDivisiController@getAllNotaKeDivisiLain')->middleware('cors', 'auth');
// Route::get('getDetailNota', 'NotaAntarDivisiController@getDetailNotaAntarDivisi')->middleware('cors', 'auth');
// Route::post('storeNotaAntarDivisi', 'NotaAntarDivisiController@storeNotaAntarDivisi')->middleware('cors', 'auth');
// Route::put('updateNotaAntarDivisi/{idNota}', 'NotaAntarDivisiController@updateNotaAntarDivisi')->middleware('cors', 'auth');
// Route::put('konfirmasiNotaAntarDivisi/{idNota}', 'NotaAntarDivisiController@konfirmasiNotaAntarDivisi')->middleware('cors', 'auth');
// Route::put('setujuiNotaAntarDivisi', 'NotaAntarDivisiController@setujuiNotaAntarDivisi')->middleware('cors', 'auth');

Route::get('aktivitas/getAll', 'AktivitasUserController@getAllAktivitas')->middleware('cors', 'auth');
