<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermintaanMaterial extends Model
{
    public $timestamps = false;
    protected $table = 'permintaan_materials';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
       'kode_permintaan',
       'nama_item',
       'no_batch',
       'satuan',
       'status',
       'kuantitas',
       'tanggal',
       'diminta_oleh',
       'diterima_oleh',
       'diserahkan_oleh'
    ];
}
