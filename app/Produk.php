<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';
    protected $fillable = [
        'kode_produk',
        'id_project',
        'jenis_material',
        'nama_produk',
        'jumlah_item',
        'yang_menambahkan',
        'keterangan'
    ];


}
