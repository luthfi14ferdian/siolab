<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanAllignmentDimensi extends Model
{
    public $timestamps = false;
    protected $table = 'laporan_allignment_dimensi';

    protected $hidden = [
        'created_at', 'updated_at'
   ];
}
