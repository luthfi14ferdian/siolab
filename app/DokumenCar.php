<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DokumenCar extends Model
{
    public $timestamps = false;
    protected $table = 'dokumen_car';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id',
        'nama_project',
        'jenis',
        'divisi',
        'standar',
        'nomor',
        'tanggal',
        'status',
        'id_pengembalian',

    ];
}
