<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{

    /** Hardcoded divisi id */
    const DIVISI_NAME_TO_ID = [
        "desain" => 1,
        "material" => 2,
        "manufaktur" => 3,
        "metrologi" => 4,
        "biocompatibility" => 5,
        "surface_treatment" => 6,
        "biomechanic" => 7
    ];
   
    public $timestamps = false;
    protected $table = 'divisi';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id',
        'nama',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'created_at', 'updated_at'
    ];
}
