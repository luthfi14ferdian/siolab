<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class material extends Model
{
    public $timestamps = false;
    protected $table = 'materials';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id',
        'jenis_material',
        'satuan',
        'keterangan',
        'kode_proyek',
    ];
}
