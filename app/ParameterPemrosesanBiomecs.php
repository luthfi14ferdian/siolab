<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParameterPemrosesanBiomecs extends Model
{
    protected $table = 'parameter_pemrosesan_biomecs';
    protected $primaryKey = ['id_parameter', 'id_pemrosesan'];
    public $incrementing = false;
    public $timestamps = false;
}
