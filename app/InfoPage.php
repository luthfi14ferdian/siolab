<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoPage extends Model
{
    protected $table = 'info_page';

    protected $hidden = [
        'created_at', 'updated_at'
   ];

   public function diinput_oleh()
   {
       return $this->belongsTo('App\User', 'diinput_oleh');
   }
}
