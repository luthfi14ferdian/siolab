<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SerahTerima extends Model
{
    public $timestamps = false;
    protected $table = 'serah_terimas';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id_NAD',
        'tanggal',
        'nama_item',
        'no_batch',
        'jumlah',
        'tujuan',
        'status',
        'keterangan',
        'yang_menyerahkan',
        'yang_menerima',
    ];

    public function scopeFindByIdNADOrFail($query, $id)
    {
    $result = $query->where('id_NAD', $id)->first();

    if (!is_null($result))
    {
        return $result->id;
    }

    throw (new ModelNotFoundException())->setModel(SerahTerima::class);
    }
}
