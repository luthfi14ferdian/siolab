<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class TahapanPemrosesanIk extends Model
{

    public static function serializePemrosesanIkItem($idPercobaan)
    {

        $pemrosesanIK = DB::table("pemrosesan_iks")
            ->select("pemrosesan_iks.*")
            ->where("pemrosesan_iks.id_pemrosesan", $idPercobaan)
            ->get();

        if (count($pemrosesanIK) != 0) {

            $dataIk = InstruksiKerja::getForDetail($pemrosesanIK[0]->id_ik);

            $dataUmumItems = DB::table("pemrosesan_iks")
                ->join("pemrosesans", "pemrosesan_iks.id_pemrosesan", "=", "pemrosesans.id")
                ->where("pemrosesans.id", $idPercobaan)
                ->get();
            
            $dataUmum = $dataUmumItems[0];
            $dataUmum->diproses_oleh =  User::find($dataUmum->diproses_oleh);
            $dataUmum->produk = Produk::find($dataUmum->id_produk);

            $allDataTahapan = DB::table("tahapan_pemrosesan_iks")
                ->join("tahapan_iks", "tahapan_pemrosesan_iks.id_tahapan", "=", "tahapan_iks.id")
                ->select("tahapan_iks.uraian", "tahapan_pemrosesan_iks.*", "tahapan_iks.urutan")
                ->where("tahapan_pemrosesan_iks.id_pemrosesan", $idPercobaan)
                ->get()->sortBy("urutan");

            $alats = DB::table("alat_tahapan_iks")
                ->join("alats", "alat_tahapan_iks.id_alat", "=", "alats.id")
                ->select("alats.*", "alat_tahapan_iks.id_tahapan")
                ->get();
            
            $konfirmasi_penyelesaiaan = false;

            for ($i = 0; $i < count($allDataTahapan); $i++) {
                $alatTemp = [];
                $allDataTahapan[$i] = collect($allDataTahapan[$i])->put("alat_terkait", []);

                $alat = $alats->where("id_tahapan", $allDataTahapan[$i]["id_tahapan"]);

                foreach ($alat as $key => $value) {
                    $thisAlat = [];

                    $thisAlat = collect($thisAlat)->put("id_alat", $value->id);
                    $thisAlat = collect($thisAlat)->put("kode_alat", $value->kode);
                    $thisAlat = collect($thisAlat)->put("nama_alat", $value->nama);
                    $thisAlat = collect($thisAlat)->put("tipe_alat", $value->tipe);
                    $thisAlat = collect($thisAlat)->put("fungsi_alat", $value->fungsi);
                    $thisAlat = collect($thisAlat)->put("created_at", $value->created_at);
                    $thisAlat = collect($thisAlat)->put("updated_at", $value->updated_at);

                    $param = DB::table("param_alats")
                        ->join("parameter_pemrosesans", "param_alats.id", "=", "parameter_pemrosesans.id_parameter")
                        ->select("param_alats.id", "param_alats.parameter", "param_alats.metric", "parameter_pemrosesans.value")
                        ->where("param_alats.id_alat", $value->id)
                        ->where("parameter_pemrosesans.id_tahapan", $allDataTahapan[$i]["id_tahapan"])
                        ->where("parameter_pemrosesans.id_pemrosesan", $idPercobaan)
                        ->get();

                    $thisAlat->put("parameters", $param->toArray());

                    array_push($alatTemp, $thisAlat);
                }

                $allDataTahapan[$i]["alat_terkait"] = $alatTemp;

                $konfirmasi_penyelesaiaan = false;
                $status_tahapans = DB::table("tahapan_pemrosesan_iks")
                    ->select("tahapan_pemrosesan_iks.status")
                    ->where("tahapan_pemrosesan_iks.id_pemrosesan", $idPercobaan)
                    ->get()->pluck("status")->toArray();

                if (!in_array("Sedang dikerjakan", $status_tahapans, true) && !in_array("Belum diproses", $status_tahapans, true)) {
                    $konfirmasi_penyelesaiaan = true;
                }
            }

            return [
                "data_umum" => $dataUmum,
                "data_ik" => $dataIk,
                "tahapan_pemrosesan" => $allDataTahapan,
                "konfirmasi_penyelesaiaan" => $konfirmasi_penyelesaiaan,
            ];
        }
    }
}
