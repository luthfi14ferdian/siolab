<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PemrosesanIkBiomec extends Model
{
    protected $table = 'pemrosesan_ik_biomecs';

    public $timestamps = true;
}
