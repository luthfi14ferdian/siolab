<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TahapanIK extends Model
{
    public $timestamps = true;
    protected $fillable = [
        "id_ik",
        "uraian",
        "urutan",
    ];
}
