<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KodeAktivitas extends Model
{
    public $timestamps = false;
    protected $table = 'kode_aktivitas';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id',
        'jenis_aktivitas',
    ];
}
