<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use function PHPSTORM_META\map;

class project extends Model
{
    public $timestamps = false;
    protected $table = 'projects';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'nama_project',
        'project_leader',
        'pic_desain',
        'pic_manufaktur',
        'pic_material',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
   ];

   public function jenis_project()
   {
       return $this->belongsTo('App\JenisProject', 'nama_project');
   }
}
