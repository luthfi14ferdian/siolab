<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DokumenSerahTerima extends Model
{
    public $timestamps = false;
    protected $table = 'dokumen_serah_terimas';

    protected $hidden = [
        'created_at', 'updated_at'
   ];

   public function produk()
    {
        return $this->belongsTo('App\Produk', 'id_produk');
    }
}


