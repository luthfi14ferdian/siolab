<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaAntarDivisi extends Model
{
    public $timestamps = false;
    protected $table = 'nota_antar_divisis';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'nomor_WO',
        'nomor_NAD',
        'nomor_batch',
        'tanggal',
        'divisi_yang_mengajukan',
        'divisi_yang_diajukan',
        'nama_barang',
        'satuan',
        'kuantitas',
        'keterangan',
        'dibuat_oleh',
        'diketahui_oleh',
        'disetujui_oleh',
        'dokumen_3d_fit',
        'dokumen_drawing',
        'dokumen_spec_desain',
        'id_produk',
        'status',
    ];
    protected $hidden = [
         'created_at', 'updated_at'
    ];

    public function diketahui_oleh()
    {
        return $this->belongsTo('App\User');
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk', 'id_produk');
    }
}
