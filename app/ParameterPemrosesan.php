<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParameterPemrosesan extends Model
{
    protected $table = 'parameter_pemrosesans';
    protected $primaryKey = ['id_parameter', 'id_pemrosesan'];
    public $incrementing = false;
    public $timestamps = false;
}
