<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KartuStockPrototype extends Model
{
    public $timestamps = false;
    protected $table = 'kartu_stock_prototype';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id',
        'nama_project',
        'jenis_prototype',
        'no_batch',
        'masuk',
        'keluar'.
        'keterangan',
        'dikerjakan_oleh'

    ];


}
