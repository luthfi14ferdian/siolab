<?php

namespace App;

use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Database\Eloquent\Model;

class PemrosesanAlatBiomec extends Model
{
    public static function serializePercobaanItem($idPercobaan)
    {

        // Select data umum while checking whether item exists or not
        $matchingPercobaanItems =  DB::table("pemrosesan_alat_biomecs")
            ->join("pemrosesan_biomecs", "pemrosesan_alat_biomecs.id_pemrosesan", "=", "pemrosesan_biomecs.id")
            ->where("pemrosesan_biomecs.id", $idPercobaan)->get()->toArray();

        if (count($matchingPercobaanItems) < 1) {
            return;
        }


        $dataUmum = $matchingPercobaanItems[0];
        $dataUmum->produk = Produk::find($dataUmum->id_produk);

        unset($dataUmum->id_pemrosesan);

        $dataUmum->diproses_oleh = User::find($dataUmum->diproses_oleh);

        $alats = DB::table("percobaan_alat_biomecs")
            ->join("alat_biomecs", "percobaan_alat_biomecs.id_alat", "=", "alat_biomecs.id")
            ->join("pemrosesan_biomecs", "percobaan_alat_biomecs.id_pemrosesan", "=", "pemrosesan_biomecs.id")
            ->select("alat_biomecs.*", "percobaan_alat_biomecs.note")
            ->where("pemrosesan_biomecs.id", $idPercobaan)
            ->get();

        $alatTemp = [];
        foreach ($alats as $key => $value) {
            $thisAlat = [];
            $thisAlat = collect($thisAlat)->put("id", $value->id);
            $thisAlat->put("kode", $value->kode);
            $thisAlat->put("nama", $value->nama);
            $thisAlat->put("tipe", $value->tipe);
            $thisAlat->put("fungsi", $value->fungsi);
            $thisAlat->put("note", $value->note);

            $param = DB::table("parameter_pemrosesan_biomecs")
                ->where("parameter_pemrosesan_biomecs.id_pemrosesan", $idPercobaan)
                ->join("param_alat_biomecs", "parameter_pemrosesan_biomecs.id_parameter", "=", "param_alat_biomecs.id")
                ->select("param_alat_biomecs.id", "param_alat_biomecs.parameter", "param_alat_biomecs.metric", "parameter_pemrosesan_biomecs.value")
                ->where("param_alat_biomecs.id_alat", $value->id)
                ->get();

            $thisAlat->put("parameters", $param->toArray());

            array_push($alatTemp, $thisAlat);
        }

        return [
            "data_umum" => $dataUmum,
            "alats" => $alatTemp,
        ];
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk', 'id_produk');
    }
}
