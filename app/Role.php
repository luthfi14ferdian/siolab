<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    /** Hardcoded divisi id */
    const ROLE_NAME_TO_ID = [
        "anggota" => 1,
        "penanggung_jawab_divisi" => 2,
        "koordinator_divisi" => 3,
        "manajer_representative" => 4,
        "admin" => 5,
        "super_admin" => 6,
        "pic_project" => 7,
        "project_leader" => 8,
        "koordinator_lab" => 9,
    ];

    public $timestamps = false;
    protected $table = 'role';

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
