<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataBatch extends Model
{
    protected $table = 'data_batch';

    protected $hidden = [
        'created_at', 'updated_at'
   ];

}
