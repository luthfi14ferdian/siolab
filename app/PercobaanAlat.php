<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PercobaanAlat extends Model
{
    protected $table = 'percobaan_alats';
    protected $primaryKey = ['id_pemrosesan', 'id_alat'];
    public $incrementing = false;

    public $timestamps = true;
    protected $hidden = [
        'created_at', 'updated_at'
   ];

}
