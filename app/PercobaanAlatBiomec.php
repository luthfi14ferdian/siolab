<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PercobaanAlatBiomec extends Model
{
    protected $table = 'percobaan_alat_biomecs';
    protected $primaryKey = ['id_pemrosesan', 'id_alat'];
    public $incrementing = false;

    public $timestamps = true;
    protected $hidden = [
        'created_at', 'updated_at'
   ];
}
