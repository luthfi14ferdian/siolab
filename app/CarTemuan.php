<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarTemuan extends Model
{
    public $timestamps = false;
    protected $table = 'car_temuan';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id',
        'id_car',
        'temuan',
        'status',
        'auditor',
        'auditee',
        'manager_representative',
        'note',

    ];

    public function scopeFindByIdCAROrFail($query, $id)
    {
    $result = $query->where('id_car', $id)->first();

    if (!is_null($result))
    {
        return $result->id;
    }


    throw (new ModelNotFoundException())->setModel(SerahTerima::class);
    }
}
