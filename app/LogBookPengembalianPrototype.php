<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogBookPengembalianPrototype extends Model
{
    public $timestamps = false;
    protected $table = 'log_book_pengembalian_prototypes';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id',
        'tanggal',
        'item',
        'jumlah',
        'keterangan',
        'divisi_melapor',
        'divisi_terlapor',
        'yang_menyerahkan',
        'yang_menerima',
        'status_pengembalian',
        'dokumen_CAR'
    ];
}
