<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;
use App\Divisi;

class CheckDivisi
{
    /**
     * Protect endpoint based on user divisi.
     * 
     * How to use:
     * `$endpoint->middleware("check-divisi:<nama_divisi_1_in_snake_case>,<nama_divisi_2_in_snake_case>")`
     * 
     * **Note:** nama divisi must be separated with comma without any space
     * Examples:
     * 1. Allow surface treatment user only:
     *    `->middleware("check-divisi:surface_treatment")`
     * 2. Allow desain and manufaktur user only:
     *    `->middleware("check-divisi:desain,manufaktur")`
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowedDivisiName = array_slice(func_get_args(), 2);
        $user = Auth::user();
        $userDivisiId = $user->divisi_id;

        $isAllowed = array_reduce($allowedDivisiName, function ($carry, $divisiName) use ($userDivisiId) {
            $itemDivisiId = Divisi::DIVISI_NAME_TO_ID[$divisiName];
            return $carry || ($userDivisiId === $itemDivisiId);
        }, false);

        if (!$isAllowed) {
            return response()->json(['message' => 'Forbidden to perform this operation.'], 403);
        }
        return $next($request);
    }
}
