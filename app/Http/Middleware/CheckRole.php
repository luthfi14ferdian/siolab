<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;
use App\Role;

class CheckRole
{
    /**
     * Protect endpoint based on user role.
     * 
     * How to use:
     * `$endpoint->middleware("check-role:<nama_role_1_in_snake_case>,<nama_role_2_in_snake_case>")`
     * 
     * **Note:** nama role must be separated with comma without any space
     * Examples:
     * 1. Allow admin user only:
     *    `->middleware("check-role:admin")`
     * 2. Allow anggota lab and admin only:
     *    `->middleware("check-role:anggota_lab,admin")`
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowedRoleName = array_slice(func_get_args(), 2);
        $user = Auth::user();
        $userRoleId = $user->role_id;

        $isAllowed = array_reduce($allowedRoleName, function ($carry, $roleName) use ($userRoleId) {
            $itemRoleId = Role::ROLE_NAME_TO_ID[$roleName];
            return $carry || ($userRoleId === $itemRoleId);
        }, false);

        if (!$isAllowed) {
            return response()->json(['message' => 'Forbidden to perform this operation.'], 403);
        }
        return $next($request);
    }
}
