<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InfoPage;
use App\Divisi;
use App\Helpers\FilePathUtils;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class InfoPageController extends Controller
{
    public function index(Request $request)
    {
        $documentName = $request->nama_dokumen;
        $documentName = isset($documentName) ? trim($documentName) : '';
        $category = $request->kategori;

        $queryDb = InfoPage::query();

        if (isset($category)) {
            $queryDb = $queryDb->where("kategori", $category);
        }

        if ($documentName !== '') {
            $queryDb = $queryDb->where("nama_dokumen", 'ilike', '%' . $documentName . '%');
        }

        $queryDb = $queryDb->paginate(10);
        $queryDb->makeHidden(["diinput_oleh", "notes"]);

        $items = $queryDb->items();
        $total_item = $queryDb->total();
        $current_page = $queryDb->currentPage();
        $total_item_per_page = $queryDb->perPage();

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $items
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }


    public function kategori(){
        $alldivisi= Divisi::all();

        $kategori=['general'];

        foreach($alldivisi as $divisi){
            array_push($kategori, $divisi->nama);
        }

        return response()->json([
            "status"=>1,
            "data"=>$kategori
        ], 200);
    }

    public function store(Request $request){
        $info_page = new InfoPage();
        $name= FilePathUtils::getRandomFileName($request->file('file'));
        $path= Storage::putFileAs('dokumen_info', new File($request->file), $name);

        $info_page->nama_dokumen = $request->nama_dokumen;
        $info_page->kategori = $request->kategori;
        $info_page->diinput_oleh = $request->diinput_oleh;
        $info_page->notes= $request->notes;
        $info_page->nama_file=$name;
        $info_page->dokumen_path=$path;

        $info_page->save();
        return response()->json([
            "status"=>1,
            "message" => "Data berhasil disimpan"
        ], 200);

    }
    public function detail(Request $request){
        $info_page = InfoPage::with(['diinput_oleh'])->find($request->id);
        if($info_page){
            return response()->json([
                "status"=>1,
                "message" => "Berhasil",
                "data"=>$info_page,
            ], 200);
        }

        else{
            return response()->json([
                "status"=>0,
                "message" => "Data tidak ditemukkan"
            ], 404);
        }
    }

    public function edit(Request $request)
    {
        $info_page = InfoPage::find($request->id);
        if (!$info_page) {
            return response()->json([
                "status" => 1,
                "message" => "Data tidak ditemukan"
            ], 404);
        }

        $info_page->nama_dokumen = $request->nama_dokumen;
        $info_page->kategori = $request->kategori;
        $info_page->notes = $request->notes;

        if ($request->file != null) {
            Storage::disk('public')->delete($info_page->dokumen_path);

            $name = FilePathUtils::getRandomFileName($request->file('file'));
            $path = Storage::putFileAs('dokumen_info', new File($request->file), $name);

            $info_page->nama_file = $name;
            $info_page->dokumen_path = $path;
        }
        $info_page->save();

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil diubah"
        ], 200);
    }

    public function delete(Request $request){
        $info_page = InfoPage::find($request->id);

        if($info_page){
            Storage::disk('public')->delete($info_page->dokumen_path);
            $info_page->delete();

            return response()->json([
                "status"=>1,
                "message" => 'Data berhasil dihapus',
            ],200);
        }

        else{
            return response()->json([
                "status"=>1,
                "message" => "Data tidak ditemukan"
            ], 404);
        }

    }

}
