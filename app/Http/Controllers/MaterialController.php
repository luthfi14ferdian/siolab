<?php

namespace App\Http\Controllers;

use App\material;
use App\Produk;
use DB;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProduk(Request $request)
    {
        $queryKode = $request->kode;
        $queryKode = isset($queryKode) ? trim($queryKode) : '';

        $projectId = $request->id_project;
        $dbQuery = Produk::where('id_project', $projectId);

        if ($queryKode !== '') {
            $dbQuery = $dbQuery->where('kode_produk', 'LIKE', '%'.$queryKode.'%');
        }

        $paginated = $dbQuery->paginate(10);

        $items = $paginated->items();
        $total_item =  $paginated->total();
        $current_page =  $paginated->currentPage();
        $total_item_per_page =  $paginated->perPage();

        return response()->json([
            "status" => 1,
            "data" => [
                "item" =>$items
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\material  $material
     * @return \Illuminate\Http\Response
     */
    public function show(material $material)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\material  $material
     * @return \Illuminate\Http\Response
     */
    public function edit(material $material)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\material  $material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, material $material)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\material  $material
     * @return \Illuminate\Http\Response
     */
    public function destroy(material $material)
    {
        //
    }
}
