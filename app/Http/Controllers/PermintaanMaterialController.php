<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PermintaanMaterial;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\material;
use App\User;
use App\KodeAktivitas;

class PermintaanMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if($request->id_item == NULL){
            $all_permintaan = DB::table('permintaan_materials')
            ->where('nama_item', $request->id_proyek)
            ->select('permintaan_materials.kode_permintaan', 'permintaan_materials.nama_item', 'permintaan_materials.diminta_oleh as penanggung_jawab'
            , 'permintaan_materials.status')
            ->get();

            $permintaanMaterial = array_chunk($all_permintaan->toArray(), 10);
            if (count($permintaanMaterial) <= (($request->start)-1) ) {
                $permintaanMaterial = [];
            }
            else {
                $permintaanMaterial = $permintaanMaterial[($request->start)-1];
            }

            $total_item = count($all_permintaan);
            $current_page = (int)$request->start;
            $total_item_per_page = count($permintaanMaterial);

            return response()->json([
                "status" => 1,
                "data" => [
                    "item" => $permintaanMaterial
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_pr_page" => $total_item_per_page,
                ],
            ]);
        }
        else{
            return response()->json($this->getDetailPermintaanMaterial($request));
        }
    }

    public function getDetailPermintaanMaterial(Request $request){
        $detail_permintaan = PermintaanMaterial::find($request->id_item);

        $detail_permintaan->nama_item = Material::find($detail_permintaan->nama_item);
        $detail_permintaan->diminta_oleh = User::find($detail_permintaan->diminta_oleh
        , ['id', 'username', 'last_name', 'first_name']);
        $detail_permintaan->diserahkan_oleh = User::find($detail_permintaan->diserahkan_oleh
        , ['id', 'username', 'last_name', 'first_name']);
        $detail_permintaan->diterima_oleh = User::find($detail_permintaan->diterima_oleh
        , ['id', 'username', 'last_name', 'first_name']);
        return $detail_permintaan;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function createAktivitasUser($aktivitas, $kode, $idUser, $idAktivitas){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        DB::table('aktivitas_user')->insert([
            'jenis_aktivitas' => $aktivitas,
            'id_user' => $idUser,
            'kode_aktivitas' => KodeAktivitas::where('jenis_aktivitas', $kode)->first()->id,
            'id_aktivitas' => $idAktivitas,
            'created_at' => $mytimenew->toDateTimeString(),
            'updated_at' => $mytimenew->toDateTimeString(),
        ]);
    }

    private function invalidMaterialRequest(Request $request){
        return (
            $request->nama_item == NULL ||
            $request->no_batch == NULL ||
            $request->tanggal == NULL ||
            $request->satuan == NULL ||
            $request->kuantitas == NULL ||
            $request->diminta_oleh == NULL
        );
    }

    private function randomCharacters($number){
        $alpha = array();
        for ($u = 65; $u <= 90; $u++) {
            // Uppercase Char
            array_push($alpha, chr($u));
        }

        // Just in case you need lower case
        // for ($l = 97; $l <= 122; $l++) {
        //    // Lowercase Char
        //    array_push($alpha, chr($l));
        // }

        // Get random alpha character
        $rand_alpha_key = array_rand($alpha);
        $rand_alpha = $alpha[$rand_alpha_key];

        // Add the other missing integers
        $rand = array($rand_alpha);
        for ($c = 0; $c < $number - 1; $c++) {
            array_push($rand, mt_rand(0, 9));
            shuffle($rand);
        }

        return implode('', $rand);
    }

    public function storeMaterialRequest(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $randomCode = $this->randomCharacters(6);
        if(!$this->invalidMaterialRequest($request)){
            $permintaanMaterial = DB::table('permintaan_materials')->insertGetId($request->all() +
            ['created_at' => $mytimenew->toDateTimeString(), 'status'=> 'baru', 'kode_permintaan'=>$randomCode]);

            $this->createAktivitasUser('Membuat Permintaan Material', 'Permintaan Material', $request->session()->get('id'), $permintaanMaterial);

            if($permintaanMaterial != NULL){
                return response()
                    ->json(['status' => 'data has been inserted']);
            }
        }
        else{
            return response()
                    ->json(['status' => 'FAILED','reason'=>'Empty request']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PermintaanMaterial  $permintaanMaterial
     * @return \Illuminate\Http\Response
     */
    public function show(PermintaanMaterial $permintaanMaterial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PermintaanMaterial  $permintaanMaterial
     * @return \Illuminate\Http\Response
     */
    public function edit(PermintaanMaterial $permintaanMaterial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PermintaanMaterial  $permintaanMaterial
     * @return \Illuminate\Http\Response
     */
    public function updatePermintaanMaterial(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $permintaanMaterial = DB::table('permintaan_materials')
            ->where('permintaan_materials.id', $request->idPermintaan)
            ->get();
        if($permintaanMaterial[0]->status == 'baru'){
            DB::table('permintaan_materials')
            ->where('permintaan_materials.id', $request->idPermintaan)
            ->update($request->except('idPermintaan') + ['updated_at' => $mytimenew->toDateTimeString()]);

            $this->createAktivitasUser('Mengubah Permintaan Material', 'Permintaan Material', $request->session()->get('id'), $request->idPermintaan);

            return response()->json(['ID'=> $permintaanMaterial[0]->id, 'status'=> 'Sukses mengubah'], 200);
        }
        else{
            return response()->json(['ID'=> $permintaanMaterial[0]->id, 'status'=> 'Gagal mengubah karena nota bukan nota baru'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PermintaanMaterial  $permintaanMaterial
     * @return \Illuminate\Http\Response
     */
    public function destroy(PermintaanMaterial $permintaanMaterial)
    {
        //
    }

    public function cancelPermintaanMaterial(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $permintaanMaterial = DB::table('permintaan_materials')
            ->where('permintaan_materials.id', $request->idPermintaan)
            ->get();
        if($permintaanMaterial[0]->status == 'baru'){
            DB::table('permintaan_materials')
            ->where('permintaan_materials.id', $request->idPermintaan)
            ->update(['status'=>'dibatalkan','updated_at' => $mytimenew->toDateTimeString()]);

            return response()->json(['ID'=> $permintaanMaterial[0]->id, 'status'=> 'Permintaan Material berhasil dibatalkan'], 200);
        }
        else{
            return response()->json(['ID'=> $permintaanMaterial[0]->id, 'status'=> 'Gagal mengubah karena status permintaan sudah bukan Baru'], 200);
        }
    }

    public function setujuiPermintaanMaterial(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $permintaanMaterial = DB::table('permintaan_materials')
            ->where('permintaan_materials.id', $request->idPermintaan)
            ->get();
        if($permintaanMaterial[0]->status == 'sedang_diproses'){
            $this->ubahStatus($request->idPermintaan, 'selesai',
                $request->session()->get('id'), 'diterima_oleh');

            return response()->json(['ID'=> $permintaanMaterial[0]->id, 'status'=> 'Permintaan Material berhasil diselesaikan'], 200);
        }
        else{
            return response()->json(['ID'=> $permintaanMaterial[0]->id, 'status'=> 'Gagal mengubah karena permintaan belum diproses'], 200);
        }
    }

    private function ubahStatus($idPermintaan, $newStatus, $idUser, $test){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');

        $nota = PermintaanMaterial::updateOrCreate(
            ['permintaan_materials.id' => $idPermintaan],
            ['status'=>$newStatus,$test=>$idUser,'updated_at' => $mytimenew->toDateTimeString()]
        );

        return $nota;
    }
}
