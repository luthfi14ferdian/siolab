<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Divisi;
use App\Role;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserController extends Controller
{
    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request){
        $allUser = User::paginate(10);
        $allUser->makeVisible(['is_approve','created_at','email']);

        $items = $allUser->items();
        $total_item = $allUser->total();
        $current_page = $allUser->currentPage();
        $total_item_per_page = $allUser->perPage();

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $items
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    public function register(Request $request){
        $user_db= User::all();
        foreach($user_db as $us ){
            if($request->username==$us->username){
                return response()->json([
                    "status" => 1,
                    "message" => 'Akun dengan username tersebut sudah terdaftar'
                ], 404);
            }
            elseif($request->email==$us->email){
                return response()->json([
                    "status" => 1,
                    "message" => 'Akun dengan email tersebut sudah terdaftar'
                ], 404);
            }
        }
        $user = new User();
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->divisi_id= $request->divisi_id;
        $user->is_approve= False;
        $user->role_id= $request->role_id;
        $user->created_at = Carbon::now();
        $result=$user->save();
        return response()->json([
            "status" => 1,
            "message" => 'Akun berhasil dibuat'
        ], 200);

    }

    public function getUserLogin(){
        $user = Auth::user();
        $user->makeVisible('email');
        $role= Role::find($user->role_id);
        $divisi=Divisi::find($user->divisi_id);

        return response()->json([
            "status" => 1,
            "data" =>[
                'user'=> $user,
                'role'=>$role,
                'divisi'=>$divisi,
            ],
        ]);
    }

    public function getDetailUser(Request $request){
        $user = User::find($request->id);
        $divisi = Divisi::find($user->divisi_id);

        if ($user) {
            $user->makeVisible(['status','created_at','email']);
            return response()->json([
                "status" => 1,
                "data" =>[
                    'user'=> $user,
                    'divisi'=>$divisi
                ],
            ]);
        }
        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data tidak ditemukan"
            ],404);
        }
    }

    public function editUserAdmin(Request $request){
        $user = User::find($request->id);

        if($user){
            $user->first_name= $request->first_name;
            $user->last_name= $request->last_name;
            $user->divisi_id= $request->divisi_id;
            $user->role_id = $request->role_id;
            $user->save();

            $divisi= Divisi::find($user->divisi_id);
            $role= Role::find($user->role_id);
            return response()->json([
                "status" => 1,
                "data" =>[
                    'user'=> $user,
                    'divisi'=>$divisi,
                    'role' =>$role,
                ],
            ]);
        }

        else{
            return response()->json([
                'status' => 0,
                'message'=> "Data tidak ditemukan"
            ],404);
        }
    }

    public function editUser(Request $request){
        $user = User::find($request->id);
        $user_db= User::all();
        $user->makeVisible(['email']);

       if($user->email != $request->email){
        foreach($user_db as $us ){
            if($request->email==$us->email){
                return response()->json([
                    "status" => 1,
                    "message" => 'Email tersebut sudah terdaftar'
                ], 404);
            }
        }
       }
        if($user){
            $user->first_name= $request->first_name;
            $user->last_name= $request->last_name;
            $user->email= $request->email;
            $user->save();

            return response()->json([
                "status" => 1,
                "data" =>[
                    'user'=> $user,
                ],
            ]);
        }

        else{
            return response()->json([
                'status' => 0,
                'message'=> "Data tidak ditemukan"
            ],404);
        }
    }

    public function changePassword(Request $request){
        $user = User::find($request->id);
        if ($user) {
            $user->makeVisible(['is_approve','created_at','email']);
            $user->makeHidden('created_at','updated_at');
            if(Hash::check($request->oldPassword,$user->password)){
                $user->password = bcrypt($request->newPassword);
                $user->save();
                return response()->json([
                    'status' => 1,
                    'message' => 'Password berhasil diubah',
                    'data' => $user
                ]);
            }

            else{
                return response()->json([
                    'status' => 0,
                    'data'=> $user->password,
                    'message'=> "Password lama tidak cocok"
                ]);
            }
        }

        else{
            return response()->json([
                'status' => 0,
                'message'=> "Data tidak ditemukan"
            ],404);
        }
    }

    public function changePasswordAdmin(Request $request){
        $user = User::find($request->id);

        if ($user) {
            $user->makeVisible(['is_approve','created_at']);
            $user->password = bcrypt($request->newPassword);
            $user->save();
            return response()->json([
                'status' => 1,
                'message' => 'Password berhasil diubah',
                'data' => $user
            ]);


        }

        else{
            return response()->json([
                'status' => 0,
                'message'=> "Data tidak ditemukan"
            ],404);
        }
    }

    public function approveUser(Request $request){
        $user = User::find($request->id);

        if ($user) {
            $user->makeVisible(['is_approve','created_at']);
            $user->is_approve= True;
            $user->save();

            return response()->json([
                "status" => 1,
                "message" => 'Data pengguna berhasil diverifikasi',
                "data" => $user,
            ]);
        }

        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data pengguna tidak ditemukan"
            ],404);
        }
    }


    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    // public function register(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'name' => 'required',
    //         'email' => 'required|email',
    //         'password' => 'required',
    //         'c_password' => 'required|same:password',
    //     ]);
    //     if ($validator->fails()) {
    //         return response()->json(['error'=>$validator->errors()], 401);
    //     }
    //     $input = $request->all();
    //     $input['password'] = bcrypt($input['password']);
    //     $user = User::create($input);
    //     $success['token'] =  $user->createToken('MyApp')-> accessToken;
    //     $success['name'] =  $user->name;
    //     return response()->json(['success'=>$success], $this-> successStatus);
    // }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this-> successStatus);
    }
}

