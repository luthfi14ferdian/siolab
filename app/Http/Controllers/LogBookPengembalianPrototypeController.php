<?php

namespace App\Http\Controllers;

use App\LogBookPengembalianPrototype;
use App\User;
use App\Divisi;
use App\CarTemuan;
use App\DokumenCar;
use App\Project;
use App\KodeAktivitas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LogBookPengembalianPrototypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if($request->id_item != NULL){
            return response()->json($this->getPengembalian($request));
        }
        else{
            $all_log_pengembalian = DB::table('log_book_pengembalian_prototypes')
            ->where('nama_item', $request->id_proyek)
            ->get();

            foreach ($all_log_pengembalian as $p){
                $p->divisi_terlapor = Divisi::find($p->divisi_terlapor
                , ['id', 'nama']);
            }

            $logPengembalian = array_chunk($all_log_pengembalian->toArray(), 10);
            if (count($logPengembalian) <= (($request->page)-1) ) {
                $logPengembalian = [];
            }
            else {
                $logPengembalian = $logPengembalian[($request->page)-1];
            }

            $total_item = count($all_log_pengembalian);
            $current_page = (int)$request->page;
            $total_item_per_page = count($logPengembalian);

            return response()->json([
                "status" => 1,
                "data" => [
                    "item" => $logPengembalian
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_pr_page" => $total_item_per_page,
                ],
            ]);
        }

    }

    public function getPengembalian(Request $request){
        $logPengembalian = LogBookPengembalianPrototype::find($request->id_item);
        $logPengembalian->item = Project::find($logPengembalian->item
        , ['id', 'nama_project']);
        $logPengembalian->divisi_melapor = Divisi::find($logPengembalian->divisi_melapor
        , ['id', 'nama']);
        $logPengembalian->divisi_terlapor = Divisi::find($logPengembalian->divisi_terlapor
        , ['id', 'nama']);
        $logPengembalian->yang_menyerahkan = User::find($logPengembalian->yang_menyerahkan
        , ['id', 'username', 'last_name', 'first_name']);
        $logPengembalian->yang_menerima = User::find($logPengembalian->yang_menerima
        , ['id', 'username', 'last_name', 'first_name']);

        return ($logPengembalian);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function invalidLogPengembalian($data){
        return (
            $data["logBook"]["tanggal"] == NULL ||
            $data["logBook"]["item"] == NULL ||
            $data["logBook"]["batch"] == NULL ||
            $data["logBook"]["jumlah"] == NULL ||
            $data["logBook"]["divisi_terlapor"] == NULL ||
            $data["car"]["jenis"] == NULL ||
            $data["car"]["divisi"] == NULL ||
            $data["car"]["standar"] == NULL ||
            $data["car"]["nomor"] == NULL ||
            $data["car"]["tanggal"] == NULL ||
            $data["car"]["status"] == NULL ||
            $data["car"]["temuan"] == NULL
        );
    }

    public function store(Request $request)
    {
        $data = $request->json()->all();
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        if(!$this->invalidLogPengembalian($data)){
            $dokumen_car = DB::table('dokumen_car')->insertGetId(
                ['nama_project' => $data["car"]["nama_project"]["id"],
                'jenis' => $data["car"]["jenis"],
                'divisi' => $data["car"]["divisi"],
                'standar' => $data["car"]["standar"],
                'nomor' => $data["car"]["nomor"],
                'tanggal' => $data["car"]["tanggal"],
                'status' => "buat_temuan_auditor",
                'created_at' => $mytimenew->toDateTimeString(),
                'updated_at' => $mytimenew->toDateTimeString()]);

            $car_temuan = DB::table('car_temuan')->insertGetId(
                ['id_car' => $dokumen_car,
                'temuan' => $data["car"]["temuan"],
                'auditor' => $request->session()->get('id'),
                'status' => $data["car"]["status"],
                'created_at' => $mytimenew->toDateTimeString(),
                'updated_at' => $mytimenew->toDateTimeString()]);

            $user = User::find($request->session()->get('id'));
            $log_pengembalian = DB::table('log_book_pengembalian_prototypes')->insertGetId(
            ['tanggal' => $data["logBook"]["tanggal"],
            'item' => $data["logBook"]["item"]["id"],
            'jumlah' => $data["logBook"]["jumlah"],
            'batch' => $data["logBook"]["batch"],
            'divisi_melapor' => $user->divisi_id,
            'divisi_terlapor' => $data["logBook"]["divisi_terlapor"],
            'yang_menyerahkan' => $request->session()->get('id'),
            'yang_menerima' => $data["logBook"]["yang_menerima"]["id"],
            'dok_car' => $dokumen_car,
            'status_pengembalian' => "belum_diterima",
            'created_at' => $mytimenew->toDateTimeString(),
            'updated_at' => $mytimenew->toDateTimeString()]);

            DB::table('dokumen_car')
            ->where('id', $dokumen_car)
            ->update(['id_pengembalian' => $log_pengembalian]);

            if($log_pengembalian != NULL || $dokumen_car != NULL || $car_temuan != NULL){
                $this->createAktivitasUser('Membuat Log Book Pengembalian Prototype', 'Log Book Pengembalian Prototype', $request->session()->get('id'), $log_pengembalian);
                return response()
                    ->json([
                        LogBookPengembalianPrototype::find($log_pengembalian),
                        DokumenCar::find($dokumen_car),
                        CarTemuan::find($car_temuan)]);
            }


        }
        else{
            return response()
                    ->json(['status' => 'FAILED','reason'=>'Empty request']);
        }
    }

    private function createAktivitasUser($aktivitas, $kode, $idUser, $idAktivitas){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        DB::table('aktivitas_user')->insert([
            'jenis_aktivitas' => $aktivitas,
            'id_user' => $idUser,
            'kode_aktivitas' => KodeAktivitas::where('jenis_aktivitas', $kode)->first()->id,
            'id_aktivitas' => $idAktivitas,
            'created_at' => $mytimenew->toDateTimeString(),
            'updated_at' => $mytimenew->toDateTimeString(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LogBookPengembalianPrototype  $logBookPengembalianPrototype
     * @return \Illuminate\Http\Response
     */
    public function show(LogBookPengembalianPrototype $logBookPengembalianPrototype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LogBookPengembalianPrototype  $logBookPengembalianPrototype
     * @return \Illuminate\Http\Response
     */
    public function edit(LogBookPengembalianPrototype $logBookPengembalianPrototype)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LogBookPengembalianPrototype  $logBookPengembalianPrototype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');

        $log_pengembalian = LogBookPengembalianPrototype::find($request->id_item);

        if(!$this->divisiCheck($request)){
            if($log_pengembalian != NULL){

                if($log_pengembalian->status_pengembalian == 'belum_diterima'){
                    DB::table('log_book_pengembalian_prototypes')
                    ->where('log_book_pengembalian_prototypes.id', $request->id_item)
                    ->update($request->except('id_item') + ['updated_at' => $mytimenew->toDateTimeString()]);


                    $this->createAktivitasUser('Mengubah Log Book Pengembalian Prototype', 'Log Book Pengembalian Prototype', $request->session()->get('id'), $request->id_item);

                    return response()->json(['ID'=> $log_pengembalian->id, 'Success'=> 'Sukses mengubah'], 200);
                }
                else{
                    return response()->json(['ID'=> $log_pengembalian->id, 'ERROR'=> 'Gagal mengubah, Pengembalian sudah diterima'], 200);
                }
            }
            else{
                return response()->json(['ID'=> $log_pengembalian->id, 'ERROR'=> 'Gagal mengubah, Data yang Anda Cari Tidak Ada'], 200);
            }
        }
        else{
            return response()->json(['ID'=> $log_pengembalian->id, 'ERROR'=> 'Gagal mengubah, Anda tidak berasal dari divisi manufaktur'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LogBookPengembalianPrototype  $logBookPengembalianPrototype
     * @return \Illuminate\Http\Response
     */
    public function destroy(LogBookPengembalianPrototype $logBookPengembalianPrototype)
    {
        //
    }

    private function divisiCheck(Request $request){
        $user = User::find($request->session()->get('id'));
        return ($user == "Manufaktur");
    }
    public function acceptPengembalian(Request $request){
        $log_pengembalian = LogBookPengembalianPrototype::find($request->idPengembalian);

        if(!$this->divisiCheck($request)){
            if ($log_pengembalian != NULL){
                if(strtolower($log_pengembalian->status_pengembalian) == 'belum_diterima'){
                    $this->ubahStatusLogPengembalian($request->idPengembalian, 'sudah_diterima',
                    $request->session()->get('id'), 'yang_menerima');

                    $this->createAktivitasUser('Menerima Pengembalian Prototype', 'Log Book Pengembalian Prototype', $request->session()->get('id'), $log_pengembalian->id);
                    return response()->json(['ID'=> $log_pengembalian->id, 'status'=> 'Nota antar divisi berhasil disetujui'], 200);
                }
                else{
                    return response()->json(['ID'=> $log_pengembalian[0]->id, 'status'=> 'Gagal mengubah, prototype sudah diterima'], 200);
                }

            }
            else{
                return response()->json(['ID'=> $log_pengembalian[0]->id, 'status'=> 'Gagal mengubah, data not found'], 200);
            }
        }
        else{
            return response()->json(['status'=> 'Gagal mengubah, anda bukan dari divisi Manufaktur'], 200);
        }
    }

    private function ubahStatusLogPengembalian($idPengembalian, $newStatus, $idUser, $test){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');

        $nota = LogBookPengembalianPrototype::updateOrCreate(
            ['log_book_pengembalian_prototypes.id' => $idPengembalian],
            ['status_pengembalian'=>$newStatus,$test=>$idUser,'updated_at' => $mytimenew->toDateTimeString()]
        );

        return $nota;
    }
}
