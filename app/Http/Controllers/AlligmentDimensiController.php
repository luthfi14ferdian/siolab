<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LaporanAllignmentDimensi;
use App\Helpers\FilePathUtils;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AlligmentDimensiController extends Controller
{
    public function index(Request $request){
        if($request->tipe=='allignment'){
            $dataLaporan = LaporanAllignmentDimensi::where('id_laporan', $request->id_laporan)->where('tipe',$request->tipe)->get()->toArray();
            return response()->json([
                'status'=>1,
                'data'=> $dataLaporan,
            ]);
        }

        else{
            $dataLaporan = LaporanAllignmentDimensi::where('id_laporan', $request->id_laporan)->where('tipe',$request->tipe)->get()->toArray();
            return response()->json([
                'status'=>1,
                'data'=> $dataLaporan,
            ]);
        }
    }

    public function createAllignmentDimensi(Request $request){
        $name= FilePathUtils::getRandomFileName($request->file('gambar_file'));
        $path= Storage::putFileAs('allignment_dimensi', new File($request->gambar_file), $name);

        $filelaporan = new LaporanAllignmentDimensi();
        $filelaporan->id_laporan=$request->id_laporan;
        $filelaporan->nama_file = $name;
        $filelaporan->gambar_path = $path;
        $filelaporan->tipe=$request->tipe;
        $filelaporan->keterangan = $request->keterangan;
        $filelaporan->created_at =  date("Y-m-d H:i:s");

        $filelaporan->save();

        return response()->json([
            'status'=>1,
            'message'=>'Data Berhasil ditambahkan',
            'data' => $filelaporan,
        ]);
    }

    public function editAlligmentDimensi(Request $request){
        $file_laporan= LaporanAllignmentDimensi::find($request->id_item);

        if($request->gambar_file== null ){
            $file_laporan->keterangan =$request->keterangan;
            $file_laporan->updated_at =  date("Y-m-d H:i:s");
            $file_laporan->save();

            return response()->json([
                "status" => 1,
                "message" => 'Data gambar berhasil diubah',
                'data'=>$file_laporan,
            ],200);
        }

        elseif($request->gambar_file !=null ){
            Storage::disk('public')->delete( $file_laporan->gambar_path);

            $name= FilePathUtils::getRandomFileName($request->file('gambar_file'));
            $path= Storage::putFileAs('allignment_dimensi', new File($request->gambar_file), $name);

            $file_laporan->keterangan =$request->keterangan;
            $file_laporan->nama_file = $name;
            $file_laporan->gambar_path = $path;
            $file_laporan->updated_at = date("Y-m-d H:i:s");
            $file_laporan->save();

            return response()->json([
                "status" => 1,
                "message" => 'Data gambar berhasil diubah',
                'data'=>$file_laporan,
            ],200);
        }

    }

    public function deleteAllignmentDimensi(Request $request){
        $file_laporan= LaporanAllignmentDimensi::find($request->id_item);
        Storage::disk('public')->delete($file_laporan->gambar_path);
        $file_laporan->delete();

        return response()->json([
            "status"=>1,
            "message" => 'Data gambar berhasil dihapus!',
        ],200);
    }

}
