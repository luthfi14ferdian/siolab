<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\LogBookProduksiManufaktur;
use App\mesin;
use App\material;
use App\User;
use App\project;
use App\KodeAktivitas;
use Illuminate\Http\Request;

class LogBookProduksiManufakturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if($request->id_item == NULL){
            $all_log_produksi = DB::table('log_book_produksi_manufakturs')
                ->where('nama_item', $request->id_proyek)
                ->join('projects', 'log_book_produksi_manufakturs.nama_item', '=', 'projects.id')
                ->join('materials', 'log_book_produksi_manufakturs.material', '=', 'materials.id')
                ->select('log_book_produksi_manufakturs.id', 'log_book_produksi_manufakturs.status',
                'log_book_produksi_manufakturs.tanggal', 'projects.nama_project as nama_item', 'materials.jenis_material')
                ->get();

            $log_produksi = array_chunk($all_log_produksi->toArray(), 10);
            if (count($log_produksi) <= (($request->start)-1) ) {
                $log_produksi = [];
            }
            else {
                $log_produksi = $log_produksi[($request->start)-1];
            }

            $total_item = count($all_log_produksi);
            $current_page = (int)$request->start;
            $total_item_per_page = count($log_produksi);

            return response()->json([
                "status" => 1,
                "data" => [
                    "item" => $log_produksi
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_pr_page" => $total_item_per_page,
                ],
            ]);
        }
        else{
            return response()->json($this->getDetailLogProduksi($request));
        }
    }

    public function getDetailLogProduksi(Request $request){
        $log_produksi = DB::table('log_book_produksi_manufakturs')
            ->where('log_book_produksi_manufakturs.id', '=', $request->id_item)
            ->get();

        $log_produksi[0]->material = Material::find($log_produksi[0]->material,
        ['id', 'jenis_material', 'satuan', 'keterangan']);
        $log_produksi[0]->mesin = Mesin::find($log_produksi[0]->mesin,
        ['id', 'nama_mesin', 'keterangan']);
        $log_produksi[0]->nama_item = Project::find($log_produksi[0]->nama_item);
        $log_produksi[0]->dikerjakan_oleh = User::find($log_produksi[0]->dikerjakan_oleh
        , ['id', 'username', 'last_name', 'first_name']);
        return $log_produksi;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function invalidLogProduksi(Request $request){
        return (
            $request->no_batch == NULL ||
            $request->tanggal == NULL ||
            $request->nama_item == NULL ||
            $request->material == NULL ||
            $request->mesin == NULL
        );
    }

    private function dataCheck($jumlah_prod, $jumlah_trial, $jumlah_lose, $jumlah_exist){
        return (
            $jumlah_prod == NULL ||
            $jumlah_trial == NULL ||
            $jumlah_lose == NULL ||
            $jumlah_exist == NULL
        );
    }

    public function store(Request $request)
    {
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        if(!$this->invalidLogProduksi($request)){
            if(!$this->dataCheck($request->jumlah_prod, $request->jumlah_trial, $request->jumlah_lose, $request->jumlah_exist)){
                $log_produksi = DB::table('log_book_produksi_manufakturs')->insertGetId($request->all() +
                ['dikerjakan_oleh'=>$request->session()->get('id'), 'created_at' => $mytimenew->toDateTimeString(),
                'updated_at' => $mytimenew->toDateTimeString(), 'status'=> 'selesai']);
            }
            else{
                $log_produksi = DB::table('log_book_produksi_manufakturs')->insertGetId($request->all() +
                ['dikerjakan_oleh'=>$request->session()->get('id'), 'created_at' => $mytimenew->toDateTimeString(),
                'updated_at' => $mytimenew->toDateTimeString(), 'status'=> 'sedang_diproses']);
            }

            $this->createAktivitasUser('Membuat Log Book Produksi Manufaktur', 'Log Book Produksi Manufaktur',
            $request->session()->get('id'), $log_produksi);

            if($log_produksi != NULL){
                return response()
                    ->json(['status' => 'data has been inserted',
                        DB::table('log_book_produksi_manufakturs')
                        ->where('log_book_produksi_manufakturs.id', $log_produksi)
                        ->get()]);
            }
        }
        else{
            return response()
                    ->json(['status' => 'FAILED','reason'=>'Empty request']);
        }
    }

    private function createAktivitasUser($aktivitas, $kode, $idUser, $idAktivitas){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        DB::table('aktivitas_user')->insert([
            'jenis_aktivitas' => $aktivitas,
            'id_user' => $idUser,
            'kode_aktivitas' => KodeAktivitas::where('jenis_aktivitas', $kode)->first()->id,
            'id_aktivitas' => $idAktivitas,
            'created_at' => $mytimenew->toDateTimeString(),
            'updated_at' => $mytimenew->toDateTimeString(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LogBookProduksiManufaktur  $logBookProduksiManufaktur
     * @return \Illuminate\Http\Response
     */
    public function show(LogBookProduksiManufaktur $logBookProduksiManufaktur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LogBookProduksiManufaktur  $logBookProduksiManufaktur
     * @return \Illuminate\Http\Response
     */
    public function edit(LogBookProduksiManufaktur $logBookProduksiManufaktur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LogBookProduksiManufaktur  $logBookProduksiManufaktur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $logBookProduksiManufaktur = DB::table('log_book_produksi_manufakturs')
            ->where('log_book_produksi_manufakturs.id', $request->idLog)
            ->get();

        if($logBookProduksiManufaktur[0]->status == 'sedang_diproses'){
            if(!$this->dataCheck($logBookProduksiManufaktur[0]->jumlah_prod, $logBookProduksiManufaktur[0]->jumlah_trial,
            $logBookProduksiManufaktur[0]->jumlah_lose, $logBookProduksiManufaktur[0]->jumlah_exist)){
                DB::table('log_book_produksi_manufakturs')
                ->where('log_book_produksi_manufakturs.id', $request->idLog)
                ->update($request->except('idLog') + ['status'=> 'selesai', 'updated_at' => $mytimenew->toDateTimeString()]);
            }
            else{
                DB::table('log_book_produksi_manufakturs')
                ->where('log_book_produksi_manufakturs.id', $request->idLog)
                ->update($request->except('idLog') + ['status'=> 'selesai', 'updated_at' => $mytimenew->toDateTimeString()]);
            }

            $this->createAktivitasUser('Mengubah Log Book Produksi Manufaktur', 'Log Book Produksi Manufaktur',
            $request->session()->get('id'), $request->idLog);

            return response()->json(['ID'=> $logBookProduksiManufaktur[0]->id, 'Success'=> 'Sukses mengubah'], 200);
        }
        else{
            return response()->json(['ID'=> $logBookProduksiManufaktur[0]->id, 'ERROR'=> 'Gagal mengubah, Data yang Anda Cari Tidak Ada'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LogBookProduksiManufaktur  $logBookProduksiManufaktur
     * @return \Illuminate\Http\Response
     */
    public function destroy(LogBookProduksiManufaktur $logBookProduksiManufaktur)
    {
        //
    }
}
