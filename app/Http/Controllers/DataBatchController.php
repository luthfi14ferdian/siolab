<?php

namespace App\Http\Controllers;
use App\DataBatch;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DataBatchController extends Controller
{
    public function index(Request $request){
        $dataBatch = DataBatch::where('id_project',$request->id_project)->paginate(10);
        $dataBatch->makeHidden(['diperiksa_oleh','disetujui_oleh','alasan_penolakan']);

        $items = $dataBatch->items();
        $total_item =$dataBatch->total();
        $current_page = $dataBatch->currentPage();
        $total_item_per_page = $dataBatch->perPage();


        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $items
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }
    public function addBatchData(Request $request){
        $data_batch= new DataBatch();
        $data_batch->id_project= $request->id_project;
        $data_batch->no_batch=$request->no_batch;
        $data_batch->status="Baru";
        $data_batch->jumlah_laporan=0;
        $data_batch->judgement=$request->judgement;
        $data_batch->jumlah_sampel = $request->jumlah_sampel;
        $data_batch->kriteria_sampel_diterima=$request->kriteria_diterima;
        $data_batch->kriteria_sampel_ditolak=$request->kriteria_ditolak;
        $data_batch->sampel_ditolak=0;
        $data_batch->sampel_diterima=0;
        $data_batch->created_at= date("Y-m-d H:i:s");

        $data_batch->save();

        return response()->json([
            "status" => 1,
            "message" => 'Data batch berhasil ditambahkan'
        ], 200);
    }

    public function editBatchData(Request $request){
        $data_batch= DataBatch::find($request->id);
        $data_batch->no_batch=$request->no_batch;
        $data_batch->jumlah_sampel = $request->jumlah_sampel;
        $data_batch->kriteria_sampel_diterima=$request->kriteria_diterima;
        $data_batch->kriteria_sampel_ditolak=$request->kriteria_ditolak;
        $data_batch->updated_at= date("Y-m-d H:i:s");

        $data_batch->save();

        return response()->json([
            "status" => 1,
            "message" => 'Data batch berhasil diubah'
        ], 200);
    }

    public function detailBatch(Request $request){
        $data_batch= DataBatch::find($request->id);
        $diperiksa_oleh=null;
        $disetujui_oleh=null;

        if(User::find($data_batch->diperiksa_oleh)){
            $diperiksa_oleh= User::find($data_batch->diperiksa_oleh);
            $diperiksa_oleh->makeHidden(['divisi_id','role_id','role_project','is_approve']);
        }

        if (User::find($data_batch->disetujui_oleh)){
            $disetujui_oleh=User::find($data_batch->disetujui_oleh);
            $disetujui_oleh->makeHidden(['divisi_id','role_id','role_project','is_approve']);
        }

        $data_batch->diperiksa_oleh = $diperiksa_oleh;
        $data_batch->disetujui_oleh = $disetujui_oleh;

        return response()->json([
            "status" => 1,
            "message" => 'Success',
            "data"=> $data_batch,
        ], 200);
    }

    public function deleteBatch(Request $request){
        $data_batch= DataBatch::find($request->id);

        if($data_batch->status !='Baru'){
            return response()->json([
                "status" => 0,
                "message" => 'Batch ini tidak bisa dihapus!',
            ], 400);
        }

        $data_batch->delete();

        return response()->json([
            "status" => 1,
            "message" => 'Data batch berhasil dihapus',
        ],200);

    }

    public function approveBatch(Request $request){
        $batch = DataBatch::find($request->id_batch);
        if($request->tipe == "diperiksa"){
            $batch->diperiksa_oleh = Auth::user()->id;
            $batch->status = "Diperiksa";
            $batch->save();

            return response()->json([
                "status" => 1,
                "message" => 'Batch berhasil diperiksa!',
            ], 200);
        }

        elseif($request->tipe == "disetujui"){
            $batch->disetujui_oleh = Auth::user()->id;
            $batch->status = "Disetujui";
            $batch->save();

            return response()->json([
                "status" => 1,
                "message" => 'Batch berhasil disetujui!',
            ], 200);
        }
    }

    public function rejectBatch(Request $request){
        $batch = DataBatch::find($request->id_batch);
        $batch->status= "Ditolak";
        $batch->alasan_penolakan = $request->alasan_penolakan;
        $batch->save();

        return response()->json([
            "status" => 1,
            "message" => 'Batch berhasil ditolak!',
        ], 200);
    }

    public function setJudgement(Request $request){
        $batch = DataBatch::find($request->id_batch);

        $batch->judgement= $request->judgement;
        $batch->note_judgement=$request->note_judgement;

        $batch->save();

        return response()->json([
            "status" => 1,
            "message" => 'Judgement berhasil diset!',
        ], 200);

    }
}
