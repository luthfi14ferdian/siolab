<?php

namespace App\Http\Controllers;

use App\ParameterPemrosesan;
use Illuminate\Http\Request;

class ParameterPemrosesanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ParameterPemrosesan  $parameterPemrosesan
     * @return \Illuminate\Http\Response
     */
    public function show(ParameterPemrosesan $parameterPemrosesan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ParameterPemrosesan  $parameterPemrosesan
     * @return \Illuminate\Http\Response
     */
    public function edit(ParameterPemrosesan $parameterPemrosesan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ParameterPemrosesan  $parameterPemrosesan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ParameterPemrosesan $parameterPemrosesan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ParameterPemrosesan  $parameterPemrosesan
     * @return \Illuminate\Http\Response
     */
    public function destroy(ParameterPemrosesan $parameterPemrosesan)
    {
        //
    }
}
