<?php

namespace App\Http\Controllers;

use App\DokumenDesainTransfer;
use DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DokumenDesainTransferExport;

class DokumenDesainTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allDokumenDesainTransfer = DB::table("dokumen_desain_transfers")
        ->select('dokumen_desain_transfers.*')
        ->where('id_proyek', $request->id_project)
        ->get();

        $dokumenDesainTransfer = array_chunk($allDokumenDesainTransfer->toArray(), 10);
        if (count($dokumenDesainTransfer) <= (($request->page)-1) ) {
            $dokumenDesainTransfer = [];
        }
        else {
            $dokumenDesainTransfer = $dokumenDesainTransfer[($request->page)-1];
        }

        $total_item = count($allDokumenDesainTransfer);
        $current_page = (int)$request->page;
        $total_item_per_page = count($dokumenDesainTransfer);
    
        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $dokumenDesainTransfer
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $dokumenDesainTransfer = new DokumenDesainTransfer($data);

        $dokumenDesainTransfer->save();

        return response()->json($dokumenDesainTransfer);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DokumenDesainTransfer  $dokumenDesainTransfer
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $dokumenDesainTransfer = DokumenDesainTransfer::find($request->id_item);

        if ($dokumenDesainTransfer) {
            return response()->json($dokumenDesainTransfer);
        }

        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DokumenDesainTransfer  $dokumenDesainTransfer
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $dokumenDesainTransfer = DokumenDesainTransfer::find($request->id_item);


        if ($dokumenDesainTransfer) {
            $dokumenDesainTransfer = $dokumenDesainTransfer->update($request->all());

            return response()->json(['Success' => "Data berhasil diubah"],200);
        }
        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    public function export_excel(Request $request)
	{
		return Excel::download(new DokumenDesainTransferExport($request->id_proyek), 'dokumen-desain-transfer.xlsx');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DokumenDesainTransfer  $dokumenDesainTransfer
     * @return \Illuminate\Http\Response
     */
    public function destroy(DokumenDesainTransfer $dokumenDesainTransfer)
    {
        //
    }
}
