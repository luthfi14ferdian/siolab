<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\project;
use App\Model\Desain\Fit3D;
use App\Model\Desain\Drawing;
use App\Model\Desain\SpesifikasiProduk;



class DesainOutputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $index = [];
        
        $fit3D = Fit3D::select('fit3_d_s.*')->where('id_proyek',$id)->get();
        $drawing = Drawing::select('drawings.*')->where('id_proyek',$id)->get();
        $spesifikasiProduk = SpesifikasiProduk::select('spesifikasi_produks.*')->where('id_proyek',$id)->get();

        $index["fit3D"] = $fit3D;
        $index["drawing"] = $drawing;
        $index["spesifikasiProduk"] = $spesifikasiProduk;

        return response()->json($index);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
