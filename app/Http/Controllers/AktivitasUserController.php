<?php

namespace App\Http\Controllers;

use App\AktivitasUser;
use Illuminate\Http\Request;
use App\User;
use App\KodeAktivitas;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AktivitasUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AktivitasUser  $aktivitasUser
     * @return \Illuminate\Http\Response
     */
    public function show(AktivitasUser $aktivitasUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AktivitasUser  $aktivitasUser
     * @return \Illuminate\Http\Response
     */
    public function edit(AktivitasUser $aktivitasUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AktivitasUser  $aktivitasUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AktivitasUser $aktivitasUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AktivitasUser  $aktivitasUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(AktivitasUser $aktivitasUser)
    {
        //
    }
    public function getAllAktivitas(Request $request){
        $all_nota = DB::table('aktivitas_user')
            ->get();
        foreach ($all_nota as $a){
            $a->id_user = User::find($a->id_user
            , ['id', 'username', 'last_name', 'first_name']);
            $a->kode_aktivitas = KodeAktivitas::find($a->kode_aktivitas, ['id', 'jenis_aktivitas']);
        }
        return response()->json($all_nota);
    }
}
