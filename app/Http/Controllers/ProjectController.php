<?php

namespace App\Http\Controllers;

use App\project;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Divisi;
use App\JenisProject;
use App\role_project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getListJenisProject(Request $request){
        $list_jenis_project = JenisProject::all();

        return response()->json([
            'status'=>1,
           'data' => $list_jenis_project,
        ]);
    }

    public function getListProject(Request $request)
    {
        $list_project = project::where('nama_project',$request->id_jenis_project)->paginate(10);
        $list_project->makeHidden(['nomor_batch_awal','pic_desain','pic_material'
        ,'pic_manufaktur','pic_surface_treatment','pic_metrologi','pic_biomech','pic_biocomp']);

        foreach($list_project as $project){
            $project_leader= User::find($project->project_leader);
            $project_leader->makeHidden(['divisi_id','role_id','role_project','is_approve']);
            $project->nama_project = JenisProject::find($project->nama_project);
            $project->divisi_saat_ini = Divisi::find($project->divisi_saat_ini);
            $project->project_leader = $project_leader;
        }

        $items = $list_project->items();
        $total_item = $list_project->total();
        $current_page = $list_project->currentPage();
        $total_item_per_page = $list_project->perPage();

        return response()->json([
            "status" => 1,
            "data" => [
                "item" =>$items
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createJenisProject(Request $request)
    {
        $project = new JenisProject();
        $project->jenis_project = $request->nama_project ;
        $project->save();
        return response()->json([
            'status'=>1,
            'message'=> 'Jenis project berhasil ditambahkan'
        ],200);
    }

    public function createProject(Request $request)
    {
        $project = new project();
        $project->nama_project= $request->id_jenis_project;
        $project->nomor_batch_awal =$request->nomor_batch;
        $project->current_nomor_batch=$request->nomor_batch;
        $project->project_leader= $request->project_leader;
        $project->tahapan = $request->tahapan;
        $project->divisi_saat_ini = 1;
        $project->pic_desain=$request->pic_desain;
        $project->pic_material=$request->pic_material;
        $project->pic_manufaktur=$request->pic_manufaktur;
        $project->pic_surface_treatment = $request->pic_surface_treatment;
        $project->pic_metrologi=$request->pic_metrologi;
        $project->pic_biomech=$request->pic_biomech;
        $project->pic_biocomp=$request->pic_biocomp;
        $project->created_at=date("Y-m-d H:i:s");
        $project->save();

        $this->addRoleProject();

        return response()->json([
            'status'=>1,
            'message'=> 'Project Baru berhasil ditambahkan'
        ],200);
    }

    public function addRoleProject(){
        $project = project::latest()->first();
        $project_leader = $project->project_leader;
        $list_user = [(int)$project->pic_desain, (int)$project->pic_material,
        (int)$project->pic_manufaktur,(int)$project->pic_metrologi,
        (int)$project->pic_biocomp,(int)$project->pic_surface_treatment,  (int)$project->pic_biomech];

        for ($x = 0; $x <= 6; $x++){
            $user= $list_user[$x];
            $role_project = new role_project();
            $role_project->id_project = $project->id;
            $role_project->id_user = $user;
            $role_project->id_role = 7;
            $role_project->created_at = date("Y-m-d H:i:s");
            $role_project->save();
        }

        $role_project = new role_project();
        $role_project->id_project = $project->id;
        $role_project->id_user = $project_leader;
        $role_project->id_role = 8;
        $role_project->created_at = date("Y-m-d H:i:s");
        $role_project->save();

    }

    public function getDetailProject ($id){
        $project = Project::find($id);
        $project->makeVisible('created_at');

        $project_rep = $project->replicate();
        $project_rep->nama_project = JenisProject::find($project->nama_project);
        $project_rep->divisi_saat_ini = Divisi::find($project->divisi_saat_ini);
        $project_rep->project_leader = User::find($project->project_leader)->makeHidden(['role_id','role_project','is_approve']);
        $project_rep->pic_desain = User::find($project->pic_desain)->makeHidden(['role_id','role_project','is_approve']);
        $project_rep->pic_material = User::find($project->pic_material)->makeHidden(['role_id','role_project','is_approve']);
        $project_rep->pic_manufaktur = User::find($project->pic_manufaktur)->makeHidden(['role_id','role_project','is_approve']);
        $project_rep->pic_surface_treatment = User::find($project->pic_surface_treatment)->makeHidden(['role_id','role_project','is_approve']);
        $project_rep->pic_metrologi = User::find($project->pic_metrologi)->makeHidden(['role_id','role_project','is_approve']);
        $project_rep->pic_biomech = User::find($project->pic_biomech)->makeHidden(['role_id','role_project','is_approve']);
        $project_rep->pic_biocomp = User::find($project->pic_biocomp)->makeHidden(['role_id','role_project','is_approve']);

        return response()->json([
            'status'=>1,
            'data' => $project_rep,
         ]);

    }

    public function chooseProject(Request $request){
        $project = project::find($request->id_project);
        $project->makeHidden(['project_leader','pic_desain','pic_material','pic_manufaktur',
        'pic_surface_treatment','pic_biomech','pic_metrologi','pic_biocomp']);

        $userRoleProject= role_project::where('id_project',$request->id_project)
        ->where('id_user','=',Auth::user()->id)
        ->orderBy('id','desc')
        ->first();

        $user = User::where('id',Auth::user()->id)->first();
        $user->role_project= $userRoleProject->id_role;
        $user->save();

        return response()->json([
            'status'=>1,
            'data' => $project,
        ]);
    }

    public function getUser(){
        $alluser = User::all()->where('is_approve',1);
        $alldivisi= Divisi::all();

        $user = [];
        $divisi = [];

        foreach($alldivisi as $divisis){
            array_push($divisi, $divisis->nama);
        }


        foreach($alluser as $users){
            $name= $users->first_name.' '.$users->last_name. '-'.$divisi[$users->divisi_id-1];
            $data_user= [$users->id,$name];
            $a = array_push($user,$data_user);
        }

        return response()->json([
            'status'=>1,
            'data'=> $user
        ]);
    }
}
