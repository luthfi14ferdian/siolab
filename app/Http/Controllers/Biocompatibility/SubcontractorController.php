<?php

use Illuminate\Http\Request;
namespace App\Http\Controllers\Biocompatibility;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Biocompatibility\SubcontractorBiocomp;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SubcontractorController extends Controller
{
    public function index(Request $request)
    {
        $queryKode = $request->nama_pemasok;
        $status = $request->status;
        $queryKode = isset($queryKode) ? trim($queryKode) : '';

        if ($queryKode === '' && $status ==='') {
            $queryDb = DB::table("subcontractor_biocomps")
            ->select('subcontractor_biocomps.*');
        }

        elseif($queryKode=='' && $status != ''){
            $queryDb = DB::table("subcontractor_biocomps")
            ->where('status', 'LIKE', $status.'%');
        }

        elseif($queryKode!='' && $status != ''){
            $queryDb = DB::table("subcontractor_biocomps")
            ->where('status', 'LIKE', $status.'%')
            ->where('nama_pemasok', 'LIKE', '%'.$queryKode.'%');
        }

        else {
            $queryDb = DB::table("subcontractor_biocomps")
            ->where('nama_pemasok', 'LIKE', '%'.$queryKode.'%');
        }

        $paginate = $queryDb->paginate(10);

        return response()->json([
            "status" => 1,
            "message"=>'Success',
            "data" => [
                "item" => $paginate->items(),
            ],
            "pagination" => [
                "current_page" => $paginate->currentPage(),
                "total_item" => $paginate->total(),
                "items_per_page" => $paginate->perPage(),
            ],
        ]);
    }

    public function addSubcontractor(Request $request){
        $subcontractor = new SubcontractorBiocomp();
        $subcontractor->no = $request->no;
        $subcontractor->tanggal_seleksi = $request->tanggal_seleksi;
        $subcontractor->nama_pemasok = $request->nama_pemasok;
        $subcontractor->alamat = $request->alamat;
        $subcontractor->kontak = $request->kontak;
        $subcontractor->email = $request->email;
        $subcontractor->id_project = $request->id_project;
        $subcontractor->jenis_jasa = $request->jenis_jasa;
        $subcontractor->status="Baru";
        $subcontractor->total_penilaian=0;
        $subcontractor->disusun_oleh=Auth::user()->id;
        $subcontractor->save();

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan",
            "data" => $subcontractor,
        ]);
    }

    public function getSubcontractor(Request $request){
        $subcontractor= SubcontractorBiocomp::find($request->id);

        if($subcontractor){
            $disusun_oleh = User::find($subcontractor->disusun_oleh)->makeHidden(['role_project','role_id','divisi_id','is_approve']);
            $disetujui_oleh = User::find($subcontractor->disetujui_oleh);
            if($disetujui_oleh){
                $disetujui_oleh->makeHidden(['role_project','role_id','divisi_id','is_approve']);
            }
            $subcontractor->disusun_oleh = $disusun_oleh;
            $subcontractor->disetujui_oleh = $disetujui_oleh;
            return response()->json([
                "status" => 1,
                "message"=>"Success",
                "data" => $subcontractor,
            ]);
        }
        else{
            return response()->json([
                "status" => 0,
                "message"=>"Data tidak ditemukan"
            ]);
        }
    }

    public function editSubcontractor(Request $request){
        $subcontractor = SubcontractorBiocomp::find($request->id);

        if($subcontractor->status != "Baru"){
            return response()->json([
                "status" => 0,
                "message" => "Maaf data tidak bisa diubah",
            ]);
        }

        else{
            $subcontractor->no = $request->no;
            $subcontractor->tanggal_seleksi = $request->tanggal_seleksi;
            $subcontractor->nama_pemasok = $request->nama_pemasok;
            $subcontractor->alamat = $request->alamat;
            $subcontractor->kontak = $request->kontak;
            $subcontractor->email = $request->email;
            $subcontractor->jenis_jasa = $request->jenis_jasa;
            $subcontractor->disusun_oleh=Auth::user()->id;

            $subcontractor->save();

            return response()->json([
                "status" => 1,
                "message" => "Data berhasil diubah",
                "data" => $subcontractor,
            ]);
        }
    }

    public function deleteSubcontractor(Request $request){
        $subcontractor = SubcontractorBiocomp::find($request->id);
        if($subcontractor->status != "Baru"){
            return response()->json([
                "status" => 0,
                "message" => "Maaf data tidak bisa dihapus",
            ]);
        }

        else{
            $subcontractor->delete();
            return response()->json([
                "status" => 1,
                "message" => "Data berhasil dihapus",
            ]);
        }
    }

    public function approveSubcontractor(Request $request){
        $subcontractor = SubcontractorBiocomp::find($request->id);

        $subcontractor->disetujui_oleh = Auth::user()->id;
        $subcontractor->status = "Disetujui";
        $subcontractor->save();
        return response()->json([
            "status" => 1,
            "message" => "Subcontractor berhasil disetujui",
            "data"=>$subcontractor,
        ]);
    }

}
