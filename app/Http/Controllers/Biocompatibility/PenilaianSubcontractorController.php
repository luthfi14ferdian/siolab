<?php

namespace App\Http\Controllers\Biocompatibility;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Biocompatibility\SubcontractorBiocomp;
use App\Model\PenilaianSubcontractor;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PenilaianSubcontractorController extends Controller
{
    ######### PENILAIAN SUBCONTACTOR #####################
    public function getPenilaianSubcontractor(Request $request){
        $penilaian_subcontractor = PenilaianSubcontractor::where('id_subcontractor',$request->id_subcontractor)->paginate(20);
        if($penilaian_subcontractor){
            return response()->json([
                "status" => 1,
                "message"=>'Success',
                "data" => [
                    "item" => $penilaian_subcontractor->items(),
                ],
                "pagination" => [
                    "current_page" => $penilaian_subcontractor->currentPage(),
                    "total_item" => $penilaian_subcontractor->total(),
                    "items_per_page" => $penilaian_subcontractor->perPage(),
                ],
            ]);
        }
        else{
            return response()->json([
                "status" => 0,
                "message" => "Data kosong",
            ]);
        }
    }

    public function addPenilaianSubcontractor(Request $request){
        $subcontractor = SubcontractorBiocomp::find($request->id_subcontractor);

        if($subcontractor->disetujui_oleh!=null){
            return response()->json([
                "status" => 0,
                "message" => "Penilaian tidak bisa ditambah karena sudah disetujui",
            ]);
        }

        $penilaian_subcontractor = new PenilaianSubcontractor();
        $penilaian_subcontractor->id_subcontractor= $request->id_subcontractor;
        $penilaian_subcontractor->kriteria_pemasok= $request->kriteria_pemasok;
        $penilaian_subcontractor->nilai_pemasok= $request->nilai_pemasok;
        $penilaian_subcontractor->save();

        $subcontractor->total_penilaian=  $subcontractor->total_penilaian+$penilaian_subcontractor->nilai_pemasok;
        $subcontractor->status = "Sedang direview";
        $subcontractor->save();

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan",
            "data" => $penilaian_subcontractor,
        ]);
    }

    public function finishPenilaianSubcontractor(Request $request){
        $subcontractor = SubcontractorBiocomp::find($request->id_subcontractor);
        $subcontractor->penilaian_finish="Yes";
        $subcontractor->save();
        return response()->json([
            "status" => 1,
            "message" => "Penilaian selesai dan lanjut ke tahap berikutnya",
        ]);
    }

    public function editPenilaianSubcontractor(Request $request){
        $penilaian_subcontractor = PenilaianSubcontractor::find($request->id);
        $subcontractor = SubcontractorBiocomp::find($penilaian_subcontractor->id_subcontractor);

        if($subcontractor->disetujui_oleh!=null){
            return response()->json([
                "status" => 0,
                "message" => "Penilaian tidak bisa diubah karena sudah disetujui",
            ]);
        }
        else{
            $subcontractor->total_penilaian= $subcontractor->total_penilaian- $penilaian_subcontractor->nilai_pemasok;

            $penilaian_subcontractor->kriteria_pemasok= $request->kriteria_pemasok;
            $penilaian_subcontractor->nilai_pemasok= $request->nilai_pemasok;
            $penilaian_subcontractor->save();

            $subcontractor->total_penilaian= $subcontractor->total_penilaian+ $request->nilai_pemasok;
            $subcontractor->save();

            return response()->json([
                "status" => 1,
                "message" => "Data berhasil diubah",
                "data" => $penilaian_subcontractor,
            ]);
        }
    }

    public function deletePenilaianSubcontractor(Request $request){
        $penilaian_subcontractor = PenilaianSubcontractor::find($request->id);
        $subcontractor = SubcontractorBiocomp::find($penilaian_subcontractor->id_subcontractor);

        if($subcontractor->disetujui_oleh!=null){
            return response()->json([
                "status" => 0,
                "message" => "Penilaian tidak bisa dihapus karena sudah disetujui",
            ]);
        }

        else{
            if($subcontractor->penilaian_finish=="Yes"){
                $all_penilaian = PenilaianSubcontractor::where('id_subcontractor',$penilaian_subcontractor->id_subcontractor)->get();
                $count_penilaian = count($all_penilaian);
                if($count_penilaian==1){
                    return response()->json([
                        "status" => 0,
                        "message" => "Penilaian tidak bisa dihapus, penialian tidak boleh kosong jika sudah diselesaikan!",
                    ]);
                }
                else{
                    $subcontractor->total_penilaian = $subcontractor->total_penilaian-$penilaian_subcontractor->nilai_pemasok;
                    $subcontractor->save();
                    $penilaian_subcontractor->delete();
                    return response()->json([
                        "status" => 1,
                        "message" => "Data berhasil dihapus",
                    ]);
                }
            }
            else{
                $all_penilaian = PenilaianSubcontractor::where('id_subcontractor',$penilaian_subcontractor->id_subcontractor)->get();
                $count_penilaian = count($all_penilaian);
                if($count_penilaian==1){
                    $subcontractor->total_penilaian = $subcontractor->total_penilaian-$penilaian_subcontractor->nilai_pemasok;
                    $subcontractor->status="Baru";
                    $subcontractor->save();
                    $penilaian_subcontractor->delete();
                    return response()->json([
                        "status" => 1,
                        "message" => "Data berhasil dihapus",
                    ]);
                }

                else{
                    $subcontractor->total_penilaian = $subcontractor->total_penilaian-$penilaian_subcontractor->nilai_pemasok;
                    $subcontractor->save();
                    $penilaian_subcontractor->delete();
                    return response()->json([
                        "status" => 1,
                        "message" => "Data berhasil dihapus",
                    ]);
                }
            }
        }
    }

     ######### PELAYANAN SUBCONTACTOR #####################
     public function setPelayananSubcontractor(Request $request){
        $subcontractor = SubcontractorBiocomp::find($request->id_subcontractor);

        if($subcontractor->disetujui_oleh!=null){
            return response()->json([
                "status" => 0,
                "message" => "Pelayanan tidak bisa diubah karena sudah disetujui",
            ]);
        }
        else{
            $subcontractor->pelayanan= $request->pelayanan;
            $subcontractor->save();
            return response()->json([
                "status" => 1,
                "message" => "Data berhasil disimpan",
                "data" => $subcontractor,
            ]);
        }
    }

    public function setKesimpulanSubcontractor(Request $request){
        $subcontractor = SubcontractorBiocomp::find($request->id_subcontractor);

        if($subcontractor->disetujui_oleh!=null){
            return response()->json([
                "status" => 0,
                "message" => "Kesimpulan tidak bisa diubah karena sudah disetujui",
            ]);
        }
        else{
            if($request->kesimpulan == "DITERIMA SEBAGAI PEMASOK"){
                $subcontractor->status="Dapat diterima";
                $subcontractor->kesimpulan= $request->kesimpulan;
                $subcontractor->save();
                return response()->json([
                    "status" => 1,
                    "message" => "Data berhasil disimpan",
                    "data" => $subcontractor,
                ]);
            }
            elseif($request->kesimpulan == "TIDAK DITERIMA SEBAGAI PEMASOK"){
                $subcontractor->status="Tidak Dapat diterima";
                $subcontractor->kesimpulan= $request->kesimpulan;
                $subcontractor->save();
                return response()->json([
                    "status" => 1,
                    "message" => "Data berhasil disimpan",
                    "data" => $subcontractor,
                ]);
            }
        }
    }
}
