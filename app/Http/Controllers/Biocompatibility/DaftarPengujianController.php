<?php

namespace App\Http\Controllers\Biocompatibility;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Biocompatibility\DaftarPengujian;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;

class DaftarPengujianController extends Controller
{
    public function index(Request $request){
        $queryKode = $request->search;
        $queryKode = isset($queryKode) ? trim($queryKode) : '';

        if ($queryKode === '') {
            $queryDb = DB::table("daftar_pengujians")
            ->select('daftar_pengujians.*');
        }

        else {
            $queryDb = DB::table("daftar_pengujians")
            ->where('nama_dokumen', 'LIKE', '%'.$queryKode.'%')
            ->orWhere('nomor_dokumen', 'LIKE', '%'.$queryKode.'%');
        }

        $paginate = $queryDb->paginate(10);

        return response()->json([
            "status" => 1,
            "message"=>'Success',
            "data" => [
                "item" => $paginate->items(),
            ],
            "pagination" => [
                "current_page" => $paginate->currentPage(),
                "total_item" => $paginate->total(),
                "items_per_page" => $paginate->perPage(),
            ],
        ]);
    }

    public function addPengujian(Request $request){
        $daftar_pengujian = new DaftarPengujian();
        $daftar_pengujian->nama_dokumen = $request->nama_dokumen;
        $daftar_pengujian->nomor_dokumen = $request->nomor_dokumen;
        $daftar_pengujian->tujuan_pengujian = $request->tujuan_pengujian;
        $daftar_pengujian->tanggal = $request->tanggal;
        $daftar_pengujian->tujuan = $request->tujuan;
        $daftar_pengujian->ruang_lingkup = $request->ruang_lingkup;
        $daftar_pengujian->kebijakan_umum = $request->kebijakan_umum;
        $daftar_pengujian->referensi = $request->referensi;
        $daftar_pengujian->tanggung_jawab = $request->tanggung_jawab;
        $daftar_pengujian->definisi = $request->definisi;
        $daftar_pengujian->disiapkan_oleh= Auth::user()->id;
        $daftar_pengujian->status = "Baru";

        $daftar_pengujian->save();

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan",
            "data" => $daftar_pengujian,
        ]);
    }

    public function detailPengujian(Request $request){
        $daftar_pengujian= DaftarPengujian::find($request->id);

        if($daftar_pengujian){
            $disiapkan_oleh = User::find($daftar_pengujian->disiapkan_oleh)->makeHidden(['role_project','role_id','divisi_id','is_approve']);
            $diperiksa_oleh = User::find($daftar_pengujian->diperiksa_oleh);
            $disetujui_oleh = User::find($daftar_pengujian->disetujui_oleh);
            if($diperiksa_oleh){
                $diperiksa_oleh->makeHidden(['role_project','role_id','divisi_id','is_approve']);
                if($disetujui_oleh){
                    $disetujui_oleh->makeHidden(['role_project','role_id','divisi_id','is_approve']);
                }
            }
            $daftar_pengujian->disiapkan_oleh = $disiapkan_oleh;
            $daftar_pengujian->diperiksa_oleh = $diperiksa_oleh;
            $daftar_pengujian->disetujui_oleh = $disetujui_oleh;
            return response()->json([
                "status" => 1,
                "message"=>"Success",
                "data" => $daftar_pengujian,
            ]);
        }
        else{
            return response()->json([
                "status" => 0,
                "message"=>"Data tidak ditemukan"
            ]);
        }
    }

    public function editPengujian(Request $request){
        $daftar_pengujian = DaftarPengujian::find($request->id);
        $daftar_pengujian->nama_dokumen = $request->nama_dokumen;
        $daftar_pengujian->nomor_dokumen = $request->nomor_dokumen;
        $daftar_pengujian->tujuan_pengujian = $request->tujuan_pengujian;
        $daftar_pengujian->tanggal = $request->tanggal;
        $daftar_pengujian->tujuan = $request->tujuan;
        $daftar_pengujian->ruang_lingkup = $request->ruang_lingkup;
        $daftar_pengujian->kebijakan_umum = $request->kebijakan_umum;
        $daftar_pengujian->referensi = $request->referensi;
        $daftar_pengujian->tanggung_jawab = $request->tanggung_jawab;
        $daftar_pengujian->definisi = $request->definisi;
        $daftar_pengujian->disiapkan_oleh= Auth::user()->id;

        $daftar_pengujian->save();

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil diubah",
            "data" => $daftar_pengujian,
        ]);
    }

    public function deletePengujian(Request $request){
        $daftar_pengujian = DaftarPengujian::find($request->id);
        $daftar_pengujian->delete();

        if($daftar_pengujian->disetujui_oleh!=null){
            return response()->json([
                "status" => 0,
                "message" => "Maaf data tidak bisa dihapus",
            ]);
        }

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil dihapus",
        ]);
    }

    public function approvePengujian(Request $request){
        if($request->jenis=='diperiksa'){
            if(Auth::user()->role_id != 2){
                return response()->json([
                    "status" => 0,
                    "message" => "Anda tidak memiliki akses untuk aksi ini",
                ]);
            }
            $daftar_pengujian = DaftarPengujian::find($request->id);
            $daftar_pengujian->diperiksa_oleh = Auth::user()->id;
            $daftar_pengujian->status = "Diperiksa";
            $daftar_pengujian->save();

            return response()->json([
                "status" => 1,
                "message" => "Daftar pengujian berhasil diperiksa",
            ]);
        }

        elseif($request->jenis=='disetujui'){
            if(Auth::user()->role_id != 3){
                return response()->json([
                    "status" => 0,
                    "message" => "Anda tidak memiliki akses untuk aksi ini",
                ]);
            }
            $daftar_pengujian = DaftarPengujian::find($request->id);
            $daftar_pengujian->disetujui_oleh = Auth::user()->id;
            $daftar_pengujian->status = "Disetujui";
            $daftar_pengujian->save();

            return response()->json([
                "status" => 1,
                "message" => "Daftar pengujian berhasil disetujui",
            ]);
        }
    }


}
