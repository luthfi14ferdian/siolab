<?php

namespace App\Http\Controllers\Biocompatibility;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Biocompatibility\DaftarPengujian;
use App\Model\Biocompatibility\TahapanPengujian;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;


class TahapanPengujianController extends Controller
{
    public function index (Request $request){
        $tahapan = TahapanPengujian::where("id_pengujian",$request->id_pengujian)->get();
        return response()->json([
            "status" => 1,
            "message"=>'Success',
            "data" => $tahapan,
        ]);
    }

    public function addTahapan(Request $request){
        $tahapan = new TahapanPengujian();
        $tahapan->id_pengujian = $request->id_pengujian;
        $tahapan->uraian = $request->uraian;
        $tahapan->note = $request->note;
        $tahapan->save();

        return response()->json([
            "status" => 1,
            "message"=>"Data berhasil ditambahkan",
            "data"=>$tahapan,
        ]);
    }

    public function editTahapan(Request $request){
        $tahapan = TahapanPengujian::find($request->id_item);
        $daftar_pengujian= DaftarPengujian::find($tahapan->id_pengujian);

        if($daftar_pengujian->diperiksa_oleh != null){
            return response()->json([
                "status" => 1,
                "message"=>"Tahapan tidak bisa diubah",
            ]);
        }

        $tahapan->uraian = $request->uraian;
        $tahapan->note = $request->note;
        $tahapan->save();

        return response()->json([
            "status" => 1,
            "message"=>"Data berhasil diubah",
            "data"=>$tahapan,
        ]);

    }

    public function deleteTahapan(Request $request){
        $tahapan = TahapanPengujian::find($request->id_item);
        $daftar_pengujian= DaftarPengujian::find($tahapan->id_pengujian);

        if($daftar_pengujian->diperiksa_oleh != null){
            return response()->json([
                "status" => 1,
                "message"=>"Tahapan tidak bisa dihapus",
            ]);
        }

        $tahapan->delete();

        return response()->json([
            "status" => 1,
            "message"=>"Data berhasil dihapus",
        ]);

    }
}
