<?php

namespace App\Http\Controllers\Biocompatibility;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Biocompatibility\DaftarPengujian;
use App\Model\Biocompatibility\DokumenPengujian;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Helpers\FilePathUtils;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DokumenPengujianController extends Controller
{
    public function index(Request $request){
        $dokumen_pengujian = DokumenPengujian::where('id_pengujian', $request->id_pengujian)->paginate(5);
        return response()->json([
            "status" => 1,
            "message"=>'Success',
            "data" => [
                "item" => $dokumen_pengujian->items(),
            ],
            "pagination" => [
                "current_page" => $dokumen_pengujian->currentPage(),
                "total_item" => $dokumen_pengujian->total(),
                "items_per_page" => $dokumen_pengujian->perPage(),
            ],
        ]);
    }

    public function createDocPengujian(Request $request){
        $name= FilePathUtils::getRandomFileName($request->file('dokumen_pengujian'));
        $path= Storage::putFileAs('dokumen_pengujian_biocomp', new File($request->dokumen_pengujian), $name);

        $dokumen_pengujian = new DokumenPengujian();
        $dokumen_pengujian->id_pengujian= $request->id_pengujian;
        $dokumen_pengujian->nomor_dokumen =$request->nomor_dokumen;
        $dokumen_pengujian->keterangan =$request->keterangan;
        $dokumen_pengujian->nama_file = $name;
        $dokumen_pengujian->dokumen_path = $path;

        $dokumen_pengujian->save();

        return response()->json([
            'status'=>1,
            'message'=>'Data dokumen pengujian berhasil ditambahkan',
            'data' => $dokumen_pengujian,
        ]);

    }

    public function editDocPengujian(Request $request){
        $dokumen_pengujian= DokumenPengujian::find($request->id_item);
        $daftar_pengujian = DaftarPengujian::find($dokumen_pengujian->id_pengujian);

        if($daftar_pengujian->disetujui_oleh!=null){
            return response()->json([
                "status" => 0,
                "message" => "Maaf dokumen tidak bisa diubah",
            ]);
        }

        if($request->dokumen_pengujian !=null ){
            Storage::disk('public')->delete( $dokumen_pengujian->dokumen_path);

            $name= FilePathUtils::getRandomFileName($request->file('dokumen_pengujian'));
            $path= Storage::putFileAs('dokumen_pengujian_biocomp', new File($request->dokumen_pengujian), $name);

            $dokumen_pengujian->nomor_dokumen =$request->nomor_dokumen;
            $dokumen_pengujian->keterangan =$request->keterangan;
            $dokumen_pengujian->nama_file = $name;
            $dokumen_pengujian->dokumen_path = $path;
            $dokumen_pengujian->save();

            return response()->json([
                "status" => 1,
                "message" => 'Dokumen Pengujian  Berhasil diubah',
                'data'=> $dokumen_pengujian,
            ],200);
        }

        $dokumen_pengujian->nomor_dokumen =$request->nomor_dokumen;
        $dokumen_pengujian->keterangan =$request->keterangan;
        $dokumen_pengujian->save();

        return response()->json([
            "status" => 1,
            "message" => 'Dokumen Pengujian Berhasil diubah',
            'data'=> $dokumen_pengujian,
        ],200);
    }


    public function deleteDocPengujian(Request $request){
        $dokumen_pengujian= DokumenPengujian::find($request->id_item);
        $daftar_pengujian = DaftarPengujian::find($dokumen_pengujian->id_pengujian);

        if($daftar_pengujian->disetujui_oleh!=null){
            return response()->json([
                "status" => 0,
                "message" => "Maaf dokumen tidak bisa dihapus",
            ]);
        }

        Storage::disk('public')->delete( $dokumen_pengujian->dokumen_path);
        $dokumen_pengujian->delete();

        return response()->json([
            "status"=>1,
            "message" => 'Dokumen Pengujian berhasil dihapus!',
        ],200);

    }
}
