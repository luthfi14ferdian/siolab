<?php

namespace App\Http\Controllers\Biocompatibility;

use App\Http\Controllers\Controller;

use App\Model\Biocompatibility\DaftarPengujian;
use App\Model\Biocompatibility\TahapanPengujian;
use App\Model\Biocompatibility\PemrosesanPengujian;
use App\Model\Biocompatibility\TahapanPemrosesanBiocomp;
use App\Model\Biocompatibility\SubcontractorBiocomp;
use App\Produk;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PemrosesanBiocompController extends Controller
{
    public function getPemrosesan(Request $request){
        $pemrosesan_pengujian = PemrosesanPengujian::where('id_project',$request->id_project)->paginate(10);

        foreach ($pemrosesan_pengujian->items() as $pp){
            $daftar_pengujian = DaftarPengujian::find($pp->id_pengujian);
            $daftar_pengujian->makeHidden(['nomor_dokumen','tujuan_pengujian','tanggal','ruang_lingkup','tujuan',
            'referensi','kebijakan_umum', 'tanggung_jawab','status','definisi','disiapkan_oleh','diperiksa_oleh','disetujui_oleh']);
            $subcontractor = SubcontractorBiocomp::find($pp->id_subcontractor);
            $subcontractor = $subcontractor->makeHidden(['id_project','no','tanggal_seleksi','kontak','alamat','email','jenis_jasa','pelayanan',
            'kesimpulan','total_penilaian','status','penilaian_finish','disusun_oleh','disetujui_oleh']);

            $pp->pengujian = $daftar_pengujian;
            $pp->subcontractor = $subcontractor;
        }

        return response()->json([
            "status" => 1,
            "message"=>'Success',
            "data" => [
                "item" => $pemrosesan_pengujian->items(),
            ],
            "pagination" => [
                "current_page" => $pemrosesan_pengujian->currentPage(),
                "total_item" => $pemrosesan_pengujian->total(),
                "items_per_page" => $pemrosesan_pengujian->perPage(),
            ],
        ]);
    }

    public function addPemrosesan(Request $request){
        $pemrosesan_pengujian = new PemrosesanPengujian();

        $pemrosesan_pengujian->id_subcontractor = $request->id_subcontractor;
        $pemrosesan_pengujian->id_project = $request->id_project;
        $pemrosesan_pengujian->id_pengujian = $request->id_pengujian;
        $pemrosesan_pengujian->nomor_batch= $request->nomor_batch;
        $pemrosesan_pengujian->id_produk = $request->id_produk;
        $pemrosesan_pengujian->tanggal = $request->tanggal;
        $pemrosesan_pengujian->jumlah_spesimen= $request->jumlah_spesimen;
        $pemrosesan_pengujian->status = "Baru";
        $pemrosesan_pengujian->diproses_oleh = Auth::user()->id;
        $pemrosesan_pengujian->save();

        $tahapan_pengujian = TahapanPengujian::where("id_pengujian",$request->id_pengujian)->get();

        foreach ($tahapan_pengujian as $tp){
            $tahapan_pemrosesan = new TahapanPemrosesanBiocomp();
            $tahapan_pemrosesan->id_pemrosesan_pengujian = $pemrosesan_pengujian->id;
            $tahapan_pemrosesan->uraian = $tp->uraian;
            $tahapan_pemrosesan->status = "Belum diproses";
            $tahapan_pemrosesan->save();
        }

        return response()->json([
            'status'=>1,
            'message'=>'Data pemrosesan berhasil ditambahkan',
            'data' => $pemrosesan_pengujian,
        ]);

    }

    public function getDetailPemrosesan(Request $request){
        $pemrosesan_pengujian = PemrosesanPengujian::find($request->id_pemrosesan);

        $daftar_pengujian = DaftarPengujian::find($pemrosesan_pengujian->id_pengujian);
        $daftar_pengujian->makeHidden(['nomor_dokumen','tujuan_pengujian','tanggal','ruang_lingkup','tujuan',
        'referensi','kebijakan_umum', 'tanggung_jawab','status','definisi','disiapkan_oleh','diperiksa_oleh','disetujui_oleh']);

        $subcontractor = SubcontractorBiocomp::find($pemrosesan_pengujian->id_subcontractor);
        $subcontractor = $subcontractor->makeHidden(['id_project','no','tanggal_seleksi','kontak','alamat','email','jenis_jasa','pelayanan',
        'kesimpulan','total_penilaian','status','penilaian_finish','disusun_oleh','disetujui_oleh']);

        $diperiksa_oleh=null;
        $disetujui_oleh=null;

        if(User::find($pemrosesan_pengujian->diperiksa_oleh)){
            $diperiksa_oleh= User::find($pemrosesan_pengujian->diperiksa_oleh);
            $diperiksa_oleh->makeHidden(['divisi_id','role_id','role_project','is_approve']);
        }

        if (User::find($pemrosesan_pengujian->disetujui_oleh)){
            $disetujui_oleh=User::find($pemrosesan_pengujian->disetujui_oleh);
            $disetujui_oleh->makeHidden(['divisi_id','role_id','role_project','is_approve']);
        }

        $diproses_oleh = User::find($pemrosesan_pengujian->diproses_oleh);
        $diproses_oleh->makeHidden(['divisi_id','role_id','role_project','is_approve']);

        $produk = Produk::find($pemrosesan_pengujian->id_produk);
        $produk = $produk->makeHidden(['id_project','jenis_material','jumlah_item','yang_menambahkan','keterangan','created_at','updated_at']);

        $pemrosesan_pengujian->pengujian = $daftar_pengujian;
        $pemrosesan_pengujian->subcontractor = $subcontractor;
        $pemrosesan_pengujian->produk =$produk;
        $pemrosesan_pengujian->diproses_oleh =$diproses_oleh;
        $pemrosesan_pengujian->diperiksa_oleh = $diperiksa_oleh;
        $pemrosesan_pengujian->disetujui_oleh = $disetujui_oleh;

        return response()->json([
            'status'=>1,
            'data' => $pemrosesan_pengujian,
        ]);

    }

    public function editPemrosesan(Request $request){
        $pemrosesan_pengujian = PemrosesanPengujian::find($request->id_pemrosesan);
        $pemrosesan_pengujian->id_subcontractor = $request->id_subcontractor;
        $pemrosesan_pengujian->id_pengujian = $request->id_pengujian;
        $pemrosesan_pengujian->nomor_batch= $request->nomor_batch;
        $pemrosesan_pengujian->id_produk = $request->id_produk;
        $pemrosesan_pengujian->tanggal = $request->tanggal;
        $pemrosesan_pengujian->jumlah_spesimen= $request->jumlah_spesimen;
        $pemrosesan_pengujian->save();

        return response()->json([
            'status'=>1,
            'message'=>'Data pemrosesan berhasil diubah',
            'data' => $pemrosesan_pengujian,
        ]);
    }

    public function deletePemrosesan(Request $request){
        $pemrosesan_pengujian = PemrosesanPengujian::find($request->id_pemrosesan);

        if($pemrosesan_pengujian->status != "Baru"){
            return response()->json([
                "status" => 0,
                "message" => "Maaf data tidak bisa dihapus",
            ]);
        }

        else{
            $tahapan_pengujian = TahapanPemrosesanBiocomp::where("id_pemrosesan_pengujian",$pemrosesan_pengujian->id)->get();

            foreach($tahapan_pengujian as $tp){
                $tahapan = TahapanPemrosesanBiocomp::find($tp->id);
                $tahapan->delete();
            }
            $pemrosesan_pengujian->delete();
            return response()->json([
                "status" => 1,
                "message" => "Data berhasil dihapus",
            ]);
        }
    }
}
