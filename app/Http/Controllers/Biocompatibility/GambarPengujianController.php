<?php

namespace App\Http\Controllers\Biocompatibility;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Biocompatibility\DaftarPengujian;
use App\Model\Biocompatibility\GambarPengujian;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Helpers\FilePathUtils;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class GambarPengujianController extends Controller
{
    public function index(Request $request){
        $gambar_pengujian = GambarPengujian::where('id_pengujian', $request->id_pengujian)->get();
        return response()->json([
            "status" => 1,
            "message"=>'Success',
            "data" => $gambar_pengujian,
        ]);
    }

    public function createGambarPengujian(Request $request){
        $name= FilePathUtils::getRandomFileName($request->file('gambar_pengujian'));
        $path= Storage::putFileAs('gambar_pengujian_biocomp', new File($request->gambar_pengujian), $name);

        $gambar_pengujian = new GambarPengujian();
        $gambar_pengujian->id_pengujian= $request->id_pengujian;
        $gambar_pengujian->nama_gambar =$request->nama_gambar ;
        $gambar_pengujian->nama_file = $name;
        $gambar_pengujian->gambar_path = $path;

        $gambar_pengujian->save();

        return response()->json([
            'status'=>1,
            'message'=>'Data gambar pengujian berhasil ditambahkan',
            'data' => $gambar_pengujian,
        ]);

    }

    public function editGambarPengujian(Request $request){
        $gambar_pengujian= GambarPengujian::find($request->id_item);
        $daftar_pengujian = DaftarPengujian::find($gambar_pengujian->id_pengujian);

        if($daftar_pengujian->disetujui_oleh!=null){
            return response()->json([
                "status" => 0,
                "message" => "Maaf gambar tidak bisa diubah",
            ]);
        }

        if($request->gambar_pengujian !=null ){
            Storage::disk('public')->delete( $gambar_pengujian->gambar_path);

            $name= FilePathUtils::getRandomFileName($request->file('gambar_pengujian'));
            $path= Storage::putFileAs('gambar_pengujian_biocomp', new File($request->gambar_pengujian), $name);

            $gambar_pengujian->nama_gambar  =$request->nama_gambar ;
            $gambar_pengujian->nama_file = $name;
            $gambar_pengujian->gambar_path = $path;
            $gambar_pengujian->save();

            return response()->json([
                "status" => 1,
                "message" => 'Gambar Pengujian  Berhasil diubah',
                'data'=> $gambar_pengujian,
            ],200);
        }

        $gambar_pengujian->nama_gambar  =$request->nama_gambar ;
        $gambar_pengujian->save();

        return response()->json([
            "status" => 1,
            "message" => 'Gambar Pengujian Berhasil diubah',
            'data'=> $gambar_pengujian,
        ],200);
    }

    public function deleteGambarPengujian(Request $request){
        $gambar_pengujian= GambarPengujian::find($request->id_item);
        $daftar_pengujian = DaftarPengujian::find($gambar_pengujian->id_pengujian);

        if($daftar_pengujian->disetujui_oleh!=null){
            return response()->json([
                "status" => 0,
                "message" => "Maaf gambar tidak bisa dihapus",
            ]);
        }

        Storage::disk('public')->delete( $gambar_pengujian->gambar_path);
        $gambar_pengujian->delete();

        return response()->json([
            "status"=>1,
            "message" => 'Gambar Pengujian berhasil dihapus!',
        ],200);
    }
}
