<?php

namespace App\Http\Controllers;

use App\LogBookPengembalianPrototype;
use App\User;
use App\Divisi;
use App\DokumenCar;
use App\CarTemuan;
use App\CarVerifikasiKoreksi;
use App\Project;
use App\KodeAktivitas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CARController extends Controller
{
    private function kodeAktivitas($string){
        if(substr_count($string, '_')==1){
            return strtoupper(substr($string, 0, 3)) . ' ' . strtoupper(substr($string, 4, 1)) . substr($string, 5);
        }
        else{
            $no_underscore = str_replace('_', ' ', $string);
            return strtoupper(substr($no_underscore, 0, 3)) . ' '. ucwords(substr($no_underscore, 4));
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if($request->id_item != NULL){
            return response()->json([
                "overview" => $this->getOverview($request),
                "temuan" => $this->getTemuan($request),
                "koreksi" => $this->getKoreksi($request),
                "verifikasi_koreksi" => $this->getVerifKoreksi($request),
                "verifikasi_akhir" => $this->getVerifAkhir($request),
                ]
            );
        }
        else{
            $all_dokumen_car = DB::table('dokumen_car')
            ->where('nama_project', $request->id_proyek)
            ->get();

            $dokumenCar = array_chunk($all_dokumen_car->toArray(), 10);
            if (count($dokumenCar) <= (($request->start)-1) ) {
                $dokumenCar = [];
            }
            else {
                $dokumenCar = $dokumenCar[($request->start)-1];
            }

            $total_item = count($all_dokumen_car);
            $current_page = (int)$request->start;
            $total_item_per_page = count($dokumenCar);

            return response()->json([
                "status" => 1,
                "data" => [
                    "item" => $dokumenCar
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_pr_page" => $total_item_per_page,
                ],
            ]);
        }
    }


    public function getOverview(Request $request){
        $dokumenCar = DokumenCar::find($request->id_item);

        $dokumenCar->item = Project::find($dokumenCar->item
        , ['id', 'nama_project']);
        $dokumenCar->id_pengembalian = LogBookPengembalianPrototype::find($dokumenCar->id_pengembalian);

        return ($dokumenCar);
    }

    public function getTemuan(Request $request){
        $car_temuan = DB::table('car_temuan')
        ->where('id_car', $request->id_item)
        ->get();
        $car_temuan[0]->auditor = User::find($car_temuan[0]->auditor
        , ['id', 'username', 'last_name', 'first_name']);
        $car_temuan[0]->auditee = User::find($car_temuan[0]->auditee
        , ['id', 'username', 'last_name', 'first_name']);
        $car_temuan[0]->manager_representative = User::find($car_temuan[0]->manager_representative
        , ['id', 'username', 'last_name', 'first_name']);
        return ($car_temuan);
    }

    public function getKoreksi(Request $request){
        $car_koreksi = DB::table('car_koreksi')
        ->where('id_car', $request->id_item)
        ->get();
        if(sizeof($car_koreksi) > 0){
            $car_koreksi[0]->auditor = User::find($car_koreksi[0]->auditor
            , ['id', 'username', 'last_name', 'first_name']);
            $car_koreksi[0]->auditee = User::find($car_koreksi[0]->auditee
            , ['id', 'username', 'last_name', 'first_name']);
            $car_koreksi[0]->manager_representative = User::find($car_koreksi[0]->manager_representative
            , ['id', 'username', 'last_name', 'first_name']);
            return ($car_koreksi);
        }
        else{
            return NULL;
        }
    }

    public function getVerifKoreksi(Request $request){
        $car_verif_koreksi = DB::table('car_verifikasi_koreksi')
        ->where('id_car', $request->id_item)
        ->get();
        if(sizeof($car_verif_koreksi) > 0){
            $car_verif_koreksi[0]->auditor = User::find($car_verif_koreksi[0]->auditor
            , ['id', 'username', 'last_name', 'first_name']);
            $car_verif_koreksi[0]->manager_representative = User::find($car_verif_koreksi[0]->manager_representative
            , ['id', 'username', 'last_name', 'first_name']);
            return ($car_verif_koreksi);
        }
        else{
            return NULL;
        }
    }

    public function getVerifAkhir(Request $request){
        $car_verif_akhir = DB::table('car_verifikasi_akhir')
        ->where('id_car', $request->id_item)
        ->get();
        if(sizeof($car_verif_akhir) > 0){
            $car_verif_akhir[0]->manager_representative = User::find($car_verif_akhir[0]->manager_representative
            , ['id', 'username', 'last_name', 'first_name']);
            return ($car_verif_akhir);
        }
        else{
            return NULL;
        }
    }

    private function createAktivitasUser($aktivitas, $kode, $idUser, $idAktivitas){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        DB::table('aktivitas_user')->insert([
            'jenis_aktivitas' => $aktivitas,
            'id_user' => $idUser,
            'kode_aktivitas' => KodeAktivitas::where('jenis_aktivitas', $kode)->first()->id,
            'id_aktivitas' => $idAktivitas,
            'created_at' => $mytimenew->toDateTimeString(),
            'updated_at' => $mytimenew->toDateTimeString(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createNotes(Request $request)
    {
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $car = DB::table($request->jenis_car)
            ->where('id', $request->id_item)
            ->get();
        $kode_aktivitas= $this->kodeAktivitas($request->jenis_car);
        if(sizeof($car) > 0){
            DB::table($request->jenis_car)
            ->where('id', $request->id_item)
            ->update(['note' => $request->note, 'updated_at' => $mytimenew->toDateTimeString()]);

            $this->createAktivitasUser('Menambah Notes ' . $kode_aktivitas, $kode_aktivitas, $request->session()->get('id'), $request->id_item);

            return response()->json(['ID'=> $request->id_item, 'Success'=> 'Sukses mengubah'], 200);
        }
        else{
            return response()->json(['ID'=> $request->id_item, 'Success'=> 'Data Tidak Ditemukan'], 500);
        }


    }




    public function doApprove(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $car = DB::table($request->jenis_car)
            ->where('id', $request->id_item)
            ->get();
        $kode_aktivitas= $this->kodeAktivitas($request->jenis_car);

        if (sizeof($car) >= 1){
            if($car[0]->auditee == NULL){
                $this->ubahStatus($request->jenis_car, $request->id_item,
                $request->session()->get('id'), 'auditee');

                DB::table('dokumen_car')
                ->where('id', $car[0]->id_car)
                ->update(['status'=> 'setujui_'.$request->jenis_car.'_auditee','updated_at' => $mytimenew->toDateTimeString()]);
                $this->createAktivitasUser('Menyetujui ' . $kode_aktivitas. ' (Auditee)', $kode_aktivitas, $request->session()->get('id'), $request->id_item);

                return response()->json(['ID'=> $car[0]->id, 'status'=> $kode_aktivitas . ' berhasil disetujui oleh Auditee'], 200);
            }
            else if($car[0]->manager_representative == NULL){
                $this->ubahStatus($request->jenis_car, $request->id_item,
                $request->session()->get('id'), 'manager_representative');

                DB::table('dokumen_car')
                ->where('id', $car[0]->id_car)
                ->update(['status'=> 'setujui_'.$request->jenis_car.'_manager_representative','updated_at' => $mytimenew->toDateTimeString()]);
                $this->createAktivitasUser('Menyetujui ' . $kode_aktivitas. ' (Manager Representative)', $kode_aktivitas, $request->session()->get('id'), $request->id_item);

                return response()->json(['ID'=> $car[0]->id, 'status'=> $kode_aktivitas . ' berhasil disetujui oleh Manager Representative'], 200);
            }
            else{
                return response()->json(['ID'=> $car[0]->id, 'status'=> 'Gagal mengubah, authorization violation atau dokumen ini sudah disetujui oleh pelapor, terlapor, dan manager representative'], 500);
            }
        }
        else{
            return response()->json(['status'=> 'FAILED', 'reason'=> 'Gagal mengubah, data not found'], 500);
        }
    }

    private function ubahStatus($kode_aktivitas, $id_item, $id_user, $test){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');

        DB::table($kode_aktivitas)
        ->where('id', $id_item)
        ->update([$test => $id_user,'updated_at' => $mytimenew->toDateTimeString()]);
    }

    public function doApproveVerifikasi(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $car = DB::table($request->jenis_car)
        ->where('id', $request->id_item)
        ->get();;
        $kode_aktivitas= $this->kodeAktivitas($request->jenis_car);

        if (sizeof($car) >= 1){
            if($car[0]->manager_representative == NULL){
                $this->ubahStatus($request->jenis_car, $request->id_item,
                $request->session()->get('id'), 'manager_representative');

                DB::table('dokumen_car')
                ->where('id', $car[0]->id_car)
                ->update(['status'=> 'setujui_'.$request->jenis_car.'_manager_representative','updated_at' => $mytimenew->toDateTimeString()]);
                $this->createAktivitasUser('Menyetujui ' . $kode_aktivitas. ' (Manager Representative)', $kode_aktivitas, $request->session()->get('id'), $request->id_item);

                return response()->json(['ID'=> $car[0]->id, 'status'=> $kode_aktivitas . ' berhasil disetujui oleh Manager Representative'], 200);
            }
            else{
                return response()->json(['ID'=> $car[0]->id, 'status'=> 'Gagal mengubah, authorization violation atau dokumen ini sudah disetujui oleh pelapor, terlapor, dan manager representative'], 500);
            }
        }
        else{
            return response()->json(['status'=> 'FAILED', 'reason'=> 'Gagal mengubah, data not found'], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        ///
    }
    private function invalidKoreksi(Request $request){
        return (
            $request->analisis_penyebab == NULL ||
            $request->tindakan_koreksi == NULL
        );
    }

    public function storeKoreksi(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        if(!$this->invalidKoreksi($request)){
            $car_koreksi = DB::table('car_koreksi')->insertGetId($request->all() +
            ['created_at' => $mytimenew->toDateTimeString(), 'auditor' => $request->session()->get('id'), 'updated_at' => $mytimenew->toDateTimeString()]);

            DB::table('dokumen_car')
            ->where('id', $request->id_car)
            ->update(['status'=> 'koreksi_temuan_auditor','updated_at' => $mytimenew->toDateTimeString()]);

            $this->createAktivitasUser('Membuat CAR Koreksi', 'CAR Koreksi', $request->session()->get('id'), $car_koreksi);

            if($car_koreksi != NULL){
                return response()
                    ->json([
                        DB::table('car_koreksi')
                        ->where('car_koreksi.id', $car_koreksi)
                        ->get()]);
            }
            else{
                return response()->json(['status' => 'FAILED', 'reason' => 'Gagal mengubah']);
            }
        }
        else{
            return response()
                    ->json(['status' => 'FAILED','reason'=>'Empty request']);
        }

    }

    private function invalidVerifikasiKoreksi(Request $request){
        return (
            $request->efektivitas == NULL
        );
    }

    public function storeVerifikasiKoreksi(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        if(!$this->invalidVerifikasiKoreksi($request)){
            $car_verifikasi = DB::table('car_verifikasi_koreksi')->insertGetId($request->all() +
            ['created_at' => $mytimenew->toDateTimeString(), 'auditor' => $request->session()->get('id'), 'updated_at' => $mytimenew->toDateTimeString()]);

            DB::table('dokumen_car')
            ->where('id', $request->id_car)
            ->update(['status'=> 'verifikasi_koreksi','updated_at' => $mytimenew->toDateTimeString()]);

            $this->createAktivitasUser('Membuat CAR Verifikasi Koreksi', 'CAR Verifikasi Koreksi', $request->session()->get('id'), $car_verifikasi);

            if($car_verifikasi != NULL){
                return response()
                    ->json([
                        DB::table('car_verifikasi_koreksi')
                        ->where('car_verifikasi_koreksi.id', $car_verifikasi)
                        ->get()]);
            }
            else{
                return response()->json(['status' => 'FAILED', 'reason' => 'Gagal mengubah']);
            }
        }
        else{
            return response()
                    ->json(['status' => 'FAILED','reason'=>'Empty request']);
        }

    }

    public function storeVerifikasiAkhir(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $car_verifikasi = DB::table('car_verifikasi_akhir')->insertGetId($request->all() +
        ['created_at' => $mytimenew->toDateTimeString(), 'updated_at' => $mytimenew->toDateTimeString()]);

        DB::table('dokumen_car')
        ->where('id', $request->id_car)
        ->update(['status'=> 'verifikasi_akhir','updated_at' => $mytimenew->toDateTimeString()]);

        $this->createAktivitasUser('Membuat CAR Verifikasi Akhir', 'CAR Verifikasi Akhir', $request->session()->get('id'), $car_verifikasi);

        if($car_verifikasi != NULL){
            return response()
                ->json([
                    DB::table('car_verifikasi_akhir')
                    ->where('car_verifikasi_akhir.id', $car_verifikasi)
                    ->get()]);
        }
        else{
            return response()->json(['status' => 'FAILED', 'reason' => 'Gagal mengubah']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LogBookPengembalianPrototype  $logBookPengembalianPrototype
     * @return \Illuminate\Http\Response
     */
    public function show(LogBookPengembalianPrototype $logBookPengembalianPrototype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LogBookPengembalianPrototype  $logBookPengembalianPrototype
     * @return \Illuminate\Http\Response
     */
    public function edit(LogBookPengembalianPrototype $logBookPengembalianPrototype)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LogBookPengembalianPrototype  $logBookPengembalianPrototype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $car = DB::table($request->jenis_car)
            ->where('id', $request->id_item)
            ->get();
        $kode_aktivitas= $this->kodeAktivitas($request->jenis_car);
        if(sizeof($car) >= 1){
            if($car[0]->auditee == NULL){
                DB::table($request->jenis_car)
                    ->where('id', $request->id_item)
                    ->update($request->except('id_item', 'jenis_car') + ['updated_at' => $mytimenew->toDateTimeString()]);

                $this->createAktivitasUser('Mengubah ' . $kode_aktivitas, $kode_aktivitas, $request->session()->get('id'), $request->id_item);

                return response()->json(['ID'=> $request->id_item, 'Success'=> 'Sukses mengubah'], 200);
            }
            else{
                return response()->json(['ID'=> $request->id_item, 'Failed'=> 'Data Sudah Tidak Dapat Diubah'], 500);
            }
        }
        else{
            return response()->json(['ID'=> $request->id_item, 'Failed'=> 'Data Tidak Ditemukan'], 500);
        }

    }

    public function updateVerifikasi(Request $request)
    {
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $car = DB::table($request->jenis_car)
            ->where('id', $request->id_item)
            ->get();
        $kode_aktivitas= $this->kodeAktivitas($request->jenis_car);
        if(sizeof($car) >= 1){
            if($car[0]->manager_representative == NULL){
                DB::table($request->jenis_car)
                    ->where('id', $request->id_item)
                    ->update($request->except('id_item', 'jenis_car') + ['updated_at' => $mytimenew->toDateTimeString()]);

                    $this->createAktivitasUser('Mengubah ' . $kode_aktivitas, $kode_aktivitas, $request->session()->get('id'), $request->id_item);

                return response()->json(['ID'=> $request->id_item, 'Success'=> 'Sukses mengubah'], 200);
            }
            else{
                return response()->json(['ID'=> $request->id_item, 'Failed'=> 'Data Sudah Tidak Dapat Diubah'], 500);
            }
        }
        else{
            return response()->json(['ID'=> $request->id_item, 'Failed'=> 'Data Tidak Ditemukan'], 500);
        }

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LogBookPengembalianPrototype  $logBookPengembalianPrototype
     * @return \Illuminate\Http\Response
     */
    public function destroy(LogBookPengembalianPrototype $logBookPengembalianPrototype)
    {
        //
    }
}
