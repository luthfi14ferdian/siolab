<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class RegisterController extends Controller
{
    public function tambahAkun(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $rules = [
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'first_name' => ['required', 'string', 'max:128'],
            'last_name' => ['required', 'string', 'max:128'],
            'divisi_id' => ['required', 'integer', 'max:128'],
            'role_id' => ['required', 'integer', 'max:128'],
            'password' => ['required', 'string', 'min:8', 'same:password_confirmation'],
            'password_confirmation' => ['min:8'],
        ];
        $validator = Validator::make($request->all(), $rules);

        if($validator->passes()){
            $user = DB::table('users')->insertGetId($request->except('password', 'password_confirmation') +
            ['created_at' => $mytimenew->toDateTimeString(), 'password'=>Hash::make($request->password)]);


            if($user != NULL){
                return response()
                    ->json(['status' => 'user berhasil dibuat']);
            }
        }
        else{
            return response()
            ->json(['status' => 'data tidak lengkap', 'reason' => $validator->errors()->first()]);
        }

    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'first_name' => ['required', 'string', 'max:128'],
            'last_name' => ['required', 'string', 'max:128'],
            'divisi_id' => ['required', 'integer', 'max:128'],
            'role_id' => ['required', 'integer', 'max:128'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
}
