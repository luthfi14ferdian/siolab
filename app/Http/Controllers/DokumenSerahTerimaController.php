<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DokumenSerahTerima;
use App\project;
use App\DokumenNAD;
use App\Produk;
use App\User;
use Illuminate\Support\Facades\Auth;

class DokumenSerahTerimaController extends Controller
{
    public function index(Request $request){
        if($request->tujuan=="dari-divisi-lain"){
            $dst= DokumenSerahTerima::where('divisi_yang_diajukan',$request->id_divisi)
            ->where('id_project',$request->id_project)->paginate(10);

            $items = $dst->items();
            $total_item = $dst->total();
            $current_page = $dst->currentPage();
            $total_item_per_page = $dst->perPage();

            return response()->json([
                "status" => 1,
                "data" => [
                    "item" =>$items
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_per_page" => $total_item_per_page,
                ],
            ]);
        }

        elseif($request->tujuan=="ke-divisi-lain"){
            $dst= DokumenSerahTerima::where('divisi_yang_mengajukan',$request->id_divisi)
            ->where('id_project',$request->id_project)->paginate(10);

            $items = $dst->items();
            $total_item = $dst->total();
            $current_page = $dst->currentPage();
            $total_item_per_page = $dst->perPage();

            return response()->json([
                "status" => 1,
                "data" => [
                    "item" =>$items
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_per_page" => $total_item_per_page,
                ],
            ]);
        }
    }

    public function createDst(Request $request){
        $dst = new DokumenSerahTerima();
        $dst->id_project=$request->id_project;
        $dst->status = 'Belum diterima';
        $dst->tanggal = $request->tanggal;
        $dst->divisi_yang_mengajukan = Auth::user()->divisi_id;
        $dst->divisi_yang_diajukan = $request->divisi_yang_diajukan;
        $dst->id_produk = $request->id_produk;
        $dst->nama_item = $request->nama_item;
        $dst->jumlah = $request->jumlah;
        $dst->keterangan = $request->keterangan;
        $dst->yang_menyerahkan = Auth::user()->id;
        $dst->created_at = date("Y-m-d H:i:s");

        $dst->save();

        return response()->json([
            "status" => 1,
            "message" => 'Dokumen Serah Terima berhasil dibuat',

        ],200);
    }

    public function getDetailDst(Request $request, $id){
        $dst = DokumenSerahTerima::with(['produk'])->find($id);

        if($dst){
            $user_yang_menyerahkan= User::find($dst->yang_menyerahkan);
            $user_yang_menerima= User::find($dst->yang_menerima);
            $produk = $dst->produk;

            $dst_rep= $dst->replicate();
            $dst_rep->yang_menyerahkan= $user_yang_menyerahkan;
            $dst_rep->yang_menerima =  $user_yang_menerima;

            $dst->makeHidden(['yang_menyerahkan','yang_menerima']);
            $produk->makeHidden(['id_project','jenis_material','jumlah_item','yang_menambahkan','keterangan','created_at','updated_at']);
            $user_yang_menyerahkan->makeHidden(['role_project','is_approve']);

            if($user_yang_menerima){
                $user_yang_menerima->makeHidden(['role_project','is_approve']);
            }
                return response()->json([
                    "status" => 1,
                    "data" =>$dst_rep,
                ],200 );
        }

        else{
            return response()->json([
                'status'=>0,
                'message' => 'Data tidak ditemukan',
            ],404);
        }
    }

    public function editDst(Request $request,$id){
        $dst= DokumenSerahTerima::find($id);
        $dst->id_project = $request->id_project;
        $dst->tanggal = $request->tanggal;
        $dst->divisi_yang_mengajukan = Auth::user()->divisi_id;
        $dst->divisi_yang_diajukan = $request->divisi_yang_diajukan;
        $dst->id_produk = $request->id_produk;
        $dst->nama_item = $request->nama_item;
        $dst->jumlah = $request->jumlah;
        $dst->keterangan = $request->keterangan;
        $dst->yang_menyerahkan = Auth::user()->id;
        $dst->updated_at = date("Y-m-d H:i:s");

        $dst->save();

        return response()->json([
            "status" => 1,
            "message" => 'Dokumen Serah Terima berhasil diubah',
            'data'=>$dst,

        ],200);
    }

    public function deleteDst(Request $request, $id){
        $dst = DokumenSerahTerima::find($request->id);

        if($dst->status != "Belum diterima"){
            return response()->json([
                "message" => 'Dokumen Serah Terima tidak bisa dihapus!',
            ],200);
        }

        else{
            $dst->delete();

        return response()->json([
            "status" => 1,
            "message" => 'Dokumen Serah Terima berhasil dihapus',
        ],200);
        }

    }

    public function acceptDst(Request $request, $id){
        $dst = DokumenSerahTerima::find($request->id);

        if($dst){
            $dst->status = "Sudah diterima";
            $dst->yang_menerima =Auth::user()->id;
            $dst->save();

            return response()->json([
                'status'=>1,
                'message'=> "Item berhasil diterima",
            ]);
        }

        else{
            return response()->json([
                'status'=>0,
                'message'=> "Item tidak ditemukan",
            ]);
        }

    }

}
