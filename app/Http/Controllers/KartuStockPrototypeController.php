<?php

namespace App\Http\Controllers;

use App\KartuStockPrototype;
use Illuminate\Http\Request;
use App\Divisi;
use App\User;
use App\KodeAktivitas;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class KartuStockPrototypeController extends Controller
{
    private function createAktivitasUser($aktivitas, $kode, $idUser, $idAktivitas){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        DB::table('aktivitas_user')->insert([
            'jenis_aktivitas' => $aktivitas,
            'id_user' => $idUser,
            'kode_aktivitas' => KodeAktivitas::where('jenis_aktivitas', $kode)->first()->id,
            'id_aktivitas' => $idAktivitas,
            'created_at' => $mytimenew->toDateTimeString(),
            'updated_at' => $mytimenew->toDateTimeString(),
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if($request->id_item == NULL){
            $all_kartu = DB::table('kartu_stock_prototypes')
            ->where('nama_project', $request->id_proyek)
            ->join('users', 'kartu_stock_prototypes.dikerjakan_oleh', '=', 'users.id')
            ->select('kartu_stock_prototypes.id', 'kartu_stock_prototypes.jenis_prototype', 'kartu_stock_prototypes.no_batch'
            , 'users.first_name as dibuat_oleh')
            ->get();

            $kartuStockPrototype = array_chunk($all_kartu->toArray(), 10);
            if (count($kartuStockPrototype) <= (($request->start)-1) ) {
                $kartuStockPrototype = [];
            }
            else {
                $kartuStockPrototype = $kartuStockPrototype[($request->start)-1];
            }

            $total_item = count($all_kartu);
            $current_page = (int)$request->start;
            $total_item_per_page = count($kartuStockPrototype);

            return response()->json([
                "status" => 1,
                "data" => [
                    "item" => $kartuStockPrototype
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_pr_page" => $total_item_per_page,
                ],
            ]);
        }
        else{
            return response()->json($this->getDetailKartuStockPrototyoe($request));
        }
    }

    public function getDetailKartuStockPrototyoe(Request $request){
        $detail_kartu = DB::table('kartu_stock_prototypes')
            ->where('kartu_stock_prototypes.id', $request->id_item)
            ->where('nama_project', $request->id_proyek)
            ->get();

        $user = User::find($detail_kartu[0]->dikerjakan_oleh
        , ['id', 'username', 'last_name', 'first_name', 'divisi_id']);
        $user->divisi_id = Divisi::find($user->divisi_id);

        $detail_kartu[0]->dikerjakan_oleh = $user;

        return $detail_kartu;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    private function invalidKartuStock(Request $request){
        return (
            $request->kategori_prototype == NULL ||
            $request->jenis_prototype == NULL ||
            $request->no_batch == NULL ||
            $request->masuk == NULL ||
            $request->keluar == NULL ||
            $request->keterangan == NULL
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        if(!$this->invalidKartuStock($request)){
            $kartuStockPrototype = DB::table('kartu_stock_prototypes')->insertGetId($request->except('id_project') +
            ['nama_project'=>$request->id_project, 'dikerjakan_oleh'=>$request->session()->get('id'),'created_at' => $mytimenew->toDateTimeString(), 'updated_at' => $mytimenew->toDateTimeString()]);

            $this->createAktivitasUser('Membuat Kartu Stock Prototype', 'Kartu Stock Prototype', $request->session()->get('id'), $kartuStockPrototype);

            if($kartuStockPrototype != NULL){
                return response()
                    ->json([
                        DB::table('kartu_stock_prototypes')
                        ->where('kartu_stock_prototypes.id', $kartuStockPrototype)
                        ->get()]);
            }
        }
        else{
            return response()
                    ->json(['status' => 'FAILED','reason'=>'Empty request']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KartuStockPrototype  $kartuStockPrototype
     * @return \Illuminate\Http\Response
     */
    public function show(KartuStockPrototype $kartuStockPrototype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KartuStockPrototype  $kartuStockPrototype
     * @return \Illuminate\Http\Response
     */
    public function edit(KartuStockPrototype $kartuStockPrototype)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KartuStockPrototype  $kartuStockPrototype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KartuStockPrototype $kartuStockPrototype)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KartuStockPrototype  $kartuStockPrototype
     * @return \Illuminate\Http\Response
     */
    public function destroy(KartuStockPrototype $kartuStockPrototype)
    {
        //
    }
}
