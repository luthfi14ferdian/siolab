<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DokumenNAD;
use App\Helpers\FilePathUtils;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DokumenNadController extends Controller
{
    public function index(Request $request){
        $docnad = DokumenNAD::where('id_project', $request->id_project)->get()->toArray();
        return response()->json([
            'status'=>1,
            'data'=> $docnad,
        ]);
    }

    public function createDocNad(Request $request){
        $name= FilePathUtils::getRandomFileName($request->file('dokumen'));
        $path= Storage::putFileAs('dokumen_nad', new File($request->dokumen), $name);

        $docnad = new DokumenNAD();
        $docnad->id_project= $request->id_project;
        $docnad->id_divisi=Auth::user()->divisi_id;
        $docnad->status = 'editable';
        $docnad->nama_dokumen =$request->nama_dokumen;
        $docnad->nama_file = $name;
        $docnad->dokumen_path = $path;
        $docnad->created_at =  date("Y-m-d H:i:s");

        $docnad->save();

        return response()->json([
            'status'=>1,
            'message'=>'berhasil ditambahkan',
            'data' => $docnad,
        ]);

    }

    public function editDocNad(Request $request){
        $docnad= DokumenNAD::find($request->id_item);

        if($request->status != "non-editable"){
            if($request->dokumen == null){
                $docnad->nama_dokumen =$request->nama_dokumen;
                $docnad->updated_at =  date("Y-m-d H:i:s");
                $docnad->save();

                return response()->json([
                    "status" => 1,
                    "message" => 'Dokumen Nad  Berhasil diubah',
                    'data'=>$docnad,
                ],200);
            }

            elseif($request->dokumen !=null ){
                Storage::disk('public')->delete($docnad->dokumen_path);

                $name= FilePathUtils::getRandomFileName($request->file('dokumen'));
                $path= Storage::putFileAs('dokumen_nad', new File($request->dokumen), $name);

                $docnad->nama_dokumen =$request->nama_dokumen;
                $docnad->nama_file = $name;
                $docnad->dokumen_path = $path;
                $docnad->updated_at = date("Y-m-d H:i:s");
                $docnad->save();

                return response()->json([
                    "status" => 1,
                    "message" => 'Dokumen Nad  Berhasil diubah',
                    'data'=>$docnad,
                ],200);
            }
        }
        else{
            return response()->json([
                "message" => 'Dokumen NAD tidak bisa diubah!',
            ],401);
        }
    }

    public function deleteDocNad(Request $request){
        $docnad= DokumenNAD::find($request->id_item);
        if($docnad->status != "editable"){
            return response()->json([
                "message" => 'Dokumen NAD tidak bisa dihapus!',
            ],401);
        }

        else{
            Storage::disk('public')->delete($docnad->dokumen_path);
            $docnad->delete();

            return response()->json([
                "status"=>1,
                "message" => 'Dokumen NAD berhasil dihapus!',
            ],200);
        }
    }


}
