<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LaporanBatch;
use App\User;
use App\Produk;
use App\DataBatch;
use Illuminate\Support\Facades\Auth;

class LaporanBatchController extends Controller
{
    public function index(Request $request){
        $dataBatch = LaporanBatch::where('id_batch',$request->id_batch)->paginate(10);
        $dataBatch->makeHidden(['diperiksa_oleh','disetujui_oleh', 'divisi_id',
        'alamat','operator','id_produk']);

        $items = $dataBatch->items();
        $total_item =$dataBatch->total();
        $current_page = $dataBatch->currentPage();
        $total_item_per_page = $dataBatch->perPage();


        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $items
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }
    public function addLaporanBatch(Request $request){
        $laporan_batch= new LaporanBatch();
        $laporan_batch->id_batch = $request->id_batch;
        $laporan_batch->nomor_dokumen = $request->nomor_dokumen;
        $laporan_batch->status = "Draft";
        $laporan_batch->divisi_id = Auth::user()->divisi_id;
        $laporan_batch->operator = Auth::user()->id;
        $laporan_batch->tanggal_pengerjaan = $request->tanggal_pengerjaan;
        $laporan_batch->alamat= $request->alamat;
        $laporan_batch->alat_ukur= $request->alat_ukur;
        $laporan_batch->spesimen= $request->spesimen;
        $laporan_batch->id_produk= $request->id_produk;
        $laporan_batch->metode_pengukuran= $request->metode_pengukuran;
        $laporan_batch->finished_tahapan= 0;
        $laporan_batch->created_at= date("Y-m-d H:i:s");

        $laporan_batch->save();

        $data_batch = DataBatch::find($request->id_batch);
        $data_batch->status = "Sedang diproses";
        $data_batch->save();

        $laporan_batch_get= LaporanBatch::with(['produk'])->find($laporan_batch->id);
        $laporan_batch_get->makeHidden('id_produk');

        $operator = User::find($laporan_batch_get->operator);
        $operator->makeHidden(['divisi_id','role_id','role_project','is_approve']);

        $laporan_batch_get->operator = $operator;

        return response()->json([
            "status" => 1,
            "message" => 'Data Laporan batch berhasil ditambahkan',
            "data"=> $laporan_batch_get,
        ], 200);
    }

    public function editLaporanBatch(Request $request){
        $laporan_batch= LaporanBatch::find($request->id);
        $laporan_batch->nomor_dokumen = $request->nomor_dokumen;
        $laporan_batch->divisi_id = Auth::user()->divisi_id;
        $laporan_batch->alamat= $request->alamat;
        $laporan_batch->alat_ukur= $request->alat_ukur;
        $laporan_batch->spesimen= $request->spesimen;
        $laporan_batch->id_produk= $request->id_produk;
        $laporan_batch->metode_pengukuran= $request->metode_pengukuran;
        $laporan_batch->updated_at= date("Y-m-d H:i:s");

        $laporan_batch->save();

        return response()->json([
            "status" => 1,
            "message" => 'Data Laporan batch berhasil diubah'
        ], 200);
    }

    public function detailLaporanBatch(Request $request){
        $laporan_batch= LaporanBatch::with(['produk'])->find($request->id);
        $laporan_batch->makeHidden('id_produk');

        $operator = User::find($laporan_batch->operator);
        $operator->makeHidden(['divisi_id','role_id','role_project','is_approve']);

        $diperiksa_oleh=null;
        $disetujui_oleh=null;

        if(User::find($laporan_batch->diperiksa_oleh)){
            $diperiksa_oleh= User::find($laporan_batch->diperiksa_oleh);
            $diperiksa_oleh->makeHidden(['divisi_id','role_id','role_project','is_approve']);
        }

        if (User::find($laporan_batch->disetujui_oleh)){
            $disetujui_oleh=User::find($laporan_batch->disetujui_oleh);
            $disetujui_oleh->makeHidden(['divisi_id','role_id','role_project','is_approve']);
        }

        $laporan_batch->operator = $operator;
        $laporan_batch->diperiksa_oleh = $diperiksa_oleh;
        $laporan_batch->disetujui_oleh = $disetujui_oleh;

        return response()->json([
            "status" => 1,
            "message" => 'Success',
            "data"=> $laporan_batch,
        ], 200);
    }

    public function deleteLaporan(Request $request){
        $laporan = LaporanBatch::find($request->id_laporan);
        $id_batch = $laporan->id_batch;

        if($laporan->status != "Draft"){
            return response()->json([
                "status" => 0,
                "message" => 'Laporan tidak bisa dihapus!',
            ], 400);
        }

        $laporan->delete();
        $laporan_batch = LaporanBatch::where("id_batch",$id_batch)->get();

        if(count($laporan_batch) == 0 ){
            $data_batch = DataBatch::find($id_batch);
            $data_batch->status="Baru";
            $data_batch->save();
        }

        return response()->json([
            "status" => 1,
            "message" => 'Laporan berhasil dihapus!',
        ], 200);
    }

    public function stageLaporan(Request $request){
        $laporan = LaporanBatch::find($request->id_laporan);

        if($request->tipe == "allignment"){
            $laporan->finished_tahapan = 1;
            $laporan->save();
        }

        elseif($request->tipe == "dimensi"){
            $laporan->finished_tahapan = 2;
            $laporan->save();
        }

        elseif($request->tipe == "geometri"){
            $laporan->finished_tahapan = 3;
            $laporan->status="Sedang diproses";
            $laporan->save();
        }

        return response()->json([
            "status" => 1,
            "message" => 'Stage berhasil diupdate',
        ], 200);
    }

    public function approveLaporan(Request $request){
        $laporan = LaporanBatch::find($request->id_laporan);
        if($request->tipe == "diperiksa"){
            $laporan->diperiksa_oleh = Auth::user()->id;
            $laporan->status = "Diperiksa";
            $laporan->save();

            return response()->json([
                "status" => 1,
                "message" => 'Laporan berhasil diperiksa!',
            ], 200);
        }

        elseif($request->tipe == "disetujui"){
            $laporan->disetujui_oleh = Auth::user()->id;
            $laporan->status = "Disetujui";
            $laporan->save();

            return response()->json([
                "status" => 1,
                "message" => 'Laporan berhasil disetujui!',
            ], 200);
        }
    }

}
