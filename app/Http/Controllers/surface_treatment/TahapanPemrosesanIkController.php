<?php

namespace App\Http\Controllers\surface_treatment;

use App\Http\Controllers\Controller;
use App\TahapanPemrosesanIk;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TahapanPemrosesanIkController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeIK(Request $request)
    {
        $userId =  Auth::user()->id;

        $id = DB::table('pemrosesans')->insertGetId([
            'jenis' => "Instruksi Kerja",
            'jumlah_diuji' => $request->jumlah_diuji,
            'status' => "Belum diproses",
            'no_batch' => $request->no_batch,
            'id_project'=>$request->id_project,
            'id_produk'=>$request->id_project,
            'tanggal_dibuat'=> date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('pemrosesan_iks')->insert([
            'id_pemrosesan' => $id,
            'id_ik' => $request->id_ik,
            'created_at' => date("Y-m-d H:i:s"),
            'diproses_oleh' => $userId
        ]);


        $tahapanIK = DB::table("tahapan_iks")
        ->select("tahapan_iks.id")
        ->where("tahapan_iks.id_ik", $request->id_ik)
            ->get();

        for ($i = 0; $i < count($tahapanIK); $i++) {
            DB::table('tahapan_pemrosesan_iks')->insert([
                'id_tahapan' => $tahapanIK[$i]->id,
                'id_pemrosesan' => $id,
                'status' => "Belum diproses",
                'note' => "-",
            ]);
        }

        $allDataTahapan = DB::table("tahapan_pemrosesan_iks")
        ->join("tahapan_iks", "tahapan_pemrosesan_iks.id_tahapan", "=", "tahapan_iks.id")
        ->select("tahapan_iks.uraian", "tahapan_pemrosesan_iks.*", "tahapan_iks.urutan")
        ->where("tahapan_pemrosesan_iks.id_pemrosesan", $id)
        ->get()->sortBy("urutan");

        $alats = DB::table("alat_tahapan_iks")
        ->join("alats", "alat_tahapan_iks.id_alat", "=", "alats.id")
        ->select("alats.nama", "alat_tahapan_iks.id_tahapan")
        ->get();


        for ($i = 0; $i < count($allDataTahapan); $i++) {
            $allDataTahapan[$i] = collect($allDataTahapan[$i])->put("alat_terkait", []);
            $allDataTahapan[$i] = collect($allDataTahapan[$i])->put("hasil_proses", "-");
            $namas = $alats->where("id_tahapan", $allDataTahapan[$i]["id_tahapan"]);
            $allDataTahapan[$i]["alat_terkait"] = $namas->pluck("nama")->toArray();
        }

        $allTahapan = DB::table("tahapan_pemrosesan_iks")
        ->select("tahapan_pemrosesan_iks.id_tahapan")
        ->where("tahapan_pemrosesan_iks.id_pemrosesan", $id)
        ->get();


        $allParameter = DB::table("alat_tahapan_iks")
        ->join("param_alats", "alat_tahapan_iks.id_alat", "=", "param_alats.id_alat")
        ->select("alat_tahapan_iks.*", "param_alats.id")
        ->whereIn("alat_tahapan_iks.id_tahapan", $allTahapan->pluck("id_tahapan")->toArray())
            ->get();


        for ($i = 0; $i < count($allParameter); $i++) {

            DB::table('parameter_pemrosesans')->insert([
                'id_parameter' => $allParameter[$i]->id,
                'id_tahapan' => $allParameter[$i]->id_tahapan,
                'id_pemrosesan' => $id,
                'value' => null,
            ]);
        }

        $namaIK =  DB::table("instruksi_kerjas")
        ->select("instruksi_kerjas.nama")
        ->where("instruksi_kerjas.id", $request->id_ik)
            ->get()[0]
            ->nama;

        $activityDetail = "Menambah pemrosesan baru surface treatment dengan menggunakan IK [" . $namaIK . "]";
        $activityDetail = substr($activityDetail, 0, 128);

        DB::table('aktivitas_users')->insert([
            'id_project' => DB::table('projects')->select("projects.id")->where("projects.id", 1)->get()[0]->id, //toDo nanti ganti
            'id_user' => $userId,
            'jenis_aktivitas' => "Menambah sesuai Instruksi Kerja",
            'id_aktivitas' => $id, //nyambung ke id pemrosesan
            'detail_aktivitas' => $activityDetail,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan",
            "data" => TahapanPemrosesanIk::serializePemrosesanIkItem($id)
        ], 200);
    }

    /**
     * Edit data umum IK
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function editDataUmumIk(Request $request)
    {        
        DB::table("pemrosesans")
            ->where("pemrosesans.id", $request->id_item)
            ->update([
                'jumlah_diuji' => $request->jumlah_diuji,
                'no_batch' => $request->no_batch,
                'tanggal_dibuat' => $request->tanggal_dibuat,
            ]);

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan",
            "data" => TahapanPemrosesanIk::serializePemrosesanIkItem($request->id_item)
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TahapanPemrosesanIk  $tahapanPemrosesanIk
     * @return \Illuminate\Http\Response
     */
    public function showIK(Request $request)
    {
        $serializedData = TahapanPemrosesanIk::serializePemrosesanIkItem($request->id_pemrosesan);
        if ($serializedData) {
            return response()->json([
                "status" => 1,
                "data" => $serializedData,
            ]);
        }
        return response()->json([
            'status' => 0,
            'message' => "Data tidak ditemukan"
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TahapanPemrosesanIk  $tahapanPemrosesanIk
     * @return \Illuminate\Http\Response
     */
    public function editHasilPemrosesan(Request $request)
    {
        //untuk setiap param dalam suatu tahapan, kita looping
        $param = $request->all()["param"];

        //Mengubah note dari tahapan tersebut
        DB::table("tahapan_pemrosesan_iks")
            ->where("tahapan_pemrosesan_iks.id_pemrosesan", $request->id_pemrosesan)
            ->where("tahapan_pemrosesan_iks.id_tahapan", $request->id_tahapan)
            ->update([
                'note' => $request->all()["note"],
            ]);

        $user =  Auth::user();

        $namaIK = DB::table("instruksi_kerjas")
        ->join("tahapan_iks", "instruksi_kerjas.id", "=", "tahapan_iks.id_ik")
        ->select("instruksi_kerjas.nama")
        ->where("tahapan_iks.id", $request->id_tahapan)->get();

        //Mengubah setiap value dari tiap param yang ada
        if (count($param)>0) {
            for ($i=0; $i < count($param); $i++) {
                DB::table("parameter_pemrosesans")
                ->select("parameter_pemrosesans.*")
                ->where("parameter_pemrosesans.id_parameter", $param[$i]["id"])
                ->where("parameter_pemrosesans.id_pemrosesan", $request->id_pemrosesan)
                ->where("parameter_pemrosesans.id_tahapan", $request->id_tahapan)
                ->update([
                    'value' => $param[$i]["value"],
                    ]);
            }
        }

        //Ini nentuin status dari tiap tahapannya
        DB::table("tahapan_pemrosesan_iks")
                    ->where("tahapan_pemrosesan_iks.id_pemrosesan", $request->id_pemrosesan)
                    ->where("tahapan_pemrosesan_iks.id_tahapan", $request->id_tahapan)
                    ->update([
                        'status' => "Sedang dikerjakan",
                    ]);

        if ($request->simpan_selesai == "true") {
            DB::table("tahapan_pemrosesan_iks")
                    ->where("tahapan_pemrosesan_iks.id_pemrosesan", $request->id_pemrosesan)
                    ->where("tahapan_pemrosesan_iks.id_tahapan", $request->id_tahapan)
                    ->update([
                        'status' => "Sudah Diproses",
                    ]);
        }

        //ini nentuin status dari pemrosesan IK tersebut (di data umum)
        $statuses = DB::table("tahapan_pemrosesan_iks")
                    ->select("tahapan_pemrosesan_iks.status")
                    ->where("tahapan_pemrosesan_iks.id_pemrosesan", $request->id_pemrosesan)
                    ->get()->pluck("status")->toArray();

        if (in_array("Belum diproses",$statuses) || in_array("Sedang dikerjakan",$statuses)) {
            DB::table("pemrosesans")
            ->where("pemrosesans.id", $request->id_pemrosesan)
            ->update([
                'status' => "Sedang dikerjakan",
            ]);
        }


        //Menyimpan aktivitas user
        DB::table('aktivitas_users')->insert([
            'id_project' => 1, //toDo nanti ganti
            'id_user' => $user->id,
            'jenis_aktivitas' => "Menambah hasil pemrosesan tahapan IK",
            'id_aktivitas' => $request->id_pemrosesan, //nyambung ke id pemrosesan
            'detail_aktivitas' => "Melakukan pemrosesan pada tahapan ke-[".$namaIK."] pada [",
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        $serializedData = TahapanPemrosesanIk::serializePemrosesanIkItem($request->id_pemrosesan);
        return response()->json([
            "status" => 1,
            "data" => $serializedData,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TahapanPemrosesanIk  $tahapanPemrosesanIk
     * @return \Illuminate\Http\Response
     */
    public function selesaikanPemrosesanIK(Request $request)
    {
        $idPemrosesan = $request->id_pemrosesan;
        $pemrosesanIK = DB::table("pemrosesan_iks")
            ->select("pemrosesan_iks.*")
            ->where("pemrosesan_iks.id_pemrosesan", $idPemrosesan)
            ->get();

        if (!$pemrosesanIK) {
            return response()->json([
                'status' => 0,
                'message' => "Data tidak ditemukan"
            ]);
        }

        DB::table("pemrosesans")
            ->where("pemrosesans.id", $idPemrosesan)
            ->update([
                'status' => "Selesai",
            ]);

        $file = $request->file('file_hasil');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000, 1001238912) . "." . $ext;

        DB::table("pemrosesan_iks")
            ->where("pemrosesan_iks.id_pemrosesan", $idPemrosesan)
            ->update([
                'path_file_hasil' => $newName,
                'jumlah_sisa' => $request->jumlah_sisa
            ]);
        $file->move('uploads/file', $newName);

        $serializedData = TahapanPemrosesanIk::serializePemrosesanIkItem($idPemrosesan);
        return response()->json([
            "status" => 1,
            "data" => $serializedData,
        ]);
    }

    public function cancelPemrosesanIK(Request $request)
    {
        $idPemrosesan = $request->id_pemrosesan;
        $pemrosesanIK = DB::table("pemrosesan_iks")
            ->select("pemrosesan_iks.*")
            ->where("pemrosesan_iks.id_pemrosesan", $idPemrosesan)
            ->get();

        if (!$pemrosesanIK) {
            return response()->json([
                'status' => 0,
                'message' => "Data tidak ditemukan"
            ]);
        }

        DB::table("pemrosesans")
            ->where("pemrosesans.id", $idPemrosesan)
            ->update([
                'status' => "Dibatalkan",
            ]);
        return response()->json([
            "status" => 1,
            "message" => "Pemrosesan berhasil dibatalkan"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TahapanPemrosesanIk  $tahapanPemrosesanIk
     * @return \Illuminate\Http\Response
     */
    public function destroyIK(Request  $request)
    {

        $pemrosesanIK = DB::table("pemrosesan_iks")
        ->select("pemrosesan_iks.*")
        ->where("pemrosesan_iks.id_pemrosesan", $request->id_pemrosesan)
            ->get();

        if ($pemrosesanIK) {

            $pemrosesan = DB::table("pemrosesans")
            ->select("pemrosesans.*")
            ->where("pemrosesans.id", $request->id_pemrosesan)
                ->get();

            $pemrosesan->delete();
            return response()->json([
                "status" => 1,
                'message' => "Data berhasil dihapus"
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data tidak ditemukan"
            ]);
        }
    }
}
