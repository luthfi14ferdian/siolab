<?php

namespace App\Http\Controllers\surface_treatment;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\PemrosesanAlat;
use App\TahapanPemrosesanIk;

function toId($array)
{
    return array_map(
        function ($item) {
            return $item->id;
        },
        $array
    );
};

class PemrosesanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Determine whether user asks for all types of pemrosesan,
        // "Instruksi Kerja" only, or "Percobaan" only

        $jenis = $request->jenis;
        if ($jenis !== "Instruksi Kerja" && $jenis !== "Percobaan") {
            $jenis = null;
        }

        if ($jenis) {
            $query = DB::table("pemrosesans")
                ->where("pemrosesans.jenis", $jenis)->where('id_project',$request->id_project);
        } else {
            $query = DB::table("pemrosesans")->where('id_project',$request->id_project);
        }

        // Query initial pemrosesan items using paginate

        $pagination = $query->orderBy("id")->paginate(10);
        $pemrosesanItems = $pagination->items();

        $idToArrIndex = [];
        foreach ($pemrosesanItems as $index => $item) {
            $idToArrIndex[$item->id] = $index;
        }
        unset($item);

        // Select joined IK and pemrosesan data with ID appearing in
        // query from pagination.

        $pemrosesanIkIds = toId(array_filter($pemrosesanItems, function ($item) {
            return $item->jenis === "Instruksi Kerja";
        }));

        $pemrosesanIks = DB::table("pemrosesan_iks")
            ->whereIn("id_pemrosesan", $pemrosesanIkIds)
            ->join("instruksi_kerjas", "pemrosesan_iks.id_ik", "=", "instruksi_kerjas.id")
            ->select("pemrosesan_iks.id_pemrosesan", "instruksi_kerjas.nama")
            ->get()
            ->toArray();

        foreach ($pemrosesanIks as $pemrosesanIk) {
            $idPemrosesan = $pemrosesanIk->id_pemrosesan;
            $index = $idToArrIndex[$idPemrosesan];
            $pemrosesanItems[$index]->nama_ik = $pemrosesanIk->nama;
        }

        // Select joined alat and pemrosesan percobaan data with ID appearing in
        // query from pagination.

        $pemrosesanPercobaanIds = toId(array_filter($pemrosesanItems, function ($item) {
            return $item->jenis === "Percobaan";
        }));

        $pemrosesanAlats = DB::table("percobaan_alats")
            ->whereIn("id_pemrosesan", $pemrosesanPercobaanIds)
            ->join("alats", "percobaan_alats.id_alat", "=", "alats.id")
            ->select("alats.nama", "percobaan_alats.id_pemrosesan")
            ->get();

        $groupedPemrosesan = array();
        foreach ($pemrosesanAlats as $item) {
            $groupedPemrosesan[$item->id_pemrosesan][] = $item->nama;
        }
        unset($item);

        foreach ($groupedPemrosesan as $idPemrosesan => $item) {
            $index = $idToArrIndex[$idPemrosesan];
            $pemrosesanItems[$index]->alat = $item;
        }

        // Return response

        return response()->json([
            "status" => 1,
            "data" => [
                "pemrosesan" => $pemrosesanItems,
            ],
            "pagination" => [
                "current_page" => $pagination->currentPage(),
                "total_item" => $pagination->total(),
                "items_per_page" => $pagination->perPage(),
            ],
        ]);
    }

    /**
     * Get pemrosesan item given id_pemrosesan. If item is an IK pemrosesan,
     * returns pemrosesan with IK data. Othwerwise, returns percobaan item.
     */

    public function show(Request $request)
    {
        $items = DB::table("pemrosesans")
            ->select("pemrosesans.jenis")
            ->where("pemrosesans.id", $request->id_pemrosesan)
            ->get();

        if (count($items) < 1) {
            return response()->json([
                'status' => 0,
                'message' => "Data tidak ditemukan"
            ]);
        }

        $jenisPemrosesan = $items->pluck("jenis")[0];

        if ($jenisPemrosesan == "Instruksi Kerja") {
            $pemrosesanIk = TahapanPemrosesanIk::serializePemrosesanIkItem($request->id_pemrosesan);
            return response()->json([
                "status" => 1,
                "data" => $pemrosesanIk,
            ]);
        } else {
            $serializedPercobaan = PemrosesanAlat::serializePercobaanItem($request->id_pemrosesan);
            return response()->json([
                "status" => 1,
                "data" => $serializedPercobaan
            ]);
        }
    }
}
