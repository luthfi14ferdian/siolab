<?php

namespace App\Http\Controllers\surface_treatment;

use App\Http\Controllers\Controller;
use App\InstruksiKerja;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TahapanIKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $instruksiKerja = InstruksiKerja::find($request->id_item);

        if ($instruksiKerja) {

            $allTahapanIK = DB::table("tahapan_iks")
            ->select('tahapan_iks.*')
            ->where('id_ik', $request->id_item)
            ->get();

            $tahapanIK = array_chunk($allTahapanIK->toArray(), 10);
            if (count($tahapanIK) <= (($request->page)-1) ) {
                $tahapanIK = [];
            }
            else {
                $tahapanIK = $tahapanIK[($request->page)-1];
            }

            for ($i=0; $i < count($tahapanIK); $i++) {
                $idAlats = DB::table("alat_tahapan_iks")
                ->select('id_alat')
                ->where('id_tahapan', $tahapanIK[$i]->id)
                ->get()
                ->pluck("id_alat");

                $alats = DB::table("alats")
                ->select('alats.*')
                ->whereIn("id",$idAlats)
                ->get();


                // dd(collect($tahapanIK[$i]));
                $tahapanIK[$i] = collect($tahapanIK[$i])->put("alat", $alats);
            }

            $total_item = count($allTahapanIK);
            $current_page = (int)$request->page;
            $total_item_per_page = count($tahapanIK);

            return response()->json([
                "status" => 1,
                "data" => [
                    "tahapan_ik" => $tahapanIK,
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_per_page" => $total_item_per_page,
                ],
            ]);
        }

        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $instruksiKerja = InstruksiKerja::find($request->id_item);

        if ($instruksiKerja) {
            $id = DB::table('tahapan_iks')->insertGetId([
                            'id_ik' => $request->id_item,
                            'uraian' => $request->uraian,
                            'urutan' => $request->urutan,
                            'created_at' => date("Y-m-d H:i:s"),
            ]);

            for ($i=0; $i < count($request->id_alat); $i++) {
                DB::table('alat_tahapan_iks')->insert([
                    'id_tahapan' => $id,
                    'id_alat' => $request->id_alat[$i],
                    'created_at' => date("Y-m-d H:i:s"),
                ]);
            }

            return response()->json([
                "status" => 1,
                "message" => "Data berhasil disimpan"
            ], 200);

        }

        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $tahapanIK = DB::table("tahapan_iks")
            ->select('tahapan_iks.*')
            ->where('id', $request->id_item)
            ->get();

        if ($tahapanIK) {

            DB::table('tahapan_iks')
            ->select('tahapan_iks.*')
            ->where('id', $request->id_item)
            ->update([
                'id_ik' => $request->id_item,
                'uraian' => $request->uraian,
                'urutan' => $request->urutan,
                'updated_at' => date("Y-m-d H:i:s"),
            ]);

            DB::table("alat_tahapan_iks")
            ->where('id_tahapan', $tahapanIK[0]->id)
            ->delete();

            for ($i=0; $i < count($request->id_alat); $i++) {
                DB::table('alat_tahapan_iks')->insert([
                    'id_tahapan' => $tahapanIK[0]->id,
                    'id_alat' => $request->id_alat[$i],
                    'created_at' => date("Y-m-d H:i:s"),
                ]);
            }

            return response()->json([
                "status" => 1,
                "message" => "Data berhasil disimpan"
            ], 200);

        }

        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $tahapanIK = DB::table("tahapan_iks")
            ->select('tahapan_iks.*')
            ->where('id', $request->id_item)
            ->get();

        if ($tahapanIK) {

            $tahapanIK = DB::table("tahapan_iks")
            ->select('tahapan_iks.*')
            ->where('id', $request->id_item)
            ->delete();

            return response()->json([
                "status" => 1,
                "message" => "Data berhasil dihapus"
            ], 200);

        }

        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    /**
     * Add new tahapan IK items related to certain IK to DB
     */

    private function addItems($items, $ikId) {
        foreach ($items as &$item) {
            $id = DB::table('tahapan_iks')->insertGetId([
                'id_ik' => $ikId,
                'uraian' => $item['uraian'],
                'urutan' => $item['urutan'],
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            if (array_key_exists('id_alat', $item)) {
                $this->replaceAlatItems($item['id_alat'], $id);
            }
        }

        unset($item);
    }


    /**
     * Delete certain tahapan items related to certain IK to DB
     */

    private function transactDeleteItems($tahapanIds) {
        foreach ($tahapanIds as &$id) {
            $tahapanIK = DB::table("tahapan_iks")
                ->select('tahapan_iks.*')
                ->where('id', $id)
                ->get();

            if (!$tahapanIK) {
                continue;
            }

            // Delete tahapan IK item and its alat items

            DB::table("tahapan_iks")
                ->select('tahapan_iks.*')
                ->where('id', $id)
                ->delete();
            $this->replaceAlatItems([], $id);
        }

        unset($id);
    }


    /**
     * Update certain tahapan items related to certain IK to DB
     */

    private function transactUpdateItems($items) {
        foreach ($items as &$item) {
            $id = $item['id'];

            $tahapanIK = DB::table("tahapan_iks")
                ->select('tahapan_iks.*')
                ->where('id', $id)
                ->get();

            if (!$tahapanIK) {
                continue;
            }

            DB::table('tahapan_iks')
            ->select('tahapan_iks.*')
            ->where('id', $id)
            ->update([
                'uraian' => $item['uraian'],
                'urutan' => $item['urutan'],
                'updated_at' => date("Y-m-d H:i:s"),
            ]);

            if (array_key_exists('id_alat', $item)) {
                $this->replaceAlatItems($item['id_alat'], $id);
            } else {
                $this->replaceAlatItems([], $id);
            }
        }

        unset($item);
    }


    /**
     * Replace all existing alat items of certain tahapan
     * with new ones from items.
     */

    private function replaceAlatItems($items, $tahapanId) {
        DB::table("alat_tahapan_iks")
        ->where('id_tahapan', $tahapanId)
        ->delete();

        for ($i=0; $i < count($items); $i++) {
            DB::table('alat_tahapan_iks')->insert([
                'id_tahapan' => $tahapanId,
                'id_alat' => $items[$i],
                'created_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }

    /**
     * Manage difference of all tahapan items related to IK item
     */

    public function editStepsOfIKItem(Request $request) {

        $tahapanIK = DB::table("tahapan_iks")
            ->select('tahapan_iks.*')
            ->where('id', $request->id_item)
            ->get();

        if (!$tahapanIK) {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }

        // Add items without lock transaction

        $addedItems = $request->added;
        if ($addedItems) {
            $this->addItems($addedItems, $request->id_item);
        }

        // Lock transaction for deleted and updated

        DB::transaction(function() use ($request) {
            $deletedItems = $request->deleted;
            $updatedItems = $request->updated;

            if ($deletedItems) {
                $this->transactDeleteItems($deletedItems);
            }

            if ($updatedItems) {
                $this->transactUpdateItems($updatedItems);
            }

        });

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil diubah"
        ], 200);
    }

}
