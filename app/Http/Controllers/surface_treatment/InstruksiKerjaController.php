<?php

namespace App\Http\Controllers\surface_treatment;

use App\Http\Controllers\Controller;
use App\InstruksiKerja;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use File;

class InstruksiKerjaController extends Controller
{
    /**
     * Search items from query. If no query is supplied, this behaves the same
     * as InstruksiKerjaController:index
     *
     * @return \Illuminate\Http\Response
     */

    public function searchFromQuery(Request $request)
    {
        $queryNama = $request->nama;
        $queryNama = isset($queryNama) ? trim($queryNama) : '';

        $queryDb = DB::table("instruksi_kerjas")
            ->select('instruksi_kerjas.*');

        if ($queryNama !== '') {
            $queryDb = $queryDb->where('nama', 'LIKE', '%' . $queryNama . '%');
        }

        $isAcceptedOnly = isset($request->disetujui_only) && $request->disetujui_only === 'true';
        if ($isAcceptedOnly) {
            $queryDb = $queryDb->whereNotNull('disetujui_oleh');
        }

        $paginationData = $queryDb->paginate(10);

        $items = $paginationData->items();
        $total_item = $paginationData->total();
        $current_page = $paginationData->currentPage();
        $total_item_per_page = $paginationData->perPage();

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $items
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new InstruksiKerja();
        $data->nama = $request->input('nama');

        $file = $request->file('path_file_ik');
        $ext = $file->getClientOriginalExtension();
        $newName1 = rand(100000, 1001238912) . "." . $ext;
        $file->move('uploads/file', $newName1);
        $data->path_file_ik = $newName1;

        $file1 = $request->file('path_file_hasil_ik');
        $ext = $file1->getClientOriginalExtension();
        $newName2 = rand(100000, 1001238912) . "." . $ext;
        $file1->move('uploads/file', $newName2);
        $data->path_file_hasil_ik = $newName2;

        $data->disiapkan_oleh = Auth::user()->id;
        $data->kode = $request->input('kode');
        $data->tujuan = $request->input('tujuan');
        $data->save();

        $user =  Auth::user();
        DB::table('aktivitas_users')->insert([
            'id_project' => 1,
            'id_user' => $user->id,
            'jenis_aktivitas' => "Menambah Instruksi Kerja",
            'id_aktivitas' => $data->id,
            'detail_aktivitas' => "Menambah Instruksi Kerja Baru Surface Treatment, yaitu " . $data->nama,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan"
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InstruksiKerja  $instruksiKerja
     * @return \Illuminate\Http\Response
     */
    public function showUmum(Request $request)
    {
        $instruksiKerja = InstruksiKerja::find($request->id_item);

        if ($instruksiKerja) {
            $responseData = $instruksiKerja->replicate();

            $preparerId = $instruksiKerja->disiapkan_oleh;
            $checkerId = $instruksiKerja->diperiksa_oleh;
            $approverId = $instruksiKerja->disetujui_oleh;

            $responseData->disiapkan_oleh =  User::find($preparerId);
            $responseData->diperiksa_oleh =  User::find($checkerId);
            $responseData->disetujui_oleh =  User::find($approverId);

            return response()->json([
                "status" => 1,
                "data" => [
                    "instruksi_kerja" => $responseData,
                ],
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    public function acknowledgeItem(Request $request)
    {
        $instruksiKerja = InstruksiKerja::find($request->id_item);
        if (!$instruksiKerja) {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
        $alreadyDiperiksa = $instruksiKerja->diperiksa_oleh !== null;
        if ($alreadyDiperiksa) {
            return response()->json([
                'status' => 0,
                'message' => "Instruksi kerja ini sudah diperiksa."
            ]);
        }
        $alreadyDisetujui = $instruksiKerja->disetujui_oleh !== null;
        if ($alreadyDisetujui) {
            return response()->json([
                'status' => 0,
                'message' => "Instruksi kerja ini sudah disetujui."
            ]);
        }

        $userId = Auth::user()->id;
        $instruksiKerja->diperiksa_oleh = $userId;
        $instruksiKerja->save();
        return response()->json([
            'status' => 1,
            'message' => "Instruksi kerja berhasil ditandai sudah diperiksa."
        ]);
    }

    public function approveItem(Request $request)
    {
        $instruksiKerja = InstruksiKerja::find($request->id_item);
        if (!$instruksiKerja) {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
        $alreadyDiperiksa = $instruksiKerja->diperiksa_oleh !== null;
        if (!$alreadyDiperiksa) {
            return response()->json([
                'status' => 0,
                'message' => "Instruksi kerja ini belum diperiksa."
            ]);
        }
        $alreadyDisetujui = $instruksiKerja->disetujui_oleh !== null;
        if ($alreadyDisetujui) {
            return response()->json([
                'status' => 0,
                'message' => "Instruksi kerja ini sudah disetujui."
            ]);
        }

        $userId = Auth::user()->id;
        $instruksiKerja->disetujui_oleh = $userId;
        $instruksiKerja->save();
        return response()->json([
            'status' => 1,
            'message' => "Instruksi kerja berhasil disetujui."
        ]);
    }

    private function replaceFile($instruksiKerja, $request, $propertyName)
    {
        if ($request->hasFile($propertyName)) {
            $file = $request->file($propertyName);

            if ($instruksiKerja[$propertyName]) {
                File::delete('uploads/file/' . $instruksiKerja[$propertyName]);
            }

            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000, 1001238912) . "." . $ext;
            $file->move('uploads/file', $newName);
            $instruksiKerja[$propertyName] = $newName;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InstruksiKerja  $instruksiKerja
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $instruksiKerja = InstruksiKerja::find($request->id_item);

        if ($instruksiKerja) {
            $instruksiKerja->nama = $request->input('nama');
            $instruksiKerja->kode = $request->input('kode');
            $instruksiKerja->tujuan = $request->input('tujuan');

            $this->replaceFile($instruksiKerja, $request, 'path_file_ik');
            $this->replaceFile($instruksiKerja, $request, 'path_file_hasil_ik');

            $instruksiKerja->save();

            $user =  Auth::user();
            DB::table('aktivitas_users')->insert([
                'id_project' => DB::table('projects')->select("projects.id")->where("projects.id", 1)->get()[0]->id, //toDo nanti ganti
                'id_user' => $user->id,
                'jenis_aktivitas' => "Mengubah Instruksi Kerja",
                'id_aktivitas' => $instruksiKerja->id,
                'detail_aktivitas' => "Mengubah Instruksi Kerja, yaitu " . $instruksiKerja->nama,
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            return response()->json([
                "status" => 1,
                "message" => "Data berhasil diubah",
                "data" => [
                    "instruksi_kerja" => $instruksiKerja
                ],
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InstruksiKerja  $instruksiKerja
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $instruksiKerja = InstruksiKerja::find($request->id_item);

        if ($instruksiKerja) {

            $user =  Auth::user();
            DB::table('aktivitas_users')->insert([
                'id_project' => 1,
                'id_user' => $user->id,
                'jenis_aktivitas' => "Menghapus Instruksi Kerja",
                'id_aktivitas' => $instruksiKerja->id,
                'detail_aktivitas' => "Menghapus Instruksi Kerja, yaitu " . $instruksiKerja->nama,
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            $instruksiKerja->delete(); //masih hard delete

            return response()->json([
                "status" => 1,
                'message' => "Data berhasil dihapus"
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data tidak ditemukan"
            ]);
        }
    }

    /**
     * Testing buat upload -----------------------------------------------------------------------------------
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexUpload()
    {
        $data = InstruksiKerja::all();
        return view('file', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUpload()
    {
        return view('file_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeUpload(Request $request)
    {
        $data = new InstruksiKerja();
        $data->nama = $request->input('name');
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000, 1001238912) . "." . $ext;
        $file->move('uploads/file', $newName);
        $data->path = $newName;
        $data->disiapkan_oleh = 1;
        $data->kode = "ABCDEFG";
        $data->tujuan = "baik";
        $data->save();
        return redirect()->route('file.index')->with('alert-success', 'Data berhasil ditambahkan!');
    }
}
