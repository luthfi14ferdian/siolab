<?php

namespace App\Http\Controllers\surface_treatment;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Alat;
use App\PemrosesanAlat;
use App\PercobaanAlat;
use App\ParameterPemrosesan;

class PemrosesanAlatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $alats = DB::table("alats")
        ->select("alats.*")
        ->get()->toArray();

        $paramAlat = DB::table("param_alats")
        ->join("alats", "param_alats.id_alat", "=", "alats.id")
        ->select("param_alats.*")
        ->get();

        $alatTemp = [];
        foreach ($alats as $key => $value)  {
            $thisAlat = [];
            $thisAlat = collect($thisAlat)->put("id", $value->id);
            $thisAlat->put("kode", $value->kode);
            $thisAlat->put("nama", $value->nama);
            $thisAlat->put("tipe", $value->tipe);
            $thisAlat->put("fungsi", $value->fungsi);


            $param = DB::table("param_alats")
            ->select("param_alats.id","param_alats.parameter","param_alats.metric")
            ->where("param_alats.id_alat", $value->id)
            ->get();

            $thisAlat->put("parameters", $param->toArray());

            array_push($alatTemp, $thisAlat);

        }


        return response()->json([
            "status" => 1,
            "alats" => $alatTemp
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jenis' => 'required|max:128',
            'jumlah_diuji' => 'required',
            'no_batch' => 'required|max:128',
            'tujuan' => 'required|max:128',
            'judul' => 'required|max:128',
        ]);

        $data = $request->all();

        $id = DB::table('pemrosesans')->insertGetId([
            'jenis' => "Percobaan",
            'jumlah_diuji' => $data["jumlah_diuji"],
            'status' => "Sedang diproses",
            'no_batch' => $data["no_batch"],
            'created_at' => date("Y-m-d H:i:s"),
            'tanggal_dibuat' => $data['tanggal_dibuat'],
            'id_project' => $data['id_project'],
            'id_produk' => $data['id_produk'],
        ]);

        DB::table('pemrosesan_alats')->insert([
            'id_pemrosesan' => $id,
            "judul" => $data["judul"],
            "tujuan" => $data["tujuan"],
            "diproses_oleh" => Auth::user()->id,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        $alats = $data["alats"];
        $user =  Auth::user();
        for ($i = 0; $i < count($alats); $i++) {
            DB::table('percobaan_alats')->insert([
                'id_pemrosesan' => $id,
                "id_alat" => $alats[$i]["id"],
                "note" => $alats[$i]["note"],
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            $alatName = Alat::find($alats[$i]["id"])->nama;

            DB::table('aktivitas_users')->insert([
                'id_project' => DB::table('projects')
                    ->select("projects.id")
                    ->where("projects.id", 1)
                    ->get()[0]
                    ->id, //toDo nanti ganti
                'id_user' => $user->id,
                'jenis_aktivitas' => "Menambah Percobaan",
                'id_aktivitas' => $alats[$i]["id"],
                'detail_aktivitas' => "Melakukan percobaan dengan alat  [" . $alatName . "]",
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            foreach ($alats[$i]["parameters"] as $key => $param) {
                DB::table('parameter_pemrosesans')->insert([
                    'id_parameter' => $param["id"],
                    "id_pemrosesan" => $id,
                    "value" => $param["value"],
                ]);
            }
        }

        if ($data["simpan_selesai"] == "true") {

            DB::table("pemrosesan_alats")
                ->where("pemrosesan_alats.id_pemrosesan", $id)
                ->update([
                    "jumlah_sisa" => $data["jumlah_sisa"],
                    'tanggal_selesai' => date("Y-m-d H:i:s"),
                ]);
            DB::table("pemrosesans")
                ->where("pemrosesans.id", $id)
                ->update([
                    "status" => "Selesai",
                ]);

            $datas= PemrosesanAlat::serializePercobaanItem($id);
            DB::table("pemrosesans")
            ->where("pemrosesans.id", $id)
            ->update([
                "data" =>json_encode($datas),
            ]);
        }


        $dataUmum = DB::table("pemrosesan_alats")
            ->join("pemrosesans", "pemrosesan_alats.id_pemrosesan", "=", "pemrosesans.id")
            ->join("users", "pemrosesan_alats.diproses_oleh", "=", "users.id")
            ->select(
                "pemrosesans.*",
                "pemrosesan_alats.tujuan",
                "pemrosesan_alats.judul",
                "pemrosesan_alats.created_at",
                "users.first_name",
                "users.last_name",
                "pemrosesan_alats.jumlah_sisa",
                "pemrosesan_alats.tanggal_selesai"
            )
            ->where("pemrosesans.id", $id)
            ->get();

        $alats = DB::table("percobaan_alats")
            ->join("alats", "percobaan_alats.id_alat", "=", "alats.id")
            ->join("pemrosesans", "percobaan_alats.id_pemrosesan", "=", "pemrosesans.id")
            ->select("alats.*", "percobaan_alats.note")
            ->where("pemrosesans.id", $id)
            ->get();

        $alatTemp = [];
        foreach ($alats as $key => $value) {
            $thisAlat = [];
            $thisAlat = collect($thisAlat)->put("id", $value->id);
            $thisAlat->put("kode", $value->kode);
            $thisAlat->put("nama", $value->nama);
            $thisAlat->put("tipe", $value->tipe);
            $thisAlat->put("fungsi", $value->fungsi);

            $param = DB::table("parameter_pemrosesans")
                ->join("param_alats", "parameter_pemrosesans.id_parameter", "=", "param_alats.id")
                ->select(
                    "param_alats.id",
                    "param_alats.parameter",
                    "param_alats.metric",
                    "parameter_pemrosesans.value"
                )
                ->where("param_alats.id_alat", $value->id)
                ->get();

            $thisAlat->put("parameters", $param->toArray());

            array_push($alatTemp, $thisAlat);
        }

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan",
            "data" => [
                "data_umum" => $dataUmum,
                "alats" => $alatTemp,
            ]

        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(request $request)
    {
        $serializedPercobaan = PemrosesanAlat::serializePercobaanItem($request->id_pemrosesan);
        if ($serializedPercobaan) {
            return response()->json([
                "status" => 1,
                "data" => $serializedPercobaan
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message'=> "Data tidak ditemukan"
            ], 200);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showEditPercobaan(Request $request)
    {
        $alats = DB::table("percobaan_alats")
        ->join("alats", "percobaan_alats.id_alat", "=", "alats.id")
        ->join("pemrosesans", "percobaan_alats.id_pemrosesan", "=", "pemrosesans.id")
        ->select("alats.*", "percobaan_alats.note")
        ->where("pemrosesans.id", $request->id_pemrosesan )
        ->get();

        $alatTemp = [];
        foreach ($alats as $key => $value)  {
            $thisAlat = [];
            $thisAlat = collect($thisAlat)->put("id", $value->id);
            $thisAlat->put("kode", $value->kode);
            $thisAlat->put("nama", $value->nama);
            $thisAlat->put("tipe", $value->tipe);
            $thisAlat->put("fungsi", $value->fungsi);


            $param = DB::table("parameter_pemrosesans")
            ->join("param_alats", "parameter_pemrosesans.id_parameter", "=", "param_alats.id")
            ->select("param_alats.id", "param_alats.parameter", "param_alats.metric","parameter_pemrosesans.value")
            ->where("param_alats.id_alat", $value->id )
            ->get();

            $thisAlat->put("parameters", $param->toArray());

            array_push($alatTemp, $thisAlat);

        }

        return response()->json([
            "status" => 1,
            "data" => [
                "alats" => $alatTemp,
            ]

        ], 200);
}

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showEditPemrosesan(Request $request)
    {
        $pemrosesan = DB::table("pemrosesans")
        ->join("pemrosesan_alats", "pemrosesans.id", "=", "pemrosesan_alats.id_pemrosesan")
        ->select("pemrosesans.*", "pemrosesan_alats.*")
        ->where("pemrosesans.id", $request->id_pemrosesan )
        ->get();

        return response()->json([
            "status" => 1,
            "data" => [
                "pemrosesan" => $pemrosesan,
            ]

        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updatePercobaan(Request $request)
    {
        $validatedData = $request->validate([
            'tujuan' => 'required|max:128',
            'jumlah_diuji' => 'required',
            'simpan_selesai' => 'required',
            'no_batch' => 'required|max:128',
        ]);

        DB::table("pemrosesans")
        ->where("id", $request->id_pemrosesan)
        ->update([
            "jumlah_diuji" => $request->jumlah_diuji,
            "no_batch" => $request->no_batch,
            "updated_at" => date("Y-m-d H:i:s")
        ]);

        DB::table("pemrosesan_alats")
        ->where("id_pemrosesan", $request->id_pemrosesan)
        ->update([
            "tujuan"=>$request->tujuan,
            "judul"=>$request->judul,
            "updated_at" => date("Y-m-d H:i:s")
        ]);

        $percobaan_alat = PercobaanAlat::where('id_pemrosesan', $request->id_pemrosesan)->delete();

        $alats =$request->all()["alats"];
        for ($i=0; $i < count($alats); $i++) {
            $percobaan_alats = new PercobaanAlat();
            $percobaan_alats->id_pemrosesan = $request->id_pemrosesan;
            $percobaan_alats->id_alat = $alats[$i]["id"];
            $percobaan_alats->note = $alats[$i]["note"];
            $percobaan_alats->save();


            foreach ($alats[$i]["parameters"] as $param ) {
                $parameter_pemrosesan = ParameterPemrosesan::where('id_pemrosesan',$request->id_pemrosesan)->where('id_parameter',$param["id"])->delete();
                $new_parameter= new ParameterPemrosesan();
                $new_parameter->id_parameter=  $param["id"];
                $new_parameter->id_pemrosesan=  $request->id_pemrosesan;
                $new_parameter->value=  $param["value"];
                $new_parameter->save();
            }
        }

        if ($request->all()["simpan_selesai"] == "true") {
            DB::table("pemrosesan_alats")
            ->where("pemrosesan_alats.id_pemrosesan", $request->id_pemrosesan)
            ->update([
                "jumlah_sisa" => $request->all()["jumlah_sisa"],
                'tanggal_selesai' => date("Y-m-d H:i:s"),
            ]);
            DB::table("pemrosesans")
            ->where("pemrosesans.id", $request->id_pemrosesan)
            ->update([
                "status" => "Selesai",
            ]);

            $datas= PemrosesanAlat::serializePercobaanItem($request->id_pemrosesan);
            DB::table("pemrosesans")
            ->where("pemrosesans.id", $request->id_pemrosesan)
            ->update([
                "data" =>json_encode($datas),
            ]);
        }

        return response()->json([
            "status" => 1,
            "data" => [
                "pemrosesan" => PemrosesanAlat::serializePercobaanItem($request->id_pemrosesan),
            ]
        ], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $pemrosesanAlat = DB::table("pemrosesan_alats")
        ->select("pemrosesan_alats.*")
        ->where("pemrosesan_alats.id_pemrosesan", $request->id_pemrosesan)
            ->get();

        if ($pemrosesanAlat) {

            DB::table("pemrosesans")
            ->select("pemrosesans.*")
            ->where("pemrosesans.id", $request->id_pemrosesan)
                ->delete();


            return response()->json([
                "status" => 1,
                'message' => "Data berhasil dihapus"
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data tidak ditemukan"
            ]);
        }
    }
}
