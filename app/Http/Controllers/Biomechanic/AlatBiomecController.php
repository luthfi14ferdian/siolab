<?php

namespace App\Http\Controllers\Biomechanic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\AlatBiomec;
use App\Model\KalibrasiAlat;
use App\Model\ParamAlatBiomec;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Helpers\FilePathUtils;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class AlatBiomecController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $queryKode = $request->kode;
        $queryKode = isset($queryKode) ? trim($queryKode) : '';

        if ($queryKode === '') {
            $queryDb = DB::table("alat_biomecs")
            ->select('alat_biomecs.*');
        } else {
            $queryDb = DB::table("alat_biomecs")
            ->where('kode', 'LIKE', '%'.$queryKode.'%');
        }

        $paginate = $queryDb->paginate(10);

        return response()->json([
            "status" => 1,
            "message"=>'Success',
            "data" => [
                "item" => $paginate->items(),
            ],
            "pagination" => [
                "current_page" => $paginate->currentPage(),
                "total_item" => $paginate->total(),
                "items_per_page" => $paginate->perPage(),
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function getAllAlat(Request $request){
        $alats = DB::table("alat_biomecs")->select('alat_biomecs.*')->get();

        return response()->json([
            "status" => 1,
            "data"=>$alats,
        ], 200);
     }

    public function store(Request $request)
    {

        DB::transaction(function () use ($request) {
            $data = $request->all();
            $alat = new AlatBiomec();
            if($request->gambar_file != null){
                $name= FilePathUtils::getRandomFileName($request->file('gambar_file'));
                $path= Storage::putFileAs('alat_biomechanic', new File($request->gambar_file), $name);
                $alat->nama_file = $name;
                $alat->gambar_path = $path;
            }
            $alat->kode = $data['kode'];
            $alat->nama = $data['nama'];
            $alat->tipe = $data['tipe'];
            $alat->fungsi = $data['fungsi'];
            $alat->save();

            for ($i = 0; $i < count($data['parameters']); $i++) {
                $paramAlat = new ParamAlatBiomec();
                $paramAlat->id_alat = $alat->id;
                $paramAlat->parameter = $data['parameters'][$i]['param'];
                $paramAlat->metric = $data['parameters'][$i]['metric'];
                $paramAlat->save();
            }

            $user =  Auth::user();
            DB::table('aktivitas_users')->insert([
                'id_project' => DB::table('projects')->select("projects.id")->where("projects.id", 1)->get()[0]->id, //toDo nanti ganti
                'id_user' => $user->id,
                'jenis_aktivitas' => "Menambah Alat",
                'id_aktivitas' => $alat->id,
                'detail_aktivitas' => "Menambah Alat Baru Surface Treatment, yaitu " . $alat->nama,
                'created_at' => date("Y-m-d H:i:s"),
            ]);
        });


        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan"
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alat  $alat
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $alat = AlatBiomec::find($request->id_item);
        if ($alat) {
            $paramAlat = DB::table("param_alat_biomecs")
            ->select('param_alat_biomecs.*')
            ->where('param_alat_biomecs.id_alat', $alat->id)
            ->get();

            return response()->json([
                "status" => 1,
                "data" => [
                    "alat" => $alat,
                    "param_alat" => $paramAlat
                ],
            ]);
        }
        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    /**
     * Get alat item from code
     *
     * @param  \App\Alat  $alat
     * @return \Illuminate\Http\Response
     */
    public function showFromKode(Request $request)
    {
        $alat = AlatBiomec::where('kode', $request->kode)->first();
        if ($alat) {
            $paramAlat = DB::table("param_alat_biomecs")
            ->select('param_alat_biomecs.*')
            ->where('param_alat_biomecs.id_alat', $alat->id)
            ->get();

            return response()->json([
                "status" => 1,
                "data" => [
                    "alat" => $alat,
                    "param_alat" => $paramAlat
                ],
            ]);
        }
        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alat  $alat
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $data = $request->all();
        $alat = AlatBiomec::find($request->id_item);

        if ($alat) {

            if($request->gambar_file != null){
                Storage::disk('public')->delete( $alat->gambar_path);
                $name= FilePathUtils::getRandomFileName($request->file('gambar_file'));
                $path= Storage::putFileAs('alat_surface_biomechanic', new File($request->gambar_file), $name);
                $alat->nama_file = $name;
                $alat->gambar_path = $path;
            }

            $alat->kode = $data['kode'];
            $alat->nama = $data['nama'];
            $alat->tipe = $data['tipe'];
            $alat->fungsi = $data['fungsi'];
            $alat->save();

            for ($i=0; $i < count($data['parameters']); $i++) {
                if (array_key_exists("id",$data['parameters'][$i])) {
                    $paramAlat = ParamAlatBiomec::find($data['parameters'][$i]['id']);
                    if ($paramAlat) {
                        $paramAlat->parameter = $data['parameters'][$i]['param'];
                        $paramAlat->metric = $data['parameters'][$i]['metric'];
                        $paramAlat->save();
                    }
                }
                else {
                    $paramAlat = new ParamAlatBiomec();
                    $paramAlat->id_alat = $alat->id;
                    $paramAlat->parameter = $data['parameters'][$i]['param'];
                    $paramAlat->metric = $data['parameters'][$i]['metric'];
                    $paramAlat->save();
                }


            }

            // Delete parameters
            if (array_key_exists("deleted_parameters_id", $data)
                && is_array($data['deleted_parameters_id'])) {

                foreach($data['deleted_parameters_id'] as $param_id) {
                    DB::table("param_alat_biomecs")
                    ->select('param_alat_biomecs.*')
                    ->where('param_alat_biomecs.id_alat', $alat->id)
                    ->where('param_alat_biomecs.id', $param_id)
                    ->delete();
                }
                unset($param_id);
            }

            $paramAlat = DB::table("param_alat_biomecs")
            ->select('param_alat_biomecs.*')
            ->where('param_alat_biomecs.id_alat', $alat->id)
            ->get();

            $user =  Auth::user();
            DB::table('aktivitas_users')->insert([
                'id_project' => DB::table('projects')->select("projects.id")->where("projects.id",1)->get()[0]->id, //toDo nanti ganti
                'id_user' => $user->id,
                'jenis_aktivitas' => "Mengubah Alat",
                'id_aktivitas' => $alat->id,
                'detail_aktivitas' => "Mengubah Alat, yaitu ".$alat->nama,
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            return response()->json([
                "status" => 1,
                "message" => "Data berhasil diubah",
                "data" => [
                    "alat" => $alat,
                    "param_alat" => $paramAlat
                ],
            ]);

        }



        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alat  $alat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $alat = AlatBiomec::find($request->id_item);

        if ($alat) {

            $user =  Auth::user();
            DB::table('aktivitas_users')->insert([
                'id_project' => DB::table('projects')->select("projects.id")->where("projects.id",1)->get()[0]->id, //toDo nanti ganti
                'id_user' => $user->id,
                'jenis_aktivitas' => "Menghapus Alat",
                'id_aktivitas' => $alat->id,
                'detail_aktivitas' => "Menghapus Alat, yaitu ".$alat->nama,
                'created_at' => date("Y-m-d H:i:s"),
            ]);
            Storage::disk('public')->delete( $alat->gambar_path);
            $alat->delete();
            return response()->json([
                "status" => 1,
                'message'=> "Data berhasil dihapus"
            ]);
        }

        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data tidak ditemukan"
            ]);
        }
    }

    public function getKalibrasi(Request $request){
        $kalibrasi_alat= KalibrasiAlat::where('id_alat', $request->id_alat)->get();
        if($kalibrasi_alat){
            return response()->json([
                'status' => 1,
                'message'=> "Success",
                'data'=>$kalibrasi_alat,
            ]);
        }

        else{
            return response()->json([
                'status' => 1,
                'message'=> "Data kosong",
            ]);
        }
    }

    public function setKalibrasi(Request $request){
        $kalibrasi_alat = new KalibrasiAlat();
        $kalibrasi_alat->id_alat = $request->id_alat;
        $kalibrasi_alat->tanggal = $request->tanggal;
        $kalibrasi_alat->note = $request->note;
        $kalibrasi_alat->save();

        return response()->json([
            'status' => 1,
            'message'=> "Data berhasil disimpan",
            'data'=>$kalibrasi_alat,
        ]);
    }

    public function editKalibrasi(Request $request){
        $kalibrasi_alat= KalibrasiAlat::find($request->id);
        $kalibrasi_alat->tanggal= $request->tanggal;
        $kalibrasi_alat->note = $request->note;
        $kalibrasi_alat->save();

        return response()->json([
            'status' => 1,
            'message'=> "Data berhasil diubah",
            'data'=>$kalibrasi_alat,
        ]);
    }

    public function deleteKalibrasi(Request $request){
        $kalibrasi_alat= KalibrasiAlat::find($request->id);
        $kalibrasi_alat->delete();

        return response()->json([
            'status' => 1,
            'message'=> "Data berhasil dihapus",
        ]);
    }
}
