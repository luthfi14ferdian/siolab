<?php

namespace App\Http\Controllers\Biomechanic;

use App\Http\Controllers\Controller;
use App\TahapanPemrosesanIkBiomec;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class TahapanPemrosesanIkBiomecController extends Controller
{
    public function storeIK(Request $request)
    {
        $userId =  Auth::user()->id;

        $id = DB::table('pemrosesan_biomecs')->insertGetId([
            'jenis' => "Instruksi Kerja",
            'jumlah_diuji' => $request->jumlah_diuji,
            'status' => "Belum diproses",
            'no_batch' => $request->no_batch,
            'id_project'=>$request->id_project,
            'id_produk'=>$request->id_project,
            'tanggal_dibuat'=> date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('pemrosesan_ik_biomecs')->insert([
            'id_pemrosesan' => $id,
            'id_ik' => $request->id_ik,
            'created_at' => date("Y-m-d H:i:s"),
            'diproses_oleh' => $userId
        ]);


        $tahapanIK = DB::table("tahapan_ik_biomecs")
        ->select("tahapan_ik_biomecs.id")
        ->where("tahapan_ik_biomecs.id_ik", $request->id_ik)
            ->get();

        for ($i = 0; $i < count($tahapanIK); $i++) {
            DB::table('tahapan_pemrosesan_ik_biomecs')->insert([
                'id_tahapan' => $tahapanIK[$i]->id,
                'id_pemrosesan' => $id,
                'status' => "Belum diproses",
                'note' => "-",
            ]);
        }

        $allDataTahapan = DB::table("tahapan_pemrosesan_ik_biomecs")
        ->join("tahapan_ik_biomecs", "tahapan_pemrosesan_ik_biomecs.id_tahapan", "=", "tahapan_ik_biomecs.id")
        ->select("tahapan_ik_biomecs.uraian", "tahapan_pemrosesan_ik_biomecs.*", "tahapan_ik_biomecs.urutan")
        ->where("tahapan_pemrosesan_ik_biomecs.id_pemrosesan", $id)
        ->get()->sortBy("urutan");

        $alats = DB::table("alat_tahapan_ik_biomecs")
        ->join("alat_biomecs", "alat_tahapan_ik_biomecs.id_alat", "=", "alat_biomecs.id")
        ->select("alat_biomecs.nama", "alat_tahapan_ik_biomecs.id_tahapan")
        ->get();


        for ($i = 0; $i < count($allDataTahapan); $i++) {
            $allDataTahapan[$i] = collect($allDataTahapan[$i])->put("alat_terkait", []);
            $allDataTahapan[$i] = collect($allDataTahapan[$i])->put("hasil_proses", "-");
            $namas = $alats->where("id_tahapan", $allDataTahapan[$i]["id_tahapan"]);
            $allDataTahapan[$i]["alat_terkait"] = $namas->pluck("nama")->toArray();
        }

        $allTahapan = DB::table("tahapan_pemrosesan_ik_biomecs")
        ->select("tahapan_pemrosesan_ik_biomecs.id_tahapan")
        ->where("tahapan_pemrosesan_ik_biomecs.id_pemrosesan", $id)
        ->get();


        $allParameter = DB::table("alat_tahapan_ik_biomecs")
        ->join("param_alat_biomecs", "alat_tahapan_ik_biomecs.id_alat", "=", "param_alat_biomecs.id_alat")
        ->select("alat_tahapan_ik_biomecs.*", "param_alat_biomecs.id")
        ->whereIn("alat_tahapan_ik_biomecs.id_tahapan", $allTahapan->pluck("id_tahapan")->toArray())
            ->get();


        for ($i = 0; $i < count($allParameter); $i++) {

            DB::table('parameter_pemrosesan_biomecs')->insert([
                'id_parameter' => $allParameter[$i]->id,
                'id_tahapan' => $allParameter[$i]->id_tahapan,
                'id_pemrosesan' => $id,
                'value' => null,
            ]);
        }

        $namaIK =  DB::table("instruksi_kerja_biomecs")
        ->select("instruksi_kerja_biomecs.nama")
        ->where("instruksi_kerja_biomecs.id", $request->id_ik)
            ->get()[0]
            ->nama;

        $activityDetail = "Menambah pemrosesan baru surface treatment dengan menggunakan IK [" . $namaIK . "]";
        $activityDetail = substr($activityDetail, 0, 128);

        DB::table('aktivitas_users')->insert([
            'id_project' => DB::table('projects')->select("projects.id")->where("projects.id", 1)->get()[0]->id, //toDo nanti ganti
            'id_user' => $userId,
            'jenis_aktivitas' => "Menambah sesuai Instruksi Kerja",
            'id_aktivitas' => $id, //nyambung ke id pemrosesan
            'detail_aktivitas' => $activityDetail,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan",
            "data" => TahapanPemrosesanIkBiomec::serializePemrosesanIkItem($id)
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TahapanPemrosesanIk  $tahapanPemrosesanIk
     * @return \Illuminate\Http\Response
     */
    public function showIK(Request $request)
    {
        $serializedData = TahapanPemrosesanIkBiomec::serializePemrosesanIkItem($request->id_pemrosesan);
        if ($serializedData) {
            return response()->json([
                "status" => 1,
                "data" => $serializedData,
            ]);
        }
        return response()->json([
            'status' => 0,
            'message' => "Data tidak ditemukan"
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TahapanPemrosesanIk  $tahapanPemrosesanIk
     * @return \Illuminate\Http\Response
     */
    public function editHasilPemrosesan(Request $request)
    {
        //untuk setiap param dalam suatu tahapan, kita looping
        $param = $request->all()["param"];

        //Mengubah note dari tahapan tersebut
        DB::table("tahapan_pemrosesan_ik_biomecs")
            ->where("tahapan_pemrosesan_ik_biomecs.id_pemrosesan", $request->id_pemrosesan)
            ->where("tahapan_pemrosesan_ik_biomecs.id_tahapan", $request->id_tahapan)
            ->update([
                'note' => $request->all()["note"],
            ]);

        $user =  Auth::user();

        $namaIK = DB::table("instruksi_kerja_biomecs")
        ->join("tahapan_ik_biomecs", "instruksi_kerja_biomecs.id", "=", "tahapan_ik_biomecs.id_ik")
        ->select("instruksi_kerja_biomecs.nama")
        ->where("tahapan_ik_biomecs.id", $request->id_tahapan)->get();

        //Mengubah setiap value dari tiap param yang ada
        if (count($param)>0) {
            for ($i=0; $i < count($param); $i++) {
                DB::table("parameter_pemrosesan_biomecs")
                ->select("parameter_pemrosesan_biomecs.*")
                ->where("parameter_pemrosesan_biomecs.id_parameter", $param[$i]["id"])
                ->where("parameter_pemrosesan_biomecs.id_pemrosesan", $request->id_pemrosesan)
                ->where("parameter_pemrosesan_biomecs.id_tahapan", $request->id_tahapan)
                ->update([
                    'value' => $param[$i]["value"],
                    ]);
            }
        }

        //Ini nentuin status dari tiap tahapannya
        DB::table("tahapan_pemrosesan_ik_biomecs")
                    ->where("tahapan_pemrosesan_ik_biomecs.id_pemrosesan", $request->id_pemrosesan)
                    ->where("tahapan_pemrosesan_ik_biomecs.id_tahapan", $request->id_tahapan)
                    ->update([
                        'status' => "Sedang dikerjakan",
                    ]);

        if ($request->simpan_selesai == "true") {
            DB::table("tahapan_pemrosesan_ik_biomecs")
                    ->where("tahapan_pemrosesan_ik_biomecs.id_pemrosesan", $request->id_pemrosesan)
                    ->where("tahapan_pemrosesan_ik_biomecs.id_tahapan", $request->id_tahapan)
                    ->update([
                        'status' => "Sudah Diproses",
                    ]);
        }

        //ini nentuin status dari pemrosesan IK tersebut (di data umum)
        $statuses = DB::table("tahapan_pemrosesan_ik_biomecs")
                    ->select("tahapan_pemrosesan_ik_biomecs.status")
                    ->where("tahapan_pemrosesan_ik_biomecs.id_pemrosesan", $request->id_pemrosesan)
                    ->get()->pluck("status")->toArray();

        if (in_array("Belum diproses",$statuses) || in_array("Sedang dikerjakan",$statuses)) {
            DB::table("pemrosesan_biomecs")
            ->where("pemrosesan_biomecs.id", $request->id_pemrosesan)
            ->update([
                'status' => "Sedang dikerjakan",
            ]);
        }


        //Menyimpan aktivitas user
        DB::table('aktivitas_users')->insert([
            'id_project' => 1, //toDo nanti ganti
            'id_user' => $user->id,
            'jenis_aktivitas' => "Menambah hasil pemrosesan tahapan IK",
            'id_aktivitas' => $request->id_pemrosesan, //nyambung ke id pemrosesan
            'detail_aktivitas' => "Melakukan pemrosesan pada tahapan ke-[".$namaIK."] pada [",
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        $serializedData = TahapanPemrosesanIkBiomec::serializePemrosesanIkItem($request->id_pemrosesan);
        return response()->json([
            "status" => 1,
            "data" => $serializedData,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TahapanPemrosesanIk  $tahapanPemrosesanIk
     * @return \Illuminate\Http\Response
     */
    public function selesaikanPemrosesanIK(Request $request)
    {
        $idPemrosesan = $request->id_pemrosesan;
        $pemrosesanIK = DB::table("pemrosesan_ik_biomecs")
            ->select("pemrosesan_ik_biomecs.*")
            ->where("pemrosesan_ik_biomecs.id_pemrosesan", $idPemrosesan)
            ->get();

        if (!$pemrosesanIK) {
            return response()->json([
                'status' => 0,
                'message' => "Data tidak ditemukan"
            ]);
        }

        DB::table("pemrosesan_biomecs")
            ->where("pemrosesan_biomecs.id", $idPemrosesan)
            ->update([
                'status' => "Selesai",
            ]);

        $file = $request->file('file_hasil');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000, 1001238912) . "." . $ext;

        DB::table("pemrosesan_ik_biomecs")
            ->where("pemrosesan_ik_biomecs.id_pemrosesan", $idPemrosesan)
            ->update([
                'path_file_hasil' => $newName,
                'jumlah_sisa' => $request->jumlah_sisa
            ]);
        $file->move('uploads/file', $newName);

        $serializedData = TahapanPemrosesanIkBiomec::serializePemrosesanIkItem($idPemrosesan);
        return response()->json([
            "status" => 1,
            "data" => $serializedData,
        ]);
    }

    public function cancelPemrosesanIK(Request $request)
    {
        $idPemrosesan = $request->id_pemrosesan;
        $pemrosesanIK = DB::table("pemrosesan_ik_biomecs")
            ->select("pemrosesan_ik_biomecs.*")
            ->where("pemrosesan_ik_biomecs.id_pemrosesan", $idPemrosesan)
            ->get();

        if (!$pemrosesanIK) {
            return response()->json([
                'status' => 0,
                'message' => "Data tidak ditemukan"
            ]);
        }

        DB::table("pemrosesan_biomecs")
            ->where("pemrosesan_biomecs.id", $idPemrosesan)
            ->update([
                'status' => "Dibatalkan",
            ]);
        return response()->json([
            "status" => 1,
            "message" => "Pemrosesan berhasil dibatalkan"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TahapanPemrosesanIk  $tahapanPemrosesanIk
     * @return \Illuminate\Http\Response
     */
    public function destroyIK(Request  $request)
    {

        $pemrosesanIK = DB::table("pemrosesan_ik_biomecs")
        ->select("pemrosesan_ik_biomecs.*")
        ->where("pemrosesan_ik_biomecs.id_pemrosesan", $request->id_pemrosesan)
            ->get();

        if ($pemrosesanIK) {

            $pemrosesan = DB::table("pemrosesan_biomecs")
            ->select("pemrosesan_biomecs.*")
            ->where("pemrosesan_biomecs.id", $request->id_pemrosesan)
                ->get();

            $pemrosesan->delete();
            return response()->json([
                "status" => 1,
                'message' => "Data berhasil dihapus"
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data tidak ditemukan"
            ]);
        }
    }
}
