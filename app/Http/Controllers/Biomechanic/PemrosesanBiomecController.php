<?php

namespace App\Http\Controllers\Biomechanic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PemrosesanAlatBiomec;
use App\TahapanPemrosesanIkBiomec;
use Illuminate\Support\Facades\DB;

function toId($array)
{
    return array_map(
        function ($item) {
            return $item->id;
        },
        $array
    );
};

class PemrosesanBiomecController extends Controller
{
    public function index(Request $request)
    {
        // Determine whether user asks for all types of pemrosesan,
        // "Instruksi Kerja" only, or "Percobaan" only

        $jenis = $request->jenis;
        if ($jenis !== "Instruksi Kerja" && $jenis !== "Percobaan") {
            $jenis = null;
        }

        if ($jenis) {
            $query = DB::table("pemrosesan_biomecs")
                ->where("pemrosesan_biomecs.jenis", $jenis)->where('id_project',$request->id_project);
        } else {
            $query = DB::table("pemrosesan_biomecs")->where('id_project',$request->id_project);
        }

        // Query initial pemrosesan items using paginate

        $pagination = $query->orderBy("id")->paginate(10);
        $pemrosesanItems = $pagination->items();

        $idToArrIndex = [];
        foreach ($pemrosesanItems as $index => $item) {
            $idToArrIndex[$item->id] = $index;
        }
        unset($item);

        // Select joined IK and pemrosesan data with ID appearing in
        // query from pagination.

        $pemrosesanIkIds = toId(array_filter($pemrosesanItems, function ($item) {
            return $item->jenis === "Instruksi Kerja";
        }));

        $pemrosesanIks = DB::table("pemrosesan_ik_biomecs")
            ->whereIn("id_pemrosesan", $pemrosesanIkIds)
            ->join("instruksi_kerja_biomecs", "pemrosesan_ik_biomecs.id_ik", "=", "instruksi_kerja_biomecs.id")
            ->select("pemrosesan_ik_biomecs.id_pemrosesan", "instruksi_kerja_biomecs.nama")
            ->get()
            ->toArray();

        foreach ($pemrosesanIks as $pemrosesanIk) {
            $idPemrosesan = $pemrosesanIk->id_pemrosesan;
            $index = $idToArrIndex[$idPemrosesan];
            $pemrosesanItems[$index]->nama_ik = $pemrosesanIk->nama;
        }

        // Select joined alat and pemrosesan percobaan data with ID appearing in
        // query from pagination.

        $pemrosesanPercobaanIds = toId(array_filter($pemrosesanItems, function ($item) {
            return $item->jenis === "Percobaan";
        }));

        $pemrosesanAlats = DB::table("percobaan_alat_biomecs")
            ->whereIn("id_pemrosesan", $pemrosesanPercobaanIds)
            ->join("alat_biomecs", "percobaan_alat_biomecs.id_alat", "=", "alat_biomecs.id")
            ->select("alat_biomecs.nama", "percobaan_alat_biomecs.id_pemrosesan")
            ->get();

        $groupedPemrosesan = array();
        foreach ($pemrosesanAlats as $item) {
            $groupedPemrosesan[$item->id_pemrosesan][] = $item->nama;
        }
        unset($item);

        foreach ($groupedPemrosesan as $idPemrosesan => $item) {
            $index = $idToArrIndex[$idPemrosesan];
            $pemrosesanItems[$index]->alat = $item;
        }

        // Return response

        return response()->json([
            "status" => 1,
            "data" => [
                "pemrosesan" => $pemrosesanItems,
            ],
            "pagination" => [
                "current_page" => $pagination->currentPage(),
                "total_item" => $pagination->total(),
                "items_per_page" => $pagination->perPage(),
            ],
        ]);
    }

    /**
     * Get pemrosesan item given id_pemrosesan. If item is an IK pemrosesan,
     * returns pemrosesan with IK data. Othwerwise, returns percobaan item.
     */

    public function show(Request $request)
    {
        $items = DB::table("pemrosesan_biomecs")
            ->select("pemrosesan_biomecs.jenis")
            ->where("pemrosesan_biomecs.id", $request->id_pemrosesan)
            ->get();

        if (count($items) < 1) {
            return response()->json([
                'status' => 0,
                'message' => "Data tidak ditemukan"
            ]);
        }

        $jenisPemrosesan = $items->pluck("jenis")[0];

        if ($jenisPemrosesan == "Instruksi Kerja") {
            $pemrosesanIk = TahapanPemrosesanIkBiomec::serializePemrosesanIkItem($request->id_pemrosesan);
            return response()->json([
                "status" => 1,
                "data" => $pemrosesanIk,
            ]);
        } else {
            $serializedPercobaan = PemrosesanAlatBiomec::serializePercobaanItem($request->id_pemrosesan);
            return response()->json([
                "status" => 1,
                "data" => $serializedPercobaan
            ]);
        }
    }
}
