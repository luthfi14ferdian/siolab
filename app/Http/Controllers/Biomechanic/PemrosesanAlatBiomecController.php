<?php

namespace App\Http\Controllers\Biomechanic;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\AlatBiomec;
use App\PemrosesanAlatBiomec;
use App\PercobaanAlatBiomec;
use App\ParameterPemrosesanBiomecs;

class PemrosesanAlatBiomecController extends Controller
{
    public function create()
    {
        $alats = DB::table("alat_biomecs")
        ->select("alat_biomecs.*")
        ->get()->toArray();

        $paramAlat = DB::table("param_alat_biomecs")
        ->join("alat_biomecs", "param_alat_biomecs.id_alat", "=", "alat_biomecs.id")
        ->select("param_alat_biomecs.*")
        ->get();

        $alatTemp = [];
        foreach ($alats as $key => $value)  {
            $thisAlat = [];
            $thisAlat = collect($thisAlat)->put("id", $value->id);
            $thisAlat->put("kode", $value->kode);
            $thisAlat->put("nama", $value->nama);
            $thisAlat->put("tipe", $value->tipe);
            $thisAlat->put("fungsi", $value->fungsi);


            $param = DB::table("param_alat_biomecs")
            ->select("param_alat_biomecs.id","param_alat_biomecs.parameter","param_alat_biomecs.metric")
            ->where("param_alat_biomecs.id_alat", $value->id)
            ->get();

            $thisAlat->put("parameters", $param->toArray());

            array_push($alatTemp, $thisAlat);

        }


        return response()->json([
            "status" => 1,
            "alats" => $alatTemp
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jenis' => 'required|max:128',
            'jumlah_diuji' => 'required',
            'no_batch' => 'required|max:128',
            'tujuan' => 'required|max:128',
            'judul' => 'required|max:128',
        ]);

        $data = $request->all();

        $id = DB::table('pemrosesan_biomecs')->insertGetId([
            'jenis' => "Percobaan",
            'jumlah_diuji' => $data["jumlah_diuji"],
            'status' => "Sedang diproses",
            'no_batch' => $data["no_batch"],
            'created_at' => date("Y-m-d H:i:s"),
            'tanggal_dibuat' => $data['tanggal_dibuat'],
            'id_project' => $data['id_project'],
            'id_produk' => $data['id_produk'],
        ]);

        DB::table('pemrosesan_alat_biomecs')->insert([
            'id_pemrosesan' => $id,
            "judul" => $data["judul"],
            "tujuan" => $data["tujuan"],
            "diproses_oleh" => Auth::user()->id,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        $alats = $data["alats"];
        $user =  Auth::user();
        for ($i = 0; $i < count($alats); $i++) {
            DB::table('percobaan_alat_biomecs')->insert([
                'id_pemrosesan' => $id,
                "id_alat" => $alats[$i]["id"],
                "note" => $alats[$i]["note"],
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            $alatName = AlatBiomec::find($alats[$i]["id"])->nama;

            DB::table('aktivitas_users')->insert([
                'id_project' => DB::table('projects')
                    ->select("projects.id")
                    ->where("projects.id", 1)
                    ->get()[0]
                    ->id, //toDo nanti ganti
                'id_user' => $user->id,
                'jenis_aktivitas' => "Menambah Percobaan",
                'id_aktivitas' => $alats[$i]["id"],
                'detail_aktivitas' => "Melakukan percobaan dengan alat  [" . $alatName . "]",
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            foreach ($alats[$i]["parameters"] as $key => $param) {
                DB::table('parameter_pemrosesan_biomecs')->insert([
                    'id_parameter' => $param["id"],
                    "id_pemrosesan" => $id,
                    "value" => $param["value"],
                ]);
            }
        }

        if ($data["simpan_selesai"] == "true") {

            DB::table("pemrosesan_alat_biomecs")
                ->where("pemrosesan_alat_biomecs.id_pemrosesan", $id)
                ->update([
                    "jumlah_sisa" => $data["jumlah_sisa"],
                    'tanggal_selesai' => date("Y-m-d H:i:s"),
                ]);
            DB::table("pemrosesan_biomecs")
                ->where("pemrosesan_biomecs.id", $id)
                ->update([
                    "status" => "Selesai",
                ]);

            $datas= PemrosesanAlatBiomec::serializePercobaanItem($id);
            DB::table("pemrosesan_biomecs")
            ->where("pemrosesan_biomecs.id", $id)
            ->update([
                "data" =>json_encode($datas),
            ]);
        }


        $dataUmum = DB::table("pemrosesan_alat_biomecs")
            ->join("pemrosesan_biomecs", "pemrosesan_alat_biomecs.id_pemrosesan", "=", "pemrosesan_biomecs.id")
            ->join("users", "pemrosesan_alat_biomecs.diproses_oleh", "=", "users.id")
            ->select(
                "pemrosesan_biomecs.*",
                "pemrosesan_alat_biomecs.tujuan",
                "pemrosesan_alat_biomecs.judul",
                "pemrosesan_alat_biomecs.created_at",
                "users.first_name",
                "users.last_name",
                "pemrosesan_alat_biomecs.jumlah_sisa",
                "pemrosesan_alat_biomecs.tanggal_selesai"
            )
            ->where("pemrosesan_biomecs.id", $id)
            ->get();

        $alats = DB::table("percobaan_alat_biomecs")
            ->join("alat_biomecs", "percobaan_alat_biomecs.id_alat", "=", "alat_biomecs.id")
            ->join("pemrosesan_biomecs", "percobaan_alat_biomecs.id_pemrosesan", "=", "pemrosesan_biomecs.id")
            ->select("alat_biomecs.*", "percobaan_alat_biomecs.note")
            ->where("pemrosesan_biomecs.id", $id)
            ->get();

        $alatTemp = [];
        foreach ($alats as $key => $value) {
            $thisAlat = [];
            $thisAlat = collect($thisAlat)->put("id", $value->id);
            $thisAlat->put("kode", $value->kode);
            $thisAlat->put("nama", $value->nama);
            $thisAlat->put("tipe", $value->tipe);
            $thisAlat->put("fungsi", $value->fungsi);

            $param = DB::table("parameter_pemrosesan_biomecs")
                ->join("param_alat_biomecs", "parameter_pemrosesan_biomecs.id_parameter", "=", "param_alat_biomecs.id")
                ->select(
                    "param_alat_biomecs.id",
                    "param_alat_biomecs.parameter",
                    "param_alat_biomecs.metric",
                    "parameter_pemrosesan_biomecs.value"
                )
                ->where("param_alat_biomecs.id_alat", $value->id)
                ->get();

            $thisAlat->put("parameters", $param->toArray());

            array_push($alatTemp, $thisAlat);
        }

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan",
            "data" => [
                "data_umum" => $dataUmum,
                "alats" => $alatTemp,
            ]

        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(request $request)
    {
        $serializedPercobaan = PemrosesanAlatBiomec::serializePercobaanItem($request->id_pemrosesan);
        if ($serializedPercobaan) {
            return response()->json([
                "status" => 1,
                "data" => $serializedPercobaan
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message'=> "Data tidak ditemukan"
            ], 200);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showEditPercobaan(Request $request)
    {
        $alats = DB::table("percobaan_alat_biomecs")
        ->join("alat_biomecs", "percobaan_alat_biomecs.id_alat", "=", "alat_biomecs.id")
        ->join("pemrosesan_biomecs", "percobaan_alat_biomecs.id_pemrosesan", "=", "pemrosesan_biomecs.id")
        ->select("alat_biomecs.*", "percobaan_alat_biomecs.note")
        ->where("pemrosesan_biomecs.id", $request->id_pemrosesan )
        ->get();

        $alatTemp = [];
        foreach ($alats as $key => $value)  {
            $thisAlat = [];
            $thisAlat = collect($thisAlat)->put("id", $value->id);
            $thisAlat->put("kode", $value->kode);
            $thisAlat->put("nama", $value->nama);
            $thisAlat->put("tipe", $value->tipe);
            $thisAlat->put("fungsi", $value->fungsi);


            $param = DB::table("parameter_pemrosesan_biomecs")
            ->join("param_alat_biomecs", "parameter_pemrosesan_biomecs.id_parameter", "=", "param_alat_biomecs.id")
            ->select("param_alat_biomecs.id", "param_alat_biomecs.parameter", "param_alat_biomecs.metric","parameter_pemrosesan_biomecs.value")
            ->where("param_alat_biomecs.id_alat", $value->id )
            ->get();

            $thisAlat->put("parameters", $param->toArray());

            array_push($alatTemp, $thisAlat);

        }

        return response()->json([
            "status" => 1,
            "data" => [
                "alats" => $alatTemp,
            ]

        ], 200);
}

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showEditPemrosesan(Request $request)
    {
        $pemrosesan = DB::table("pemrosesan_biomecs")
        ->join("pemrosesan_alat_biomecs", "pemrosesan_biomecs.id", "=", "pemrosesan_alat_biomecs.id_pemrosesan")
        ->select("pemrosesan_biomecs.*", "pemrosesan_alat_biomecs.*")
        ->where("pemrosesan_biomecs.id", $request->id_pemrosesan )
        ->get();

        return response()->json([
            "status" => 1,
            "data" => [
                "pemrosesan" => $pemrosesan,
            ]

        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updatePercobaan(Request $request)
    {
        $validatedData = $request->validate([
            'tujuan' => 'required|max:128',
            'jumlah_diuji' => 'required',
            'simpan_selesai' => 'required',
            'no_batch' => 'required|max:128',
        ]);

        DB::table("pemrosesan_biomecs")
        ->where("id", $request->id_pemrosesan)
        ->update([
            "jumlah_diuji" => $request->jumlah_diuji,
            "no_batch" => $request->no_batch,
            "updated_at" => date("Y-m-d H:i:s")
        ]);

        DB::table("pemrosesan_alat_biomecs")
        ->where("id_pemrosesan", $request->id_pemrosesan)
        ->update([
            "tujuan"=>$request->tujuan,
            "judul"=>$request->judul,
            "updated_at" => date("Y-m-d H:i:s")
        ]);

        $percobaan_alat = PercobaanAlatBiomec::where('id_pemrosesan', $request->id_pemrosesan)->delete();

        $alats =$request->all()["alats"];
        for ($i=0; $i < count($alats); $i++) {
            $percobaan_alats = new PercobaanAlatBiomec();
            $percobaan_alats->id_pemrosesan = $request->id_pemrosesan;
            $percobaan_alats->id_alat = $alats[$i]["id"];
            $percobaan_alats->note = $alats[$i]["note"];
            $percobaan_alats->save();


            foreach ($alats[$i]["parameters"] as $param ) {
                $parameter_pemrosesan = ParameterPemrosesanBiomecs::where('id_pemrosesan',$request->id_pemrosesan)->where('id_parameter',$param["id"])->delete();
                $new_parameter= new ParameterPemrosesanBiomecs();
                $new_parameter->id_parameter=  $param["id"];
                $new_parameter->id_pemrosesan=  $request->id_pemrosesan;
                $new_parameter->value=  $param["value"];
                $new_parameter->save();
            }
        }

        if ($request->all()["simpan_selesai"] == "true") {
            DB::table("pemrosesan_alat_biomecs")
            ->where("pemrosesan_alat_biomecs.id_pemrosesan", $request->id_pemrosesan)
            ->update([
                "jumlah_sisa" => $request->all()["jumlah_sisa"],
                'tanggal_selesai' => date("Y-m-d H:i:s"),
            ]);
            DB::table("pemrosesan_biomecs")
            ->where("pemrosesan_biomecs.id", $request->id_pemrosesan)
            ->update([
                "status" => "Selesai",
            ]);

            $datas= PemrosesanAlatBiomec::serializePercobaanItem($request->id_pemrosesan);
            DB::table("pemrosesan_biomecs")
            ->where("pemrosesan_biomecs.id", $request->id_pemrosesan)
            ->update([
                "data" =>json_encode($datas),
            ]);
        }

        return response()->json([
            "status" => 1,
            "data" => [
                "pemrosesan" => PemrosesanAlatBiomec::serializePercobaanItem($request->id_pemrosesan),
            ]
        ], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $pemrosesanAlat = DB::table("pemrosesan_alat_biomecs")
        ->select("pemrosesan_alat_biomecs.*")
        ->where("pemrosesan_alat_biomecs.id_pemrosesan", $request->id_pemrosesan)
            ->get();

        if ($pemrosesanAlat) {

            DB::table("pemrosesan_biomecs")
            ->select("pemrosesan_biomecs.*")
            ->where("pemrosesan_biomecs.id", $request->id_pemrosesan)
                ->delete();


            return response()->json([
                "status" => 1,
                'message' => "Data berhasil dihapus"
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data tidak ditemukan"
            ]);
        }
    }
}
