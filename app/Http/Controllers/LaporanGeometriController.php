<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LaporanGeometri;
use App\PercobaanGeometri;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Produk;
use App\LaporanBatch;
use Illuminate\Support\Facades\Auth;

class LaporanGeometriController extends Controller
{
    public function index(Request $request){
        $geometri = LaporanGeometri::where('id_laporan', $request->id_laporan)->paginate(5);

        $items = $geometri->items();

        for ($i = 0; $i < count($items); $i++) {
            $percobaan = PercobaanGeometri::where('id_geometri_item',$items[$i]->id)->get();
            $items[$i]->percobaan_items=$percobaan;
        }

        $total_item =$geometri->total();
        $current_page = $geometri->currentPage();
        $total_item_per_page = $geometri->perPage();


        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $items
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    public function addLaporanGeometri(Request $request){
        $data = $request->all();
        $geometri = new LaporanGeometri();
        $geometri->nama_sampel = $data['nama_sampel'];
        $geometri->rata_rata = $data['rata_rata'];
        $geometri->dimensi_desain = $data['dimensi_desain'];
        $geometri->gap = $data['gap'];
        $geometri->penyimpangan = $data['penyimpangan'];
        $geometri->judgement = $data['judgement'];
        $geometri->id_laporan=$data['id_laporan'];
        $geometri->save();

        $this->checkJudgement($request->id_laporan);

        for ($i = 0; $i < count($data['percobaan_items']); $i++) {
            $percobaanGeometri = new PercobaanGeometri();
            $percobaanGeometri->id_geometri_item = $geometri->id;
            $percobaanGeometri->percobaan = $data['percobaan_items'][$i]['percobaan'];
            $percobaanGeometri->ukuran = $data['percobaan_items'][$i]['ukuran'];
            $percobaanGeometri->save();
        }


        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan",
        ], 200);
    }

    public function editLaporanGeometri(Request $request){
        $geometri = LaporanGeometri::find($request->id);
        $percobaan = PercobaanGeometri::where('id_geometri_item',$request->id)->get();
        $id_laporan= $geometri->id_laporan;

        for ($i = 0; $i < count($percobaan); $i++) {
            $percobaan[$i]->delete();
        }

        $data = $request->all();
        $geometri->nama_sampel = $data['nama_sampel'];
        $geometri->rata_rata = $data['rata_rata'];
        $geometri->dimensi_desain = $data['dimensi_desain'];
        $geometri->gap = $data['gap'];
        $geometri->penyimpangan = $data['penyimpangan'];
        $geometri->judgement = $data['judgement'];
        $geometri->save();

        $this->check2Judgement($id_laporan);

        for ($i = 0; $i < count($data['percobaan_items']); $i++) {
            $percobaanGeometri = new PercobaanGeometri();
            $percobaanGeometri->id_geometri_item = $geometri->id;
            $percobaanGeometri->percobaan = $data['percobaan_items'][$i]['percobaan'];
            $percobaanGeometri->ukuran = $data['percobaan_items'][$i]['ukuran'];
            $percobaanGeometri->save();
        }

        return response()->json([
            "status" =>1,
            "message" => "Data berhasil diubah",
        ], 200);
    }

    public function deleteLaporanGeometri(Request $request){
        $geometri = LaporanGeometri::find($request->id);
        $id_laporan= $geometri->id_laporan;
        $geometri->delete();

        $this->check2Judgement($id_laporan);

        return response()->json([
            "status" =>1,
            "message" => "Data berhasil dihapus"
        ], 200);
    }

    public function checkJudgement($idLaporan){
        $geometri_all = LaporanGeometri::where("id_laporan",$idLaporan)->get();
        $laporan = LaporanBatch::find($idLaporan);

        $state="";
        for ($i = 0; $i < count($geometri_all); $i++) {
            if($geometri_all[$i]->judgement == "NG"){
                $state="NG";
                $laporan->judgement="NG";
                $laporan->save();
            }
        }

        if($state!="NG"){
            $laporan->judgement="OK";
            $laporan->save();
        }
    }

    public function check2Judgement($idLaporan){
        $geometri_all = LaporanGeometri::where("id_laporan",$idLaporan)->get();
        $laporan = LaporanBatch::find($idLaporan);

        $state="";

        for ($i = 0; $i < count($geometri_all); $i++) {
            if($geometri_all[$i]->judgement == "NG"){
                $state="NG";
                $laporan->judgement="NG";
                $laporan->save();
            }
        }

        if($state!="NG"){
            $laporan->judgement="OK";
            $laporan->save();
        }
    }

}

