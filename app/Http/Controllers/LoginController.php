<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{

    public function login(Request $request){
        $email=$request->email;
        $password=$request->password;

        if (strpos($email, '@')){
            $user= Auth::attempt(['email' => $email, 'password' => $password]);
        }

        else{
            $user= Auth::attempt(['username' => $email, 'password' => $password]);
        }

        if($user){
            $user= Auth::user();
            $user->makeVisible('email');

            if($user->is_approve == False){
                return response()->json([
                    'message'=>'Akun anda belum disetujui!'
                ],401);
            }
            $user->api_token =  $user->createToken('MyApp')->accessToken;
            $user->save();

            return response()->json([
                'status'=> 1,
                'message'=>'Anda berhasil login',
                'data'=> $user,
                'access_token'=> $user->api_token,
            ], 200);
        }

        else{
            return response()->json([
                'message'=>'email/username atau password salah'
            ],401);
        }
    }

    public function logout(Request $request){
        $request->user()->token()->revoke();
        return response()->json([
            'message'=>'succesfull logout',
        ]);
    }

}
