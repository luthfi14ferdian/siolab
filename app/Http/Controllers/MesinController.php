<?php

namespace App\Http\Controllers;

use App\mesin;
use Illuminate\Http\Request;

class MesinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\mesin  $mesin
     * @return \Illuminate\Http\Response
     */
    public function show(mesin $mesin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\mesin  $mesin
     * @return \Illuminate\Http\Response
     */
    public function edit(mesin $mesin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\mesin  $mesin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, mesin $mesin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\mesin  $mesin
     * @return \Illuminate\Http\Response
     */
    public function destroy(mesin $mesin)
    {
        //
    }
}
