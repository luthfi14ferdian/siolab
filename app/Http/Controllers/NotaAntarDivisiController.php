<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotaAntarDivisi;
use App\Exports\NotaAntarDivisiExport;
use App\User;
use App\Model\Desain\Fit3D;
use App\Model\Desain\Drawing;
use App\Model\Desain\SpesifikasiProduk;
use App\KodeAktivitas;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotaAntarDivisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if($request->id_item == NULL){
            if($request->tujuan_nota == 'divisi-lain'){
                $all_nota = NotaAntarDivisi::where('divisi_yang_diajukan', "Manufaktur")
                ->where('nama_barang', $request->id_proyek)
                ->join('projects', 'nota_antar_divisis.nama_barang', '=', 'projects.id')
                ->select('nota_antar_divisis.id', 'nota_antar_divisis.nomor_batch', 'nota_antar_divisis.divisi_yang_mengajukan', 'nota_antar_divisis.tanggal',
                'projects.nama_project as nama_barang', 'nota_antar_divisis.status')
                ->get();
            }
            else{
                $all_nota = DB::table('nota_antar_divisis')
                ->where('divisi_yang_mengajukan', "Manufaktur")
                ->where('nama_barang', $request->id_project)
                ->join('projects', 'nota_antar_divisis.nama_barang', '=', 'projects.id')
                ->select('nota_antar_divisis.id', 'nota_antar_divisis.nomor_batch', 'nota_antar_divisis.divisi_yang_mengajukan', 'nota_antar_divisis.tanggal',
                'projects.nama_project as nama_barang', 'nota_antar_divisis.status')
                ->get();
            }
            $notaAntarDivisi = array_chunk($all_nota->toArray(), 10);
            if (count($notaAntarDivisi) <= (($request->start)-1) ) {
                $notaAntarDivisi = [];
            }
            else {
                $notaAntarDivisi = $notaAntarDivisi[($request->start)-1];
            }

            $total_item = count($all_nota);
            $current_page = (int)$request->start;
            $total_item_per_page = count($notaAntarDivisi);

            return response()->json([
                "status" => 1,
                "data" => [
                    "item" => $notaAntarDivisi
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_pr_page" => $total_item_per_page,
                ],
            ]);
        }
        else{
            return response()->json($this->getDetailNotaAntarDivisi($request));
        }
    }

    public function getDetailNotaAntarDivisi(Request $request){
        $detail_nota = DB::table('nota_antar_divisis')
            ->where('nota_antar_divisis.id', $request->id_item)
            ->where('nama_barang', $request->id_proyek)
            ->get();


        $detail_nota[0]->disetujui_oleh = User::find($detail_nota[0]->disetujui_oleh
        , ['id', 'username', 'last_name', 'first_name']);
        $detail_nota[0]->diketahui_oleh = User::find($detail_nota[0]->diketahui_oleh
        , ['id', 'username', 'last_name', 'first_name']);
        $detail_nota[0]->dibuat_oleh = User::find($detail_nota[0]->dibuat_oleh
        , ['id', 'username', 'last_name', 'first_name']);
        $detail_nota[0]->dokumen_3d_fit = Fit3D::find($detail_nota[0]->dokumen_3d_fit);
        $detail_nota[0]->dokumen_drawing = Drawing::find($detail_nota[0]->dokumen_drawing);
        $detail_nota[0]->dokumen_spec_desain = SpesifikasiProduk::find($detail_nota[0]->dokumen_spec_desain);

        return $detail_nota;
    }

    private function invalidNotaAntarDivisi(Request $request){
        return (
            $request->nomor_WO == NULL ||
            $request->nomor_NAD == NULL ||
            $request->nomor_batch == NULL ||
            $request->tanggal == NULL ||
            $request->divisi_yang_mengajukan == NULL ||
            $request->divisi_yang_diajukan == NULL ||
            $request->nama_barang == NULL ||
            $request->satuan == NULL ||
            $request->kuantitas == NULL ||
            $request->keterangan == NULL ||
            $request->dibuat_oleh == NULL
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeNotaAntarDivisi(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        if(!$this->invalidNotaAntarDivisi($request)){
            $notaAntarDivisi = DB::table('nota_antar_divisis')->insertGetId($request->all() +
            ['created_at' => $mytimenew->toDateTimeString(), 'updated_at' => $mytimenew->toDateTimeString(), 'status'=> 'baru']);

            $SerahTerima = DB::table('serah_terimas')->insertGetId([
                'id_NAD' => $notaAntarDivisi,
                'tanggal' => $request['tanggal'],
                'item' => $request['nama_barang'],
                'no_batch' => $request['nomor_batch'],
                'jumlah' => $request['kuantitas'],
                'tujuan' => $request['divisi_yang_diajukan'],
                'keterangan' => $request['keterangan'],
                'status' => 'Belum Diterima',
                'yang_menyerahkan' => $request['dibuat_oleh'],
                'created_at' => $mytimenew->toDateTimeString(),
                'updated_at' => $mytimenew->toDateTimeString(),
            ]);

            $user =  Auth::user();
            DB::table('aktivitas_users')->insert([
                'id_project' => DB::table('projects')->select("projects.id")->where("projects.id",1)->get()[0]->id, //toDo nanti ganti
                'id_user' => $user->id,
                'jenis_aktivitas' => "Menambah NAD",
                'id_aktivitas' => $notaAntarDivisi->id,
                'detail_aktivitas' => "Menambah Nota Antar Divisi Baru untuk ke divisi ". $request['divisi'],
                'created_at' => date("Y-m-d H:i:s"),
            ]);


            if($notaAntarDivisi != NULL || $SerahTerima != NULL){
                return response()
                    ->json([
                        DB::table('nota_antar_divisis')
                        ->where('nota_antar_divisis.id', $notaAntarDivisi)
                        ->get(),
                        DB::table('serah_terimas')
                        ->where('serah_terimas.id', $SerahTerima)
                        ->get()]);
            }
        }
        else{
            return response()
                    ->json(['status' => 'FAILED','reason'=>'Empty request']);
        }

    }

    private function createAktivitasUser($aktivitas, $kode, $idUser, $idAktivitas){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        DB::table('aktivitas_user')->insert([
            'jenis_aktivitas' => $aktivitas,
            'id_user' => $idUser,
            'kode_aktivitas' => KodeAktivitas::where('jenis_aktivitas', $kode)->first()->id,
            'id_aktivitas' => $idAktivitas,
            'created_at' => $mytimenew->toDateTimeString(),
            'updated_at' => $mytimenew->toDateTimeString(),
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NotaAntarDivisi  $notaAntarDivisi
     * @return \Illuminate\Http\Response
     */
    public function edit(NotaAntarDivisi $notaAntarDivisi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NotaAntarDivisi  $notaAntarDivisi
     * @return \Illuminate\Http\Response
     */
    public function updateNotaAntarDivisi(Request $request){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');
        $notaAntarDivisi = DB::table('nota_antar_divisis')
            ->where('nota_antar_divisis.id', $request->idNota)
            ->get();

        if($notaAntarDivisi[0]->status == 'baru'){
            DB::table('nota_antar_divisis')
            ->where('nota_antar_divisis.id', $request->idNota)
            ->update($request->all() + ['updated_at' => $mytimenew->toDateTimeString()]);

            DB::table('serah_terimas')
            ->where('serah_terimas.id_NAD', $request->idNota)
            ->update($request->all() + ['updated_at' => $mytimenew->toDateTimeString()]);

            $user =  Auth::user();
            DB::table('aktivitas_users')->insert([
                'id_project' => DB::table('projects')->select("projects.id")->where("projects.id",1)->get()[0]->id, //toDo nanti ganti
                'id_user' => $user->id,
                'jenis_aktivitas' => "Mengubah NAD",
                'id_aktivitas' => $notaAntarDivisi->id,
                'detail_aktivitas' => "Mengubah Nota Antar Divisi Baru untuk ke divisi ". $request['divisi'],
                'created_at' => date("Y-m-d H:i:s"),
            ]);


            return response()->json(['ID'=> $notaAntarDivisi[0]->id, 'Success'=> 'Sukses mengubah'], 200);
        }
        else{
            return response()->json(['ID'=> $notaAntarDivisi[0]->id, 'ERROR'=> 'Gagal mengubah, Data yang Anda Cari Tidak Ada'], 200);
        }
    }

    public function setujuiNotaAntarDivisi(Request $request){
        $notaAntarDivisi = DB::table('nota_antar_divisis')
            ->where('nota_antar_divisis.id', $request->idNota)
            ->get();
        if ($notaAntarDivisi != NULL){
            if(strtolower($notaAntarDivisi[0]->status) == 'baru'){
                $this->ubahStatusNotaAntarDivisi($request->idNota, 'sedang_diproses',
                $request->session()->get('id'), 'diketahui_oleh');

                return response()->json(['ID'=> $notaAntarDivisi[0]->id, 'status'=> 'Nota antar divisi berhasil disetujui'], 200);
            }
            else if(strtolower($notaAntarDivisi[0]->status) == 'sedang_diproses'){
                    $this->ubahStatusNotaAntarDivisi($request->idNota, 'selesai',
                    $request->session()->get('id'), 'disetujui_oleh');

                return response()->json(['ID'=> $notaAntarDivisi[0]->id, 'status'=> 'Nota antar divisi berhasil disetujui'], 200);
            }
            else{
                return response()->json(['ID'=> $notaAntarDivisi[0]->id, 'status'=> 'Gagal mengubah, authorization violation or data not found'], 200);
            }
        }
        else{
            return response()->json(['ID'=> $notaAntarDivisi[0]->id, 'status'=> 'Gagal mengubah, data not found'], 200);
        }
    }

    private function ubahStatusNotaAntarDivisi($idNota, $newStatus, $idUser, $test){
        $mytime = Carbon::now();
        $mytimenew = $mytime->setTimezone('Asia/Jakarta');

        $nota = NotaAntarDivisi::updateOrCreate(
            ['nota_antar_divisis.id' => $idNota],
            ['status'=>$newStatus,$test=>$idUser,'updated_at' => $mytimenew->toDateTimeString()]
        );

        return $nota;
    }

    public function export_excel(Request $request)
	{
		return Excel::download(new NotaAntarDivisiExport($request->id_proyek), 'nota-antar-divisi.xlsx');
    }

    public function export_pdf(Request $request)
    {
    	$nota_antar_divisi = DB::table('nota_antar_divisis')
        ->where('nota_antar_divisis.id', $request->id_item)
        ->where('nama_barang', $request->id_proyek)
        ->get();

        $project = DB::table('projects')
        ->where('id', $request->id_proyek)
        ->get();

        $disetujui_oleh = User::find($nota_antar_divisi[0]->disetujui_oleh
        , ['id', 'username', 'last_name', 'first_name']);
        $diketahui_oleh = User::find($nota_antar_divisi[0]->diketahui_oleh
        , ['id', 'username', 'last_name', 'first_name']);
        $dibuat_oleh = User::find($nota_antar_divisi[0]->dibuat_oleh
        , ['id', 'username', 'last_name', 'first_name']);
        $dokumen_3d_fit = Fit3D::find($nota_antar_divisi[0]->dokumen_3d_fit);
        $dokumen_drawing = Drawing::find($nota_antar_divisi[0]->dokumen_drawing);
        $dokumen_spec_desain = SpesifikasiProduk::find($nota_antar_divisi[0]->dokumen_spec_desain);

        $pdf = PDF::loadview('nota_antar_divisi_pdf',['nota_antar_divisi'=>$nota_antar_divisi,
        'project' => $project,
        'disetujui_oleh' => $disetujui_oleh,
        'diketahui_oleh' => $diketahui_oleh,
        'dibuat_oleh' => $dibuat_oleh,
        'dokumen_3d_fit' => $dokumen_3d_fit,
        'dokumen_drawing' => $dokumen_drawing,
        'dokumen_spec_desain' => $dokumen_spec_desain]);

    	return $pdf->download('nota-antar-divisi.pdf');
    }
}
