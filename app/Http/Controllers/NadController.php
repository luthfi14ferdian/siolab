<?php

namespace App\Http\Controllers;
use App\NotaAntarDivisi;
use App\project;
use App\DokumenNAD;
use App\Produk;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class NadController extends Controller
{
    public function index(Request $request){
        if ($request->tujuan_nota == 'dari-divisi-lain'){
            $nad = NotaAntarDivisi::where('divisi_yang_diajukan',$request->id_divisi)
            ->where('id_project',$request->id_project)->paginate(10);

            $items = $nad->items();
            $total_item = $nad->total();
            $current_page = $nad->currentPage();
            $total_item_per_page = $nad->perPage();

            return response()->json([
                "status" => 1,
                "data" => [
                    "item" =>$items
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_per_page" => $total_item_per_page,
                ],
            ]);
        }

        else{
            $nad = NotaAntarDivisi::where('divisi_yang_mengajukan',$request->id_divisi)
            ->where('id_project',$request->id_project)->paginate(10);

            $items = $nad->items();
            $total_item = $nad->total();
            $current_page = $nad->currentPage();
            $total_item_per_page = $nad->perPage();

            return response()->json([
                "status" => 1,
                "data" => [
                    "item" => $items
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_per_page" => $total_item_per_page,
                ],
            ]);
        }
    }

    public function getNad(Request $request){
        $nad = NotaAntarDivisi::with(['produk'])->find($request->id_nad);

        if($nad ==null){
            return response()->json([
                'status'=>0,
                'message' => 'Data Nota Antar Divisi tidak ditemukan',
            ],404);
        }

        $user_dibuat_oleh= User::find($nad->dibuat_oleh);
        $user_diketahui_oleh=User::find($nad->diketahui_oleh);
        $user_disetujui_oleh=User::find($nad->disetujui_oleh);

        $nad->makeHidden(['dibuat_oleh','diketahui_oleh','disetujui_oleh']);
        if($nad){
            return response()->json([
                "status" => 1,
                "data" =>[
                    'nad' => $nad,
                    'dibuat_oleh'=> $user_dibuat_oleh,
                    'diketahui_oleh'=> $user_diketahui_oleh,
                    'disetujui_oleh'=> $user_disetujui_oleh,
                ]
            ],200 );
        }

        else{
            return response()->json([
                'status'=>0,
                'message' => 'Data tidak ditemukan',
            ],404);
        }
    }

    public function createNad(Request $request){
        $nad = new NotaAntarDivisi();
        $project= project::find($request->id_project);
        $nad->id_project=$request->id_project;
        $nad->nomor_WO= $request->nomor_WO;
        $nad->nomor_NAD= $request->nomor_NAD;
        $nad->nomor_batch = $request->nomor_batch;
        $nad->status = 'Baru';
        $nad->tanggal = date("Y-m-d H:i:s");
        $nad->divisi_yang_mengajukan = Auth::user()->divisi_id;
        $nad->divisi_yang_diajukan = $request->divisi_yang_diajukan;
        $nad->id_produk = $request->id_produk;
        $nad->nama_barang = $request->nama_barang;
        $nad->satuan = $request->satuan;
        $nad->kuantitas = $request->kuantitas;
        $nad->keterangan = $request->keterangan;
        $nad->dibuat_oleh = Auth::user()->id;

        $project->current_nomor_batch = $request->nomor_batch;
        $project->updated_at = date("Y-m-d H:i:s");

        $nad->save();
        $project->save();

        return response()->json([
            "status" => 1,
            "message" => 'Nota Antar Divisi berhasil dibuat',

        ],200);
    }

    public function editNad(Request $request){
        $nad = NotaAntarDivisi::find($request->id_nad);

        if($nad){
            $project= project::find($request->id_project);

            $nad->id_project=$request->id_project;
            $nad->nomor_WO= $request->nomor_WO;
            $nad->nomor_NAD= $request->nomor_NAD;
            $nad->nomor_batch = $request->nomor_batch;
            $nad->tanggal = date("Y-m-d H:i:s");
            $nad->divisi_yang_mengajukan = $request->divisi_yang_mengajukan;
            $nad->divisi_yang_diajukan = $request->divisi_yang_diajukan;
            $nad->nama_barang = $request->nama_barang;
            $nad->satuan = $request->satuan;
            $nad->kuantitas = $request->kuantitas;
            $nad->keterangan = $request->keterangan;
            $nad->dibuat_oleh = Auth::user()->id;


            $project->current_nomor_batch = $request->nomor_batch;
            $project->updated_at = date("Y-m-d H:i:s");

            $nad->save();
            $project->save();


            $user_dibuat_oleh= User::find($request->id_user);
            $nad->makeHidden(['dibuat_oleh','diketahui_oleh','disetujui_oleh']);

            return response()->json([
                "status" => 1,
                "message" => 'Nota Antar Divisi berhasil diubah',
                "data" => [
                    'nad'=>$nad,
                    'diketahui_oleh'=>$user_dibuat_oleh,
                ]
            ],200);
        }

        else{
            return response()->json([
                "status" => 0,
                "message" => 'Data Nad Salah',
            ],404);
        }

    }

    public function deleteNad(Request $request){
        $nad = NotaAntarDivisi::find($request->id_nad);

        if($nad->status != "Baru"){
            return response()->json([
                "message" => 'Nota Antar Divisi tidak bisa dihapus!',
            ],200);
        }

        $nad->delete();

        return response()->json([
            "status" => 1,
            "message" => 'Nota Antar Divisi berhasil dihapus',
        ],200);
    }

    public function approvalNad(Request $request){

        if($request->tujuan_approval == "diketahui_oleh"){
            $nad= NotaAntarDivisi::find($request->id_nad);

            if($nad){
                $nad->diketahui_oleh = $request->id_user;
                $nad->status= "Sedang diproses";
                $nad->save();

                return response()->json([
                    "status" => 1,
                    "message" => 'Approval diketahui oleh berhasil dilakukan',
                ],200);
            }

            else{
                return response()->json([
                    "status" => 0,
                    "message" => 'Data Nota Antar Divisi Salah',
                ],404);
            }
        }

        elseif($request->tujuan_approval =="disetujui_oleh"){
            $nad= NotaAntarDivisi::find($request->id_nad);

            $nad->disetujui_oleh = $request->id_user;
            $nad->save();

            return response()->json([
                "status" => 1,
                "message" => 'Approval disetujui oleh berhasil dilakukan',
            ],200);
        }
    }

}
