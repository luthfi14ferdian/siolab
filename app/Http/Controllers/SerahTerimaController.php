<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotaAntarDivisi;
use App\project;
use App\SerahTerima;
use App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use PDF;
use App\Http\Controllers\Controller;

class SerahTerimaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        if($request->id_item == NULL){
            $all_serahTerima = DB::table('serah_terimas')
            ->where('item', $request->id_proyek)
            ->join('projects', 'serah_terimas.item', '=', 'projects.id')
            ->select('serah_terimas.id', 'serah_terimas.tanggal', 'projects.nama_project as nama_item', 'serah_terimas.tujuan',
            'serah_terimas.status')
            ->get();

            $serahTerima = array_chunk($all_serahTerima->toArray(), 10);
            if (count($serahTerima) <= (($request->start)-1) ) {
                $serahTerima = [];
            }
            else {
                $serahTerima = $serahTerima[($request->start)-1];
            }

            $total_item = count($all_serahTerima);
            $current_page = (int)$request->start;
            $total_item_per_page = count($serahTerima);

            return response()->json([
                "status" => 1,
                "data" => [
                    "item" => $serahTerima
                ],
                "pagination" => [
                    "current_page" => $current_page,
                    "total_item" => $total_item,
                    "items_pr_page" => $total_item_per_page,
                ],
            ]);
        }
        else{
            return response()->json($this->getDetailSerahTerima($request));
        }
    }

    public function getDetailSerahTerima(Request $request){
        $detail_serahTerima = SerahTerima::find($request->id_item);
        $detail_serahTerima->id_NAD = DB::table('nota_antar_divisis')
            ->where('nota_antar_divisis.id', $detail_serahTerima->id_NAD)
            ->select('nota_antar_divisis.id', 'nota_antar_divisis.nomor_NAD')
            ->first();
        $detail_serahTerima->yang_menyerahkan = User::find($detail_serahTerima->yang_menyerahkan
        , ['id', 'username', 'last_name', 'first_name']);
        $detail_serahTerima->yang_menerima = User::find($detail_serahTerima->yang_menerima
        , ['id', 'username', 'last_name', 'first_name']);

        return $detail_serahTerima;

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SerahTerima  $serahTerima
     * @return \Illuminate\Http\Response
     */
    public function show(SerahTerima $serahTerima)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SerahTerima  $serahTerima
     * @return \Illuminate\Http\Response
     */
    public function edit(SerahTerima $serahTerima)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SerahTerima  $serahTerima
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SerahTerima $serahTerima)
    {
        //
    }

    public function export_pdf(Request $request)
    {
        $serah_terima = SerahTerima::find($request->id_item);

        $project = DB::table('projects')
        ->where('id', $request->id_proyek)
        ->get();

        $yang_menyerahkan = User::find($serah_terima->yang_menyerahkan
        , ['id', 'username', 'last_name', 'first_name']);
        $yang_menerima = User::find($serah_terima->yang_menerima
        , ['id', 'username', 'last_name', 'first_name']);

        $pdf = PDF::loadview('serah_terima_pdf',['serah_terima'=>$serah_terima,
        'project' => $project,
        'yang_menyerahkan' => $yang_menyerahkan,
        'yang_menerima' => $yang_menerima,]);

    	return $pdf->download('serah_terima.pdf');
    }
}
