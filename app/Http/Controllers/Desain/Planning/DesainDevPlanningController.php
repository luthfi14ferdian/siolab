<?php

namespace App\Http\Controllers\Desain\Planning;

use App\Helpers\RequestFileManager;
use App\Model\Desain\DesainDevPlanning;
use App\Http\Controllers\Controller;
use App\JenisProject;
use App\project;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DesainDevPlanningController extends Controller
{

    public function index(Request $request)
    {
        $project = project::find($request->id_project);
        if (!$project) {
            return response()->json([
                'status' => 0,
                'message' => 'Data not found'
            ], 404);
        }

        // TODO: Add disetujui_oleh to the table.
        $item = DesainDevPlanning::with(['disiapkan_oleh', 'diperiksa_oleh', 'disetujui_oleh'])
            ->where('id_proyek', $request->id_project)
            ->first();

        if ($item) {
            return response()->json([
                'status' => 1,
                'data' => $item
            ]);
        } else {
            return response()->json([
                'status' => 1,
                'data' => null
            ]);
        }
    }

    private function handleCreate(Request $request)
    {
        $data = $request->all();
        $data['status'] = "belum diproses";
        $data["disiapkan_oleh"] = Auth::user()->id;

        $fileManager = new RequestFileManager($request->file('dokumen'));

        $data["id_proyek"] = $request->id_project;
        $data["dokumen"] = $fileManager->getNewOrOriginal();

        try {
            $devObj = new DesainDevPlanning($data);
            $devObj->save();
        } catch (Exception $e) {
            $fileManager->pickOriginal();
            throw $e;
        }

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil ditambahkan"
        ], 200);
    }

    private function handleEdit(Request $request, DesainDevPlanning $devObj)
    {
        $fileManager = new RequestFileManager(
            $request->file('dokumen'),
            $devObj->dokumen
        );

        $devObj->nama_proyek = $request->nama_proyek;
        $devObj->nomor_dokumen = $request->nomor_dokumen;
        $devObj->tgl_pengerjaan = $request->tgl_pengerjaan;
        $devObj->dokumen =  $fileManager->getNewOrOriginal();

        try {
            $devObj->save();
        } catch (Exception $e) {
            $fileManager->pickOriginal();
            throw $e;
        }

        $fileManager->pickRequest();
        return response()->json([
            'status' => 1,
            'message' => "Data berhasil diubah"
        ], 200);
    }

    public function editOrCreate(Request $request)
    {
        $projectItem = project::where('id', $request->id_project)->first();

        if (!$projectItem) {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }

        $devObj = DesainDevPlanning::where('id_proyek', $request->id_project)->first();

        if ($devObj) {
            return $this->handleEdit($request, $devObj);
        } else {
            return $this->handleCreate($request);
        }
    }

    public function confirm(Request $request)
    {
        $devObj = DesainDevPlanning::where('id_proyek', $request->id_project)->first();
        if (!$devObj) {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }

        $type = $request->tipe;

        if ($type === "diperiksa") {
            $devObj->status = "diperiksa";
            $devObj->diperiksa_oleh = Auth::user()->id;
            $devObj->save();

            return response()->json([
                "status" => 1,
                "message" => 'Approval diperiksa oleh berhasil',
            ], 200);
        } elseif ($type == "disetujui") {
            $devObj->status = "disetujui";
            $devObj->disetujui_oleh = Auth::user()->id;
            $devObj->save();

            return response()->json([
                "status" => 1,
                "message" => 'Approval disetujui oleh berhasil',
            ], 200);
        }
    }

    public function export_pdf(Request $request)
    {
        $pdf = PDF::loadview('desain_dev_planning_pdf', [
            'nama' => 'Luthfi',
            'status' => "ganteng"
        ]);

        return $pdf->download('testing.pdf');
    }
}
