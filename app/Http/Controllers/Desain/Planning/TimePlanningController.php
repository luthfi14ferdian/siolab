<?php

namespace App\Http\Controllers\Desain\Planning;

use App\Helpers\RequestFileManager;
use App\Http\Controllers\Controller;
use App\Model\Desain\TimePlanning;
use App\project;
use Exception;
use Illuminate\Http\Request;

class TimePlanningController extends Controller
{
    public function index(Request $request)
    {
        $project = project::find($request->id_project);
        if (!$project) {
            return response()->json([
                'status' => 0,
                'message' => 'Data not found'
            ], 404);
        }

        // TODO: Add disetujui_oleh to the table.
        $item = TimePlanning::where('id_proyek', $request->id_project)
            ->first();

        if ($item) {
            return response()->json([
                'status' => 1,
                'data' => $item
            ]);
        } else {
            return response()->json([
                'status' => 1,
                'data' => null
            ]);
        }
    }

    public function editOrCreate(Request $request)
    {
        $projectItem = project::where('id', $request->id_project)->first();

        if (!$projectItem) {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }

        $timeObj = TimePlanning::where('id_proyek', $request->id_project)->first();

        if ($timeObj) {
            $fileManager = new RequestFileManager(
                $request->file('dokumen'),
                $timeObj->dokumen
            );
            $timeObj->dokumen =  $fileManager->getNewOrOriginal();

            try {
                $timeObj->save();
            } catch (Exception $e) {
                $fileManager->pickOriginal();
                throw $e;
            }
            $fileManager->pickRequest();
            return response()->json([
                'status' => 1,
                'message' => "Data berhasil diubah"
            ], 200);
        } else {
            $data = $request->all();
            $data["id_proyek"] = $request->id_project;
            $fileManager = new RequestFileManager($request->file('dokumen'));
            $data["dokumen"] = $fileManager->getNewOrOriginal();

            try {
                $timeObj = new TimePlanning($data);
                $timeObj->save();
            } catch (Exception $e) {
                $fileManager->pickOriginal();
                throw $e;
            }

            return response()->json([
                "status" => 1,
                "message" => "Data berhasil ditambahkan"
            ], 200);
        }
    }
}
