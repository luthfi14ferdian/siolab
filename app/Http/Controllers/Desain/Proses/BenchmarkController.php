<?php

namespace App\Http\Controllers\Desain\Proses;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\Benchmark;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\BenchmarkExport;

class BenchmarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allBenchmark = DB::table("benchmarks")
            ->select('benchmarks.*')
            ->where('id_proyek', $request->id_project)
            ->get();

        $benchmark = array_chunk($allBenchmark->toArray(), 10);

        if (count($benchmark) <= (($request->page) - 1)) {
            $benchmark = [];
        } else {
            $benchmark = $benchmark[($request->page) - 1];
        }

        $total_item = count($allBenchmark);
        $current_page = (int)$request->page;
        $total_item_per_page = count($benchmark);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $benchmark
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $benchmark = new Benchmark($request->all());
        $benchmark->save();
        return response()->json(["status" => 1, "message" => "Data berhasil disimpan"], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Benchmark  $benchmark
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $benchmark = Benchmark::find($request->id_item);

        if ($benchmark) {
            return response()->json($benchmark, 200);
        } else {
            return response()->json(['status' => "Data yang ada cari tidak ditemukan"], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Benchmark  $benchmark
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $benchmark = Benchmark::find($request->id_item);

        if ($benchmark) {
            $benchmark = $benchmark->update($request->all());

            return response()->json(['Success' => "Data berhasil diubah"], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new BenchmarkExport($request->id_proyek), 'Benchmark.xlsx');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Benchmark  $benchmark
     * @return \Illuminate\Http\Response
     */
    public function destroy(Benchmark $benchmark)
    {
        //
    }
}
