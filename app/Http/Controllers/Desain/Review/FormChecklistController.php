<?php

namespace App\Http\Controllers\Desain\Review;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\FormChecklist;
use Illuminate\Http\Request;

class FormChecklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->id_divisi) {
            $allCheck = DB::table("form_checklists")
                ->select('form_checklists.*')
                ->where('id_proyek', $request->id_project)
                ->get();
        } else {
            $allCheck = DB::table("form_checklists")
                ->select('form_checklists.*')
                ->where('id_proyek', $request->id_project)
                ->where('id_divisi', $request->id_divisi)
                ->get();
        }

        $formChecklist = array_chunk($allCheck->toArray(), 10);
        if (count($formChecklist) <= (($request->page) - 1)) {
            $formChecklist = [];
        } else {
            $formChecklist = $formChecklist[($request->page) - 1];
        }

        $total_item = count($allCheck);
        $current_page = (int)$request->page;
        $total_item_per_page = count($formChecklist);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $formChecklist
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Desain\FormChecklist  $formChecklist
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $formChecklist = FormChecklist::find($request->id_item);

        if ($formChecklist) {
            return response()->json($formChecklist);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Desain\FormChecklist  $formChecklist
     * @return \Illuminate\Http\Response
     */
    public function edit(FormChecklist $formChecklist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Desain\FormChecklist  $formChecklist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormChecklist $formChecklist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Desain\FormChecklist  $formChecklist
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormChecklist $formChecklist)
    {
        //
    }
}
