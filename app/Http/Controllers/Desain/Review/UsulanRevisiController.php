<?php

namespace App\Http\Controllers\Desain\Review;

use App\Http\Controllers\Controller;

use App\Model\Desain\UsulanRevisi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsulanRevisiController extends Controller
{
    public function index(Request $request)
    {
        $queryDb = UsulanRevisi::with('produk.jenis_project');
        if (isset($request->search_text)) {
            # 'produk' inhere is the relationship of
            # "UsulanRevisi" model with "project" model
            $queryDb->whereHas('produk', function ($query) use ($request) {
                # 'jenis_project' in here is the relationship
                # of "project" model with "JenisProject" model
                return $query->whereHas('jenis_project', function ($query)  use ($request) {
                    # 'jenis_project' in here is
                    # a column of "JenisProject" model
                    return $query->where('jenis_project', 'ilike',  '%' . $request->search_text . '%');
                });
            });
        };
        $paginate = $queryDb->paginate(10);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $paginate->items()
            ],
            "pagination" => [
                "current_page" => $paginate->currentPage(),
                "total_item" => $paginate->total(),
                "items_per_page" => $paginate->perPage(),
            ],
        ]);
    }

    public function create(Request $request)
    {
        $formData = $request->all();
        $formData["id_produk"] = $request->id_project;
        $formData["status"] = "baru";

        $usulan = new UsulanRevisi($formData);
        $usulan->save();

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan"
        ], 200);
    }

    public function detail(Request $request)
    {
        $usulan = UsulanRevisi::with(['produk.jenis_project', 'disetujui_oleh'])
            ->find($request->id_item);

        if ($usulan) {
            return response()->json([
                'status' => 1,
                'data' => $usulan
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => 'Data not found'
            ], 404);
        }
    }

    public function update(Request $request)
    {
        $usulan = UsulanRevisi::find($request->id_item);
        $checkResponse = $this->checkIfNotExistOrApproved($usulan);
        if ($checkResponse) {
            return $checkResponse;
        }

        $usulan->tanggal = $request->tanggal;
        $usulan->save();

        return response()->json([
            'status' => 1,
            'message' => "Data berhasil diubah"
        ], 200);
    }

    public function approve(Request $request)
    {
        $usulan = UsulanRevisi::find($request->id_item);
        $checkResponse = $this->checkIfNotExistOrApproved($usulan);
        if ($checkResponse) {
            return $checkResponse;
        }

        $usulan->disetujui_oleh = Auth::user()->id;
        $usulan->save();

        return response()->json([
            "status" => 1,
            "message" => 'Data berhasil dihapus',
        ], 200);
    }

    public function delete(Request $request)
    {
        $usulan = UsulanRevisi::find($request->id_item);
        $checkResponse = $this->checkIfNotExistOrApproved($usulan);
        if ($checkResponse) {
            return $checkResponse;
        }

        $usulan->delete();

        return response()->json([
            "status" => 1,
            "message" => 'Data berhasil dihapus',
        ], 200);
    }

    /**
     * Returns response object if item does not exist.
     * Otherwise, returns nothing.
     */
    private function checkIfNotExistOrApproved($item)
    {
        if (!$item) {
            return response()->json([
                'status' => 0,
                'message' => 'Data tidak ditemukan'
            ], 404);
        }
        if ($item->disetujui_oleh !== null) {
            return response()->json([
                'status' => 0,
                'message' => 'Data telah disetujui'
            ], 403);
        }
    }
}
