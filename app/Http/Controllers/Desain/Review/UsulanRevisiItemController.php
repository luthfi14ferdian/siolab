<?php

namespace App\Http\Controllers\Desain\Review;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Desain\UsulanRevisiItem;
use Illuminate\Support\Facades\Auth;

class UsulanRevisiItemController extends Controller
{
    public function index(Request $request)
    {
        $idUsulan = $request->id_usulan_revisi;
        $paginated = UsulanRevisiItem::with("pemberi_masukan")
            ->where('id_usulan_revisi', $idUsulan)
            ->orderBy("id")
            ->paginate(10);

        return response()->json([
            "status" => 1,
            "message" => 'Success',
            "data" => [
                "item" => $paginated->items(),
            ],
            "pagination" => [
                "current_page" => $paginated->currentPage(),
                "total_item" => $paginated->total(),
                "items_per_page" => $paginated->perPage(),
            ],
        ]);
    }

    public function create(Request $request)
    {
        $formData = $request->all();
        $formData["id_usulan_revisi"] = $request->id_usulan_revisi;
        $formData["pemberi_masukan"] = Auth::user()->id;
        $item = new UsulanRevisiItem($formData);
        $item->save();

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan",
            "data" => $item,
        ], 200);
    }

    public function update(Request $request)
    {
        $item = UsulanRevisiItem::find($request->id_item);
        $checkResponse = $this->checkIfNotExist($item);
        if ($checkResponse) {
            return $checkResponse;
        }

        $item->tanggal = $request->tanggal;
        $item->usulan_revisi = $request->usulan_revisi;
        $item->alasan = $request->alasan;
        $item->save();

        return response()->json([
            "status" => 1,
            "message" => 'Data berhasil diubah',
            'data' => $item,
        ], 200);
    }

    public function delete(Request $request)
    {
        $item = UsulanRevisiItem::find($request->id_item);
        $checkResponse = $this->checkIfNotExist($item);
        if ($checkResponse) {
            return $checkResponse;
        }

        $item->delete();

        return response()->json([
            "status" => 1,
            "message" => 'Data berhasil dihapus',
        ], 200);
    }

    public function confirm(Request $request)
    {
        $item = UsulanRevisiItem::find($request->id_item);
        $checkResponse = $this->checkIfNotExist($item);
        if ($checkResponse) {
            return $checkResponse;
        }

        $item->diketahui_oleh = Auth::user()->id;
        $item->save();

        return response()->json([
            "status" => 1,
            "message" => 'Data berhasil dihapus',
        ], 200);
    }

    /**
     * Returns response object if item does not exist.
     * Otherwise, returns nothing.
     */
    private function checkIfNotExist($item)
    {
        if (!$item) {
            return response()->json([
                'status' => 0,
                'message' => 'Data tidak ditemukan'
            ], 404);
        }
    }
}
