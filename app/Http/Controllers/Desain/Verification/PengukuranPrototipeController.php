<?php

namespace App\Http\Controllers\Desain\Verification;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\PengukuranPrototipe;
use Illuminate\Http\Request;

class PengukuranPrototipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pengukuranPrototipes = DB::table("pengukuran_prototipes")
            ->select('pengukuran_prototipes.*')
            ->where('id_proyek', $request->id_project)
            ->get();

        $pengukuranPrototipe = array_chunk($pengukuranPrototipes->toArray(), 10);
        if (count($pengukuranPrototipe) <= (($request->page) - 1)) {
            $pengukuranPrototipe = [];
        } else {
            $pengukuranPrototipe = $pengukuranPrototipe[($request->page) - 1];
        }

        $total_item = count($pengukuranPrototipes);
        $current_page = (int)$request->page;
        $total_item_per_page = count($pengukuranPrototipe);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $pengukuranPrototipe
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $pengukuranPrototipe = new PengukuranPrototipe($data);

        $pengukuranPrototipe->save();

        return response()->json($pengukuranPrototipe);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PengukuranPrototipe  $pengukuranPrototipe
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $pengukuranPrototipe = PengukuranPrototipe::find($request->id_item);

        if ($pengukuranPrototipe) {
            return response()->json($pengukuranPrototipe);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PengukuranPrototipe  $pengukuranPrototipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $pengukuranPrototipe = PengukuranPrototipe::find($request->id_item);


        if ($pengukuranPrototipe) {
            $pengukuranPrototipe = $pengukuranPrototipe->update($request->all());

            return response()->json(['Success' => "Data berhasil diubah"], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PengukuranPrototipe  $pengukuranPrototipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PengukuranPrototipe $pengukuranPrototipe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PengukuranPrototipe  $pengukuranPrototipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengukuranPrototipe $pengukuranPrototipe)
    {
        //
    }
}
