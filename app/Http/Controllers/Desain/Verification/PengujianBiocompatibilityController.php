<?php

namespace App\Http\Controllers\Desain\Verification;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\PengujianBiocompatibility;
use Illuminate\Http\Request;

class PengujianBiocompatibilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allPengujian = DB::table("fit3_d_s")
        ->select('fit3_d_s.*')
        ->where('id_proyek', $request->id_project)
        ->get();

        $pengujianBiocompatibility = array_chunk($allPengujian->toArray(), 10);
        if (count($pengujianBiocompatibility) <= (($request->page)-1) ) {
            $pengujianBiocompatibility = [];
        }
        else {
            $pengujianBiocompatibility = $pengujianBiocompatibility[($request->page)-1];
        }

        $total_item = count($allPengujian);
        $current_page = (int)$request->page;
        $total_item_per_page = count($pengujianBiocompatibility);
    
        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $pengujianBiocompatibility
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $pengujianBiocompatibility = new PengujianBiocompatibility($data);

        $pengujianBiocompatibility->save();

        return response()->json($pengujianBiocompatibility);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Desain\PengujianBiocompatibility  $pengujianBiocompatibility
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $pengujianBiocompatibility = PengujianBiocompatibility::find($request->id_item);

        if ($pengujianBiocompatibility) {
            return response()->json($pengujianBiocompatibility);
        }

        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Desain\PengujianBiocompatibility  $pengujianBiocompatibility
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $pengujianBiocompatibility = PengujianBiocompatibility::find($request->id_item);


        if ($pengujianBiocompatibility) {
            $pengujianBiocompatibility = $pengujianBiocompatibility->update($request->all());

            return response()->json(['Success' => "Data berhasil diubah"],200);
        }
        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Desain\PengujianBiocompatibility  $pengujianBiocompatibility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PengujianBiocompatibility $pengujianBiocompatibility)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Desain\PengujianBiocompatibility  $pengujianBiocompatibility
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengujianBiocompatibility $pengujianBiocompatibility)
    {
        //
    }
}
