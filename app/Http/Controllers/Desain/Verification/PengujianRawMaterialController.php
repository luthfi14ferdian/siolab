<?php

namespace App\Http\Controllers\Desain\Verification;

use App\Model\Desain\Fit3D;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\PengujianRawMaterial;
use Illuminate\Http\Request;

class PengujianRawMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allPengujian = DB::table("pengujian_raw_materials")
        ->select('pengujian_raw_materials.*')
        ->where('id_proyek', $request->id_project)
        ->get();

        $pengujianRawMaterial = array_chunk($allPengujian->toArray(), 10);
        if (count($pengujianRawMaterial) <= (($request->page)-1) ) {
            $pengujianRawMaterial = [];
        }
        else {
            $pengujianRawMaterial = $pengujianRawMaterial[($request->page)-1];
        }

        $total_item = count($allPengujian);
        $current_page = (int)$request->page;
        $total_item_per_page = count($pengujianRawMaterial);
    
        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $pengujianRawMaterial
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $pengujianRawMaterial = new PengujianRawMaterial($data);

        $pengujianRawMaterial->save();

        return response()->json($pengujianRawMaterial);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pengujianRawMaterial  $pengujianRawMaterial
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $PengujianRawMaterial = PengujianRawMaterial::find($request->id_item);

        if ($PengujianRawMaterial) {
            return response()->json($PengujianRawMaterial);
        }

        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pengujianRawMaterial  $pengujianRawMaterial
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $PengujianRawMaterial = Fit3D::find($request->id_item);


        if ($PengujianRawMaterial) {
            $fitPengujianRawMaterial3d = $PengujianRawMaterial->update($request->all());

            return response()->json(['Success' => "Data berhasil diubah"],200);
        }
        else {
            return response()->json([
                'status' => 0,
                'message'=> "Data not found"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pengujianRawMaterial  $pengujianRawMaterial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pengujianRawMaterial $pengujianRawMaterial)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pengujianRawMaterial  $pengujianRawMaterial
     * @return \Illuminate\Http\Response
     */
    public function destroy(pengujianRawMaterial $pengujianRawMaterial)
    {
        //
    }
}
