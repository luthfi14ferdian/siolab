<?php

namespace App\Http\Controllers\Desain\Verification;

use App\Model\Desain\Fit3D;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\PengujianPrototipe;
use Illuminate\Http\Request;

class PengujianPrototipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allPengujian = DB::table("pengujian_prototipes")
            ->select('pengujian_prototipes.*')
            ->where('id_proyek', $request->id_project)
            ->get();

        $pengujianPrototipe = array_chunk($allPengujian->toArray(), 10);
        if (count($pengujianPrototipe) <= (($request->page) - 1)) {
            $pengujianPrototipe = [];
        } else {
            $pengujianPrototipe = $pengujianPrototipe[($request->page) - 1];
        }

        $total_item = count($allPengujian);
        $current_page = (int)$request->page;
        $total_item_per_page = count($pengujianPrototipe);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $pengujianPrototipe
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $pengujianPrototipe = new Fit3D($data);

        $pengujianPrototipe->save();

        return response()->json($pengujianPrototipe);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Desain\PengujianPrototipe  $pengujianPrototipe
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $pengujianPrototipe = PengujianPrototipe::find($request->id_item);

        if ($pengujianPrototipe) {
            return response()->json($pengujianPrototipe);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Desain\PengujianPrototipe  $pengujianPrototipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $pengujianPrototipe = PengujianPrototipe::find($request->id_item);


        if ($pengujianPrototipe) {
            $pengujianPrototipe = $pengujianPrototipe->update($request->all());

            return response()->json(['Success' => "Data berhasil diubah"], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Desain\PengujianPrototipe  $pengujianPrototipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PengujianPrototipe $pengujianPrototipe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Desain\PengujianPrototipe  $pengujianPrototipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengujianPrototipe $pengujianPrototipe)
    {
        //
    }
}
