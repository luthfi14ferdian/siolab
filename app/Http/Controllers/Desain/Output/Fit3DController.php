<?php

namespace App\Http\Controllers\Desain\Output;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\Fit3D;
use Illuminate\Http\Request;

class Fit3DController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $allFit3D = DB::table("fit3_d_s")
            ->select('fit3_d_s.*')
            ->where('id_proyek', $request->id_project)
            ->get();

        $fit3D = array_chunk($allFit3D->toArray(), 10);
        if (count($fit3D) <= (($request->page) - 1)) {
            $fit3D = [];
        } else {
            $fit3D = $fit3D[($request->page) - 1];
        }

        $total_item = count($allFit3D);
        $current_page = (int)$request->page;
        $total_item_per_page = count($fit3D);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $fit3D
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $data = $request->all();
        $fit3d = new Fit3D($data);

        $fit3d->save();

        return response()->json(["status" => 1, "message" => "Data berhasil disimpan"], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Desain\Fit3D  $fit3D
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        $fit3d = Fit3D::find($request->id_item);

        if ($fit3d) {
            return response()->json($fit3d);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Desain\Fit3D  $fit3D
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $fit3d = Fit3D::find($request->id_item);


        if ($fit3d) {
            $fit3d = $fit3d->update($request->all());

            return response()->json(['Success' => "Data berhasil diubah"], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Desain\Fit3D  $fit3D
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fit3D $fit3D)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Desain\Fit3D  $fit3D
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fit3D $fit3D)
    {
        //
    }
}
