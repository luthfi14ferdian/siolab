<?php

namespace App\Http\Controllers\Desain\Output;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\SpesifikasiProduk;
use Illuminate\Http\Request;

class SpesifikasiProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allSpesifikasi = DB::table("spesifikasi_produks")
            ->select('spesifikasi_produks.*')
            ->where('id_proyek', $request->id_project)
            ->get();

        $specProduk = array_chunk($allSpesifikasi->toArray(), 10);
        if (count($specProduk) <= (($request->page) - 1)) {
            $specProduk = [];
        } else {
            $specProduk = $specProduk[($request->page) - 1];
        }

        $total_item = count($allSpesifikasi);
        $current_page = (int)$request->page;
        $total_item_per_page = count($specProduk);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $specProduk
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $specProduk = new SpesifikasiProduk($data);

        $specProduk->save();

        return response()->json(["status" => 1, "message" => "Data berhasil disimpan"], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Desain\SpesifikasiProduk  $spesifikasiProduk
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $specProduk = SpesifikasiProduk::find($request->id_item);

        if ($specProduk) {
            return response()->json($specProduk);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Desain\SpesifikasiProduk  $spesifikasiProduk
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $specProduk = SpesifikasiProduk::find($request->id_item);

        if ($specProduk) {
            $specProduk = $specProduk->update($request->all());

            return response()->json(['Success' => "Data berhasil diubah"], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Desain\SpesifikasiProduk  $spesifikasiProduk
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpesifikasiProduk $spesifikasiProduk)
    {
        //
    }
}
