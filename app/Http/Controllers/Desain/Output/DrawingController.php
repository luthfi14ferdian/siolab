<?php

namespace App\Http\Controllers\Desain\Output;

use App\Helpers\RequestFileManager;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\Drawing;
use Exception;
use Illuminate\Http\Request;

class DrawingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allDrawing = DB::table("drawings")
            ->select('drawings.*')
            ->where('id_proyek', $request->id_project)
            ->get();

        $drawing = array_chunk($allDrawing->toArray(), 10);
        if (count($drawing) <= (($request->page) - 1)) {
            $drawing = [];
        } else {
            $drawing = $drawing[($request->page) - 1];
        }

        $total_item = count($allDrawing);
        $current_page = (int)$request->page;
        $total_item_per_page = count($drawing);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $drawing
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasilGambarField = new RequestFileManager($request->file('hasil_gambar'));
        $filePendukungField = new RequestFileManager($request->file('file_pendukung'));

        $formData = $request->all();
        $formData["id_proyek"] = $request->id_project;
        $formData["hasil_gambar"] = $hasilGambarField->getNewOrOriginal();
        $formData["file_pendukung"] = $filePendukungField->getNewOrOriginal();

        try {
            $drawing = new Drawing($formData);
            $drawing->save();
        } catch (Exception $e) {
            $hasilGambarField->pickOriginal();
            $filePendukungField->pickOriginal();
            throw $e;
        }

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan"
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Desain\Drawing  $drawing
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $drawing = Drawing::find($request->id_item);

        if ($drawing) {
            return response()->json([
                "status" => 1,
                'data' => $drawing
            ]);
        } else {
            return response()->json([
                "status" => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Desain\Drawing  $drawing
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $drawing = Drawing::find($request->id_item);

        if ($drawing) {
            $hasilGambarField = new RequestFileManager($request->file('hasil_gambar'), $drawing->hasil_gambar);
            $filePendukungField = new RequestFileManager($request->file('file_pendukung'), $drawing->file_pendukung);

            $formData = $request->all();
            $formData["id_proyek"] = $drawing->id_proyek;
            $formData["hasil_gambar"] = $hasilGambarField->getNewOrOriginal();
            $formData["file_pendukung"] = $filePendukungField->getNewOrOriginal();

            try {
                $drawing = $drawing->update($formData);
            } catch (Exception $e) {
                $hasilGambarField->pickOriginal();
                $filePendukungField->pickOriginal();
                throw $e;
            }

            return response()->json([
                "status" => 1,
                "message" => "Data berhasil diubah"
            ], 200);
        } else {
            return response()->json([
                "status" => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Desain\Drawing  $drawing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Drawing $drawing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Desain\Drawing  $drawing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Drawing $drawing)
    {
        //
    }
}
