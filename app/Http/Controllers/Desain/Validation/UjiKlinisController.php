<?php

namespace App\Http\Controllers\Desain\Validation;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\UjiKlinis;
use Illuminate\Http\Request;

class UjiKlinisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allUjiKlinis = DB::table("uji_klinis")
            ->select('uji_klinis.*')
            ->where('id_proyek', $request->id_project)
            ->get();

        $ujiKlinis = array_chunk($allUjiKlinis->toArray(), 10);
        if (count($ujiKlinis) <= (($request->page) - 1)) {
            $ujiKlinis = [];
        } else {
            $ujiKlinis = $ujiKlinis[($request->page) - 1];
        }

        $total_item = count($allUjiKlinis);
        $current_page = (int)$request->page;
        $total_item_per_page = count($ujiKlinis);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $ujiKlinis
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $ujiKlinis = new UjiKlinis($data);

        $ujiKlinis->save();

        return response()->json($ujiKlinis);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UjiKlinis  $ujiKlinis
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $ujiKlinis = UjiKlinis::find($request->id_item);

        if ($ujiKlinis) {
            return response()->json($ujiKlinis);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UjiKlinis  $ujiKlinis
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $ujiKlinis = UjiKlinis::find($request->id_item);


        if ($ujiKlinis) {
            $ujiKlinis = $ujiKlinis->update($request->all());

            return response()->json(['Success' => "Data berhasil diubah"], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UjiKlinis  $ujiKlinis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UjiKlinis $ujiKlinis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UjiKlinis  $ujiKlinis
     * @return \Illuminate\Http\Response
     */
    public function destroy(UjiKlinis $ujiKlinis)
    {
        //
    }
}
