<?php


namespace App\Http\Controllers\Desain\Validation;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\UserTrial;
use Illuminate\Http\Request;

class UserTrialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allUserTrials = DB::table("user_trials")
            ->select('user_trials.*')
            ->where('id_proyek', $request->id_project)
            ->get();

        $userTrial = array_chunk($allUserTrials->toArray(), 10);
        if (count($userTrial) <= (($request->page) - 1)) {
            $userTrial = [];
        } else {
            $userTrial = $userTrial[($request->page) - 1];
        }

        $total_item = count($allUserTrials);
        $current_page = (int)$request->page;
        $total_item_per_page = count($userTrial);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $userTrial
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $userTrial = new UserTrial($data);

        $userTrial->save();

        return response()->json($userTrial);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Desain\UserTrial  $userTrial
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $userTrial = UserTrial::find($request->id_item);

        if ($userTrial) {
            return response()->json($userTrial);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Desain\UserTrial  $userTrial
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $userTrial = UserTrial::find($request->id_item);


        if ($userTrial) {
            $userTrial = $userTrial->update($request->all());

            return response()->json(['Success' => "Data berhasil diubah"], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Desain\UserTrial  $userTrial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserTrial $userTrial)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Desain\UserTrial  $userTrial
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserTrial $userTrial)
    {
        //
    }
}
