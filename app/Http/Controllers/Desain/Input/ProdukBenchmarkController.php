<?php

namespace App\Http\Controllers\Desain\Input;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\ProdukBenchmark;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ProdukBenchmarkExport;

class ProdukBenchmarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allProdukBenchmark = DB::table("produk_benchmarks")
            ->select('produk_benchmarks.*')
            ->where('id_proyek', $request->id_project)
            ->get();

        $produkBenchmark = array_chunk($allProdukBenchmark->toArray(), 10);

        if (count($produkBenchmark) <= (($request->page) - 1)) {
            $produkBenchmark = [];
        } else {
            $produkBenchmark = $produkBenchmark[($request->page) - 1];
        }

        $total_item = count($allProdukBenchmark);
        $current_page = (int)$request->page;
        $total_item_per_page = count($produkBenchmark);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $produkBenchmark
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->all();
        $formData["id_proyek"] = $request->id_project;
        $produkBenchmark = new ProdukBenchmark($formData);
        $produkBenchmark->save();
        return response()->json(["status" => 1, "message" => "Data berhasil disimpan"], 200);
    }

    public function show(Request $request)
    {
        $produkBenchmark = ProdukBenchmark::find($request->id_item);

        if ($produkBenchmark) {
            return response()->json([
                'status' => 1,
                'data' => $produkBenchmark
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => 'Data not found'
            ], 404);
        }
    }

    public function edit(Request $request)
    {
        $produkBenchmark = ProdukBenchmark::find($request->id_item);

        if ($produkBenchmark) {
            $formData = $request->all();
            $formData["id_proyek"] = $produkBenchmark->id_proyek;
            $produkBenchmark = $produkBenchmark->update($formData);

            return response()->json([
                'status' => 1,
                'message' => "Data berhasil diubah",
            ], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found",
            ]);
        }
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new ProdukBenchmarkExport($request->id_proyek), 'Produk-Benchmark.xlsx');
    }

    public function destroy(ProdukBenchmark $produkBenchmark)
    {
        //
    }
}
