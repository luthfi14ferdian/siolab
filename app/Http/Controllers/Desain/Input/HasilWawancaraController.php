<?php

namespace App\Http\Controllers\Desain\Input;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\HasilWawancara;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HasilWawancaraExport;
use App\Helpers\RequestFileManager;
use Exception;

class HasilWawancaraController extends Controller
{
    public function index(Request $request)
    {
        $paginate = DB::table("hasil_wawancaras")
            ->select('hasil_wawancaras.*')
            ->where('id_proyek', $request->id_project)
            ->paginate(10);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $paginate->items()
            ],
            "pagination" => [
                "current_page" => $paginate->currentPage(),
                "total_item" => $paginate->total(),
                "items_per_page" => $paginate->perPage(),
            ],
        ]);
    }

    public function detail(Request $request)
    {
        $wawancara = HasilWawancara::find($request->id_item);
        $checkResponse = $this->checkIfNotExist($wawancara);
        if ($checkResponse) {
            return $checkResponse;
        }

        return response()->json([
            'status' => 1,
            'data' => $wawancara
        ]);
    }

    public function create(Request $request)
    {
        $fileManager = new RequestFileManager($request->file('dok_hasil_wawancara'));

        $formData = $request->all();
        $formData["id_proyek"] = $request->id_project;
        $formData["dok_hasil_wawancara"] = $fileManager->getNewOrOriginal();

        try {
            $wawancara = new HasilWawancara($formData);
            $wawancara->save();
        } catch (Exception $e) {
            $fileManager->pickOriginal();
            throw $e;
        }

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan"
        ], 200);
    }
    public function update(Request $request)
    {
        $wawancara = HasilWawancara::find($request->id_item);
        $checkResponse = $this->checkIfNotExist($wawancara);
        if ($checkResponse) {
            return $checkResponse;
        }

        $formData = $request->all();
        $formData["id_proyek"] = $wawancara->id_proyek;
        $pastPath = $wawancara->dok_hasil_wawancara;

        $fileManager = new RequestFileManager($request->file('dok_hasil_wawancara'), $pastPath);
        $formData["dok_hasil_wawancara"] = $fileManager->getNewOrOriginal();

        try {
            $wawancara = $wawancara->update($formData);
        } catch (Exception $e) {
            // There's an error in DB. We must undo putting file and delete it.
            $fileManager->pickOriginal();
            throw $e;
        }
        $fileManager->pickRequest();
        return response()->json([
            'status' => 1,
            'message' => "Data berhasil diubah"
        ], 200);
    }

    public function delete(Request $request)
    {
        $wawancara = HasilWawancara::find($request->id_item);
        $checkResponse = $this->checkIfNotExist($wawancara);
        if ($checkResponse) {
            return $checkResponse;
        }

        $wawancara->delete();

        return response()->json([
            "status" => 1,
            "message" => 'Data berhasil dihapus',
        ], 200);
    }

    /**
     * Returns response object if item does not exist.
     * Otherwise, returns nothing.
     */
    private function checkIfNotExist($item)
    {
        if (!$item) {
            return response()->json([
                'status' => 0,
                'message' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new HasilWawancaraExport($request->id_proyek), 'HasilWawancara.xlsx');
    }
}
