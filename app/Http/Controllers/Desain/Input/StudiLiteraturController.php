<?php

namespace App\Http\Controllers\Desain\Input;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Model\Desain\StudiLiteratur;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\StudiLiteraturExport;
use App\Helpers\RequestFileManager;
use Exception;

class StudiLiteraturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allStudi = DB::table("studi_literaturs")
            ->select('studi_literaturs.*')
            ->where('id_proyek', $request->id_project)
            ->get();

        $studiLiteratur = array_chunk($allStudi->toArray(), 10);
        if (count($studiLiteratur) <= (($request->page) - 1)) {
            $studiLiteratur = [];
        } else {
            $studiLiteratur = $studiLiteratur[($request->page) - 1];
        }

        $total_item = count($allStudi);
        $current_page = (int)$request->page;
        $total_item_per_page = count($studiLiteratur);

        return response()->json([
            "status" => 1,
            "data" => [
                "item" => $studiLiteratur
            ],
            "pagination" => [
                "current_page" => $current_page,
                "total_item" => $total_item,
                "items_per_page" => $total_item_per_page,
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileManager = new RequestFileManager($request->file('dok_studi_literatur'));

        $formData = $request->all();
        $formData["id_proyek"] = $request->id_project;
        $formData["dok_studi_literatur"] = $fileManager->getNewOrOriginal();

        try {
            $studiLiteratur = new StudiLiteratur($formData);
            $studiLiteratur->save();
        } catch (Exception $e) {
            // There's an error in DB. We must undo putting file and delete it.
            $fileManager->pickOriginal();
            throw $e;
        }

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan"
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StudiLiteratur  $studiLiteratur
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $studiLiteratur = StudiLiteratur::find($request->id_item);

        if ($studiLiteratur) {
            return response()->json([
                'status' => 1,
                'data' => $studiLiteratur
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => 'Data not found'
            ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudiLiteratur  $studiLiteratur
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $studiLiteratur = StudiLiteratur::find($request->id_item);
        if ($studiLiteratur) {

            $formData = $request->all();
            $formData["id_proyek"] = $studiLiteratur->id_proyek;
            $pastPath = $studiLiteratur->dok_studi_literatur;

            $fileManager = new RequestFileManager($request->file('dok_studi_literatur'), $pastPath);
            $formData["dok_studi_literatur"] = $fileManager->getNewOrOriginal();

            try {
                $studiLiteratur = $studiLiteratur->update($formData);
            } catch (Exception $e) {
                // There's an error in DB. We must undo putting file and delete it.
                $fileManager->pickOriginal();
                throw $e;
            }
            $fileManager->pickRequest();
            return response()->json([
                'status' => 1,
                'message' => "Data berhasil diubah"
            ], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message' => "Data not found"
            ]);
        }
    }

    public function export_excel(Request $request)
    {
        return Excel::download(new StudiLiteraturExport($request->id_proyek), 'Studi-Literatur.xlsx');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudiLiteratur  $studiLiteratur
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudiLiteratur $studiLiteratur)
    {
        //
    }
}
