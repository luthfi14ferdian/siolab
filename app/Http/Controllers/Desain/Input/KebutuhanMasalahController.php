<?php

namespace App\Http\Controllers\Desain\Input;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Desain\KebutuhanMasalah;
use Illuminate\Support\Facades\Auth;

class KebutuhanMasalahController extends Controller
{
    public function index(Request $request)
    {
        $idWawancara = $request->id_hasil_wawancara;
        $paginated = KebutuhanMasalah::with("diketahui_oleh")
            ->where('id_hasil_wawancara', $idWawancara)
            ->orderBy("id")
            ->paginate(10);

        return response()->json([
            "status" => 1,
            "message" => 'Success',
            "data" => [
                "item" => $paginated->items(),
            ],
            "pagination" => [
                "current_page" => $paginated->currentPage(),
                "total_item" => $paginated->total(),
                "items_per_page" => $paginated->perPage(),
            ],
        ]);
    }

    public function create(Request $request)
    {
        $formData = $request->all();
        $formData["id_hasil_wawancara"] = $request->id_hasil_wawancara;
        $item = new KebutuhanMasalah($formData);
        $item->save();

        return response()->json([
            "status" => 1,
            "message" => "Data berhasil disimpan",
            "data" => $item,
        ], 200);
    }

    public function update(Request $request)
    {
        $item = KebutuhanMasalah::find($request->id_item);
        $checkResponse = $this->checkIfNotExistOrConfirmed($item);
        if ($checkResponse) {
            return $checkResponse;
        }

        $item->tanggal = $request->tanggal;
        $item->poin_kebutuhan_masalah = $request->poin_kebutuhan_masalah;
        $item->save();

        return response()->json([
            "status" => 1,
            "message" => 'Data berhasil diubah',
            'data' => $item,
        ], 200);
    }

    public function delete(Request $request)
    {
        $item = KebutuhanMasalah::find($request->id_item);
        $checkResponse = $this->checkIfNotExistOrConfirmed($item);
        if ($checkResponse) {
            return $checkResponse;
        }

        $item->delete();

        return response()->json([
            "status" => 1,
            "message" => 'Data berhasil dihapus',
        ], 200);
    }

    public function confirm(Request $request)
    {
        $item = KebutuhanMasalah::find($request->id_item);
        $checkResponse = $this->checkIfNotExistOrConfirmed($item);
        if ($checkResponse) {
            return $checkResponse;
        }

        $item->diketahui_oleh = Auth::user()->id;
        $item->save();

        return response()->json([
            "status" => 1,
            "message" => 'Data berhasil dihapus',
        ], 200);
    }

    /**
     * Returns response object if item does not exist or has been confirmed.
     * Otherwise, returns nothing.
     */
    private function checkIfNotExistOrConfirmed($item)
    {
        if (!$item) {
            return response()->json([
                'status' => 0,
                'message' => 'Data tidak ditemukan'
            ], 404);
        }

        if ($item->diketahui_oleh !== null) {
            return response()->json([
                'status' => 0,
                'message' => 'Data telah diketahui'
            ], 403);
        }
    }
}
