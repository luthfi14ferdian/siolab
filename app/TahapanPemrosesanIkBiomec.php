<?php

namespace App;

use App\Model\InstruksiKerjaBiomec;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class TahapanPemrosesanIkBiomec extends Model
{
    public static function serializePemrosesanIkItem($idPercobaan)
    {

        $pemrosesanIK = DB::table("pemrosesan_ik_biomecs")
            ->select("pemrosesan_ik_biomecs.*")
            ->where("pemrosesan_ik_biomecs.id_pemrosesan", $idPercobaan)
            ->get();

        if (count($pemrosesanIK) != 0) {

            $dataIk = InstruksiKerjaBiomec::getForDetail($pemrosesanIK[0]->id_ik);

            $dataUmumItems = DB::table("pemrosesan_ik_biomecs")
                ->join("pemrosesan_biomecs", "pemrosesan_ik_biomecs.id_pemrosesan", "=", "pemrosesan_biomecs.id")
                ->where("pemrosesan_biomecs.id", $idPercobaan)
                ->get();

            $dataUmum = $dataUmumItems[0];
            $dataUmum->diproses_oleh =  User::find($dataUmum->diproses_oleh);
            $dataUmum->produk = Produk::find($dataUmum->id_produk);

            $allDataTahapan = DB::table("tahapan_pemrosesan_ik_biomecs")
                ->join("tahapan_ik_biomecs", "tahapan_pemrosesan_ik_biomecs.id_tahapan", "=", "tahapan_ik_biomecs.id")
                ->select("tahapan_ik_biomecs.uraian", "tahapan_pemrosesan_ik_biomecs.*", "tahapan_ik_biomecs.urutan")
                ->where("tahapan_pemrosesan_ik_biomecs.id_pemrosesan", $idPercobaan)
                ->get()->sortBy("urutan");

            $alats = DB::table("alat_tahapan_ik_biomecs")
                ->join("alat_biomecs", "alat_tahapan_ik_biomecs.id_alat", "=", "alat_biomecs.id")
                ->select("alat_biomecs.*", "alat_tahapan_ik_biomecs.id_tahapan")
                ->get();

            $konfirmasi_penyelesaiaan = false;
            $allTahapanArr = [];

            for ($i = 0; $i < count($allDataTahapan); $i++) {
                $alatTemp = [];
                $tahapanItem = $allDataTahapan[$i];
                $tahapanItem->alat_terkait = [];

                $alat = $alats->where("id_tahapan", $tahapanItem->id_tahapan);

                foreach ($alat as $key => $value) {
                    $thisAlat = (object) [
                        "id_alat" => $value->id,
                        "kode_alat" => $value->kode,
                        "nama_alat" => $value->nama,
                        "tipe_alat" => $value->tipe,
                        "fungsi_alat" => $value->fungsi,
                        "created_at" => $value->created_at,
                        "updated_at" => $value->updated_at,
                    ];

                    $param = DB::table("param_alat_biomecs")
                        ->join("parameter_pemrosesan_biomecs", "param_alat_biomecs.id", "=", "parameter_pemrosesan_biomecs.id_parameter")
                        ->select("param_alat_biomecs.id", "param_alat_biomecs.parameter", "param_alat_biomecs.metric", "parameter_pemrosesan_biomecs.value")
                        ->where("param_alat_biomecs.id_alat", $value->id)
                        ->where("parameter_pemrosesan_biomecs.id_tahapan", $tahapanItem->id_tahapan)
                        ->where("parameter_pemrosesan_biomecs.id_pemrosesan", $idPercobaan)
                        ->get();

                    $thisAlat->parameters = $param->toArray();
                    array_push($alatTemp, $thisAlat);
                }

                $tahapanItem->alat_terkait = $alatTemp;

                $konfirmasi_penyelesaiaan = false;
                $status_tahapans = DB::table("tahapan_pemrosesan_ik_biomecs")
                    ->select("tahapan_pemrosesan_ik_biomecs.status")
                    ->where("tahapan_pemrosesan_ik_biomecs.id_pemrosesan", $idPercobaan)
                    ->get()->pluck("status")->toArray();

                if (!in_array("Sedang dikerjakan", $status_tahapans, true) && !in_array("Belum diproses", $status_tahapans, true)) {
                    $konfirmasi_penyelesaiaan = true;
                }

                array_push(
                    $allTahapanArr,
                    $tahapanItem
                );
            }

            return [
                "data_umum" => $dataUmum,
                "data_ik" => $dataIk,
                "tahapan_pemrosesan" => $allTahapanArr,
                "konfirmasi_penyelesaiaan" => $konfirmasi_penyelesaiaan,
            ];
        }
    }
}
