<?php

namespace App\Exports;

use App\Model\Desain\Benchmark;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BenchmarkExport implements FromCollection, WithHeadings
{
    protected $id_proyek;

    function __construct($id_proyek) {
            $this->id_proyek = $id_proyek;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Benchmark::where('id_proyek', $this->id_proyek)
        ->get();
    }

    public function headings(): array
    {
        return [
            "id",
            "no_batch",
            "geo_dan_dimensi",
            "pengujian",
            "modelling_dan_simulasi",
            "data_spesifikasi",
            "nilai",
            "satuan",
            "ref",
            "id_proyek",
            "created_at",
            "updated_at",
        ];
    }
}
