<?php

namespace App\Exports;

use App\DokumenDesainTransfer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DokumenDesainTransferExport implements FromCollection, WithHeadings
{
    protected $id_proyek;

    function __construct($id_proyek) {
            $this->id_proyek = $id_proyek;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DokumenDesainTransfer::where('id_proyek', $this->id_proyek)
        ->get();
    }

    public function headings(): array
    {
        return [
            "id",
            "no_batch",
            "no_dokumen",
            "nama_dokumen",
            "dibuat_oleh",
            "disetujui_oleh",
            "dokumen_pendukung",
            "id_proyek",
            "created_at",
            "updated_at",
        ];
    }
}
