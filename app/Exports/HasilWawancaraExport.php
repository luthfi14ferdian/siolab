<?php

namespace App\Exports;

use App\Model\Desain\HasilWawancara;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class HasilWawancaraExport implements FromCollection, WithHeadings
{
    protected $id_proyek;

    function __construct($id_proyek) {
            $this->id_proyek = $id_proyek;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return HasilWawancara::where('id_proyek', $this->id_proyek)
        ->get();
    }

    public function headings(): array
    {
        return [
            "id",
            "no_batch",
            "nomor_dokumen",
            "nama_pewawancara",
            "nama_narasumber",
            "tgl_wawancara",
            "dok_hasil_wawancara",
            "id_proyek",
            "created_at",
            "updated_at",
        ];
    }
}
