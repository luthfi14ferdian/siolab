<?php

namespace App\Exports;

use App\Model\Desain\StudiLiteratur;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class StudiLiteraturExport implements FromCollection, WithHeadings
{
    protected $id_proyek;

    function __construct($id_proyek) {
            $this->id_proyek = $id_proyek;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return StudiLiteratur::where('id_proyek', $this->id_proyek)
        ->get();
    }

    public function headings(): array
    {
        
        return [
            "id",
            "no_batch",
            "no_dokumen",
            "judul_literatur",
            "link_url_source",
            "dok_studi_literatur",
            "id_proyek",
            "created_at",
            "updated_at",
        ];
    }
}
