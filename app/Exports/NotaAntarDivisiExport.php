<?php

namespace App\Exports;

use App\NotaAntarDivisi;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class NotaAntarDivisiExport implements FromCollection, WithHeadings
{
    protected $id_proyek;

    function __construct($id_proyek) {
            $this->id_proyek = $id_proyek;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return NotaAntarDivisi::where('nama_barang', $this->id_proyek)
        ->get();
    }

    public function headings(): array
    {
        return [
            'id',
            'nomor_WO',
            'nomor_NAD',
            'nomor_batch',
            'tanggal',
            'divisi_yang_mengajukan',
            'divisi_yang_diajukan',
            'nama_barang',
            'satuan',
            'kuantitas',
            'keterangan',
            'dibuat_oleh',
            'diketahui_oleh',
            'disetujui_oleh',
            'dokumen_3d_fit',
            'dokumen_drawing',
            'dokumen_spec_desain',
            'status',
            'created_at',
            'updated_at',
        ];
    }
}
