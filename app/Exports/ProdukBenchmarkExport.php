<?php

namespace App\Exports;

use App\Model\Desain\ProdukBenchmark;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProdukBenchmarkExport implements FromCollection, WithHeadings
{
    protected $id_proyek;

    function __construct($id_proyek) {
            $this->id_proyek = $id_proyek;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return ProdukBenchmark::where('id_proyek', $this->id_proyek)
        ->get();
    }

    public function headings(): array
    {
        return [
            "id",
            "no_batch",
            "judul",
            "source",
            "form_kebutuhan",
            "id_proyek",
            "created_at",
            "updated_at",
        ];
    }
}
