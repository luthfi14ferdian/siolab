<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarVerifikasiKoreksi extends Model
{
    public $timestamps = false;
    protected $table = 'car_verifikasi_koreksi';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id',
        'id_car',
        'efektivitas',
        'realisasi',
        'auditor',
        'manager_representative',
    ];
}
