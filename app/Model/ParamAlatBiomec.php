<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ParamAlatBiomec extends Model
{
    protected $table = 'param_alat_biomecs';
    public $timestamps = true;
}
