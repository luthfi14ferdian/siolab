<?php

namespace App\Model\Biocompatibility;

use Illuminate\Database\Eloquent\Model;

class TahapanPengujian extends Model
{
    protected $table = 'tahapan_pengujians';
    public $timestamps = true;
}
