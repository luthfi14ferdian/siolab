<?php

namespace App\Model\Biocompatibility;

use Illuminate\Database\Eloquent\Model;

class DaftarPengujian extends Model
{
    protected $table = 'daftar_pengujians';
    public $timestamps = true;

    protected $hidden = [
        'created_at', 'updated_at'
   ];
}
