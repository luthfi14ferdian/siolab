<?php

namespace App\Model\Biocompatibility;

use Illuminate\Database\Eloquent\Model;

class DokumenPengujian extends Model
{
    protected $table = 'dokumen_pengujians';
    public $timestamps = true;
}
