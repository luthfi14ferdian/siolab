<?php

namespace App\Model\Biocompatibility;

use Illuminate\Database\Eloquent\Model;

class GambarPengujian extends Model
{
    protected $table = 'gambar_pengujians';
    public $timestamps = true;
}
