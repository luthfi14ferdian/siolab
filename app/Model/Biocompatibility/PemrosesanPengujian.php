<?php

namespace App\Model\Biocompatibility;

use Illuminate\Database\Eloquent\Model;

class PemrosesanPengujian extends Model
{
    protected $table = 'pemrosesan_pengujians';
    public $timestamps = true;
}
