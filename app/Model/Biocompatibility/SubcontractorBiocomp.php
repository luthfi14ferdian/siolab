<?php

namespace App\Model\Biocompatibility;

use Illuminate\Database\Eloquent\Model;

class SubcontractorBiocomp extends Model
{
    public $timestamps = true;
    protected $table = 'subcontractor_biocomps';

    protected $hidden = [
        'created_at', 'updated_at'
   ];
}
