<?php

namespace App\Model\Biocompatibility;

use Illuminate\Database\Eloquent\Model;

class TahapanPemrosesanBiocomp extends Model
{
    protected $table = 'tahapan_pemrosesan_biocomps';
    public $timestamps = true;

}
