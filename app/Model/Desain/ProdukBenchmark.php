<?php

namespace App\Model\Desain;

use Illuminate\Database\Eloquent\Model;

class ProdukBenchmark extends Model
{
    public $timestamps = true;
    protected $fillable = [
        "no_batch",
        "judul",
        "source",
        "form_kebutuhan",
        "id_proyek",
    ];
}
