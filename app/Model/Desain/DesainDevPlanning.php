<?php

namespace App\Model\Desain;

use Illuminate\Database\Eloquent\Model;

class DesainDevPlanning extends Model
{
    public $timestamps = true;
    protected $fillable = [
        'nama_proyek',
        'nomor_dokumen',
        'tgl_pengerjaan',
        'disiapkan_oleh',
        'diperiksa_oleh',
        'status',
        'dokumen',
        'id_proyek',
    ];

    public function disiapkan_oleh()
    {
        return $this->belongsTo('App\User', 'disiapkan_oleh');
    }

    public function diperiksa_oleh()
    {
        return $this->belongsTo('App\User', 'diperiksa_oleh');
    }

    public function disetujui_oleh()
    {
        return $this->belongsTo('App\User', 'disetujui_oleh');
    }
}
