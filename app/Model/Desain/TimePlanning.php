<?php

namespace App\Model\Desain;

use Illuminate\Database\Eloquent\Model;

class TimePlanning extends Model
{
    public $timestamps = true;
    protected $fillable = [
        'dokumen',
        'id_proyek',
    ];
}
