<?php

namespace App\Model\Desain;

use Illuminate\Database\Eloquent\Model;

class Benchmark extends Model
{
    public $timestamps = true;
    protected $fillable = [
        "no_batch",
        "geo_dan_dimensi",
        "pengujian",
        "modelling_dan_simulasi",
        "data_spesifikasi",
        "nilai",
        "satuan",
        "ref",
        "id_proyek"
    ];
}
