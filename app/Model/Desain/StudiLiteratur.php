<?php

namespace App\Model\Desain;

use Illuminate\Database\Eloquent\Model;

class StudiLiteratur extends Model
{
    public $timestamps = true;
    protected $fillable = [
        "no_batch",
        "no_dokumen",
        "judul_literatur",
        "link_url_source",
        "dok_studi_literatur",
        "id_proyek"
    ];
}
