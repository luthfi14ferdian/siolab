<?php

namespace App\Model\Desain;

use Illuminate\Database\Eloquent\Model;

class UsulanRevisiItem extends Model
{
    protected $table = 'usulan_revisi_items';
    public $timestamps = true;
    protected $fillable = [
        'tanggal',
        'usulan_revisi',
        'alasan',
        'pemberi_masukan',
        'id_usulan_revisi',
    ];

    public function pemberi_masukan()
    {
        return $this->belongsTo('App\User', 'pemberi_masukan');
    }
}
