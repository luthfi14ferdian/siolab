<?php

namespace App\Model\Desain;

use Illuminate\Database\Eloquent\Model;

class SpesifikasiProduk extends Model
{
    public $timestamps = true;
    protected $fillable = [
        "no_batch",
        "data_spesifikasi",
        "nilai",
        "satuan",
        "ref",
        "id_proyek",
    ];
}
