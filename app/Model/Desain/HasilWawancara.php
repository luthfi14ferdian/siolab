<?php

namespace App\Model\Desain;

use Illuminate\Database\Eloquent\Model;

class HasilWawancara extends Model
{
    public $timestamps = true;
    protected $fillable = [
        'no_batch',
        'nomor_dokumen',
        'nama_pewawancara',
        'nama_narasumber',
        'tgl_wawancara',
        'dok_hasil_wawancara',
        'id_proyek'
    ];
}
