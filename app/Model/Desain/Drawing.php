<?php

namespace App\Model\Desain;

use Illuminate\Database\Eloquent\Model;

class Drawing extends Model
{
    public $timestamps = true;
    protected $fillable = [
        "no_batch",
        "judul_dokumen",
        "file_pendukung",
        "hasil_gambar",
        "id_proyek",
    ];
}
