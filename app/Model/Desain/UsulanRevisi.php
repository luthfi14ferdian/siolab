<?php

namespace App\Model\Desain;

use Illuminate\Database\Eloquent\Model;

class UsulanRevisi extends Model
{
    public $timestamps = true;
    protected $fillable = [
        'id_produk',
        'tanggal',
        'status',
        'disetujui_oleh'
    ];

    public function disetujui_oleh()
    {
        return $this->belongsTo('App\User', 'disetujui_oleh');
    }

    public function produk()
    {
        return $this->belongsTo('App\project', 'id_produk');
    }
}
