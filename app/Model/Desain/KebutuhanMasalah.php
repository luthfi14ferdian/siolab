<?php

namespace App\Model\Desain;

use Illuminate\Database\Eloquent\Model;

class KebutuhanMasalah extends Model
{
    protected $table = 'kebutuhan_masalahs';
    public $timestamps = true;
    protected $fillable = [
        'tanggal',
        'poin_kebutuhan_masalah',
        'id_hasil_wawancara',
        'diketahui_oleh',
    ];

    public function diketahui_oleh()
    {
        return $this->belongsTo('App\User', 'diketahui_oleh');
    }
}
