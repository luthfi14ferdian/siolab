<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KalibrasiAlat extends Model
{
    protected $table = 'kalibrasi_alats';
    public $timestamps = true;
}
