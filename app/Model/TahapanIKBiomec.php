<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TahapanIKBiomec extends Model
{
    public $timestamps = true;
    protected $table = 'tahapan_ik_biomecs';
    protected $fillable = [
        "id_ik",
        "uraian",
        "urutan",
    ];
}
