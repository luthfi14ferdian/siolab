<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class InstruksiKerjaBiomec extends Model
{
    public $timestamps = true;
    protected $table = 'instruksi_kerja_biomecs';
    protected $fillable = [
        "disiapkan_oleh",
        "disetujui_oleh",
        "diperiksa_oleh",
        "kode",
        "nama",
        "tujuan",
        "path_file_ik",
        "path_file_hasil_ik",
    ];

    public static function getForDetail($id)
    {
        $item = InstruksiKerjaBiomec::find($id);
        if (!$item) {
            return;
        }

        $item = $item->replicate();
        $item->disiapkan_oleh = User::find($item->disiapkan_oleh);
        $item->diperiksa_oleh = User::find($item->diperiksa_oleh);
        $item->disetujui_oleh = User::find($item->disetujui_oleh);
        return $item;
    }
}
