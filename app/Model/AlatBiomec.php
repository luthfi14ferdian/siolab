<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AlatBiomec extends Model
{
    protected $table = 'alat_biomecs';
    public $timestamps = true;
    protected $fillable = [
        "kode",
        "nama",
        "tipe",
        "fungsi",
    ];

}
