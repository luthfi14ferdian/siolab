<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParamAlat extends Model
{
    public $timestamps = true;
    protected $fillable = [
        "id_alat",
        "parameter",
        "metric",
    ];
}
