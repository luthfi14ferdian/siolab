<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanGeometri extends Model
{
    protected $table = 'laporan_geometri';

    protected $hidden = [
        'created_at', 'updated_at'
   ];

   public function percobaan()
    {

        return $this->belongsTo('App\PercobaanGeometri', 'id_geometri');
    }

}
