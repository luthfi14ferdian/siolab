<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogBookProduksiManufaktur extends Model
{
    public $timestamps = false;
    protected $table = 'log_book_produksi_manufakturs';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
        'id',
        'tanggal',
        'nama_item',
        'no_batch',
        'material',
        'jumlah_trial',
        'jumlah_prod',
        'jumlah_exist',
        'jumlah_lose',
        'keterangan',
        'dikerjakan_oleh',
        'status'
    ];
}
