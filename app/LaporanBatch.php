<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanBatch extends Model
{
    protected $table = 'laporan_batch';

    protected $hidden = [
        'created_at', 'updated_at'
   ];

   public function produk()
    {

        return $this->belongsTo('App\Produk', 'id_produk');
    }
}
