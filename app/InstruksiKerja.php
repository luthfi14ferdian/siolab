<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstruksiKerja extends Model
{
    public $timestamps = true;
    protected $fillable = [
        "disiapkan_oleh",
        "disetujui_oleh",
        "diperiksa_oleh",
        "kode",
        "nama",
        "tujuan",
        "path_file_ik",
        "path_file_hasil_ik",
    ];

    public static function getForDetail($id)
    {
        $item = InstruksiKerja::find($id);
        if (!$item) {
            return;
        }

        $item = $item->replicate();
        $item->disiapkan_oleh = User::find($item->disiapkan_oleh);
        $item->diperiksa_oleh = User::find($item->diperiksa_oleh);
        $item->disetujui_oleh = User::find($item->disetujui_oleh);
        return $item;
    }
}
