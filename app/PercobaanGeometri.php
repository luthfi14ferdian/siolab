<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PercobaanGeometri extends Model
{
    protected $table = 'percobaan_geometri';

    protected $hidden = [
        'created_at', 'updated_at'
   ];

}
