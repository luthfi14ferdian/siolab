<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisProject extends Model
{
    public $timestamps = false;
    protected $table = 'jenis_projects';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $hidden = [
        'created_at', 'updated_at'
   ];
}
