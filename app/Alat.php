<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alat extends Model
{
    public $timestamps = true;
    protected $fillable = [
        "kode",
        "nama",
        "tipe",
        "fungsi",
    ];
}
