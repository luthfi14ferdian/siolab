<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DokumenNAD extends Model
{
    public $timestamps = false;
    protected $table = 'dokumen_nad';

    protected $hidden = [
        'created_at', 'updated_at'
   ];
}
