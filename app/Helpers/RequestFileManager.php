<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

/**
 * Request file manager that deals with deleting and putting file
 * according to new/old path.
 */

class RequestFileManager
{
    public function __construct($file, $originalPath = null, $fieldRequired = true)
    {
        $this->fieldRequired = $fieldRequired;
        $this->hasRequest = isset($file) && $file !== null;
        $this->hasOriginalPath = $originalPath !== null || $originalPath === '';

        if ($this->hasRequest) {

            $name = FilePathUtils::getRandomFileName($file);
            $path = Storage::putFileAs('desain_input', new File($file), $name);

            $this->filePath = $path;
        }

        $this->originalPath = $originalPath;
        $this->originalPathDeleted = false;
        $this->fileDeleted = false;
    }

    public function pickOriginal()
    {
        if ($this->hasRequest) {
            Storage::disk('public')->delete($this->filePath);
        }
        unset($this->filePath);
        $this->fileDeleted = true;
    }

    /**
     * Delete original path, if exists.
     * If field is required, but no file request is given,
     * this does nothing. 
     */

    public function pickRequest()
    {
        if (!$this->fieldRequired || !$this->hasRequest && $this->hasOriginalPath) {
            Storage::disk('public')->delete($this->originalPath);
            unset($this->originalPath);
            $this->originalPathDeleted = true;
        }
    }

    public function getNewOrOriginal()
    {
        if ($this->hasRequest) {
            return $this->filePath;
        } else {
            return $this->originalPath;
        }
    }
}
