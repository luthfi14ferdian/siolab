<?php

namespace App\Helpers;

use Illuminate\Http\UploadedFile;

class FilePathUtils
{

    public static function getRandomFileName(UploadedFile $file)
    {
        $name = $file->getClientOriginalName();
        $filePathinfo = pathinfo($name);

        $fileName = isset($filePathinfo["filename"]) ? $filePathinfo["filename"] : "";
        $fileExt = isset($filePathinfo["extension"]) ? $filePathinfo["extension"] : "";
        $extensionExists = isset($filePathinfo["extension"]);

        # Sanitize name and file extension
        $fileName = preg_replace('/[^A-z0-9]+/', '-', $fileName);
        $fileExt = preg_replace('/[^A-z0-9]+/', '-', $fileExt);
        if ($extensionExists) {
            $fileExt = '.' . $fileExt;
        }

        $extensionCharLength = strlen($fileExt);

        $randomName = preg_replace('/\.[A-z0-9]+/', '', $file->hashName());
        if ($extensionExists) {
            $randomName = substr($randomName, 0, -$extensionCharLength);
        }

        $joinedName = substr($randomName . "_" . $fileName, 0, 255 - $extensionCharLength);
        return $joinedName . $fileExt;
    }
}
