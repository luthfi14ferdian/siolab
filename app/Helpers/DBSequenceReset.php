<?php

namespace App\Helpers;
use Illuminate\Support\Facades\DB;

class DBSequenceReset
{
    /** 
     * NOTE: table_name and table_seq_name is unescaped.
     */

    public static function resetDbIncrement($table_name, $table_seq_name)
    {
        # Update PostgreSQL's autoincrement sequence
        DB::unprepared("
        BEGIN;

        -- protect against concurrent inserts while you update the counter
        LOCK TABLE ".$table_name." IN EXCLUSIVE MODE;

        -- Update the sequence
        SELECT setval('".$table_seq_name."',
            COALESCE((SELECT MAX(id)+1 FROM ".$table_name."), 1), false);

        COMMIT;
        ");
    }
}
