<?php

namespace App;

use DB;
use App\User;
use Illuminate\Database\Eloquent\Model;

class PemrosesanAlat extends Model
{
    /**
     * Return serialized percobaan item. If item is found, return the serialized array.
     * If not, return null
     */

    public static function serializePercobaanItem($idPercobaan)
    {

        // Select data umum while checking whether item exists or not
        $matchingPercobaanItems =  DB::table("pemrosesan_alats")
            ->join("pemrosesans", "pemrosesan_alats.id_pemrosesan", "=", "pemrosesans.id")
            ->where("pemrosesans.id", $idPercobaan)->get()->toArray();

        if (count($matchingPercobaanItems) < 1) {
            return;
        }


        $dataUmum = $matchingPercobaanItems[0];
        $dataUmum->produk = Produk::find($dataUmum->id_produk);

        unset($dataUmum->id_pemrosesan);

        $dataUmum->diproses_oleh = User::find($dataUmum->diproses_oleh);

        $alats = DB::table("percobaan_alats")
            ->join("alats", "percobaan_alats.id_alat", "=", "alats.id")
            ->join("pemrosesans", "percobaan_alats.id_pemrosesan", "=", "pemrosesans.id")
            ->select("alats.*", "percobaan_alats.note")
            ->where("pemrosesans.id", $idPercobaan)
            ->get();

        $alatTemp = [];
        foreach ($alats as $key => $value) {
            $thisAlat = [];
            $thisAlat = collect($thisAlat)->put("id", $value->id);
            $thisAlat->put("kode", $value->kode);
            $thisAlat->put("nama", $value->nama);
            $thisAlat->put("tipe", $value->tipe);
            $thisAlat->put("fungsi", $value->fungsi);
            $thisAlat->put("note", $value->note);

            $param = DB::table("parameter_pemrosesans")
                ->where("parameter_pemrosesans.id_pemrosesan", $idPercobaan)
                ->join("param_alats", "parameter_pemrosesans.id_parameter", "=", "param_alats.id")
                ->select("param_alats.id", "param_alats.parameter", "param_alats.metric", "parameter_pemrosesans.value")
                ->where("param_alats.id_alat", $value->id)
                ->get();

            $thisAlat->put("parameters", $param->toArray());

            array_push($alatTemp, $thisAlat);
        }

        return [
            "data_umum" => $dataUmum,
            "alats" => $alatTemp,
        ];
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk', 'id_produk');
    }
}
