<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role_project extends Model
{
    public $timestamps = false;
    protected $table = 'role_projects';
}
