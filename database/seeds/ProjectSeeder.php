<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Helpers\DBSequenceReset;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('jenis_projects')->delete();
        DB::table('projects')->delete();

        DB::table('jenis_projects')->insert([
            'id' => 1,
            'jenis_project' => 'MiniScrew',
        ]);

        DB::table('jenis_projects')->insert([
            'id' => 2,
            'jenis_project' => 'Miniplate',
        ]);

        DB::table('projects')->insert([
            'id' => 1,
            'nama_project' => 1,
            'tahapan' => 'Rev 0',
            'nomor_batch_awal' => 'A1-B2-C3',
            'divisi_saat_ini' => 3,
            'project_leader' => 2,
            'pic_desain' => 1,
            'pic_material' => 2,
            'pic_manufaktur' => 3,
            'pic_surface_treatment' => 4,
            'pic_metrologi' => 5,
            'pic_biomech' => 6,
            'pic_biocomp' => 7,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('projects')->insert([
            'id' => 2,
            'nama_project' => 2,
            'tahapan' => 'Rev 0',
            'nomor_batch_awal' => 'A1-B2-C3',
            'divisi_saat_ini' => 2,
            'project_leader' => 2,
            'pic_desain' => 1,
            'pic_material' => 2,
            'pic_manufaktur' => 3,
            'pic_surface_treatment' => 4,
            'pic_metrologi' => 5,
            'pic_biomech' => 6,
            'pic_biocomp' => 7,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DBSequenceReset::resetDbIncrement('jenis_projects', 'jenis_projects_id_seq');
        DBSequenceReset::resetDbIncrement('projects', 'projects_id_seq');
    }
}
