<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\project;
use App\Model\Desain\Drawing;
use App\Model\Desain\Fit3D;
use App\Model\Desain\SpesifikasiProduk;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ProjectSeeder::class);  
        $this->call(ProdukMaterialSeeder::class);  
        $this->call(NotaAntarDivisiSeeder::class); 


        DB::table('fit3_d_s')->insert([
            'id_proyek' => App\project::all()->random()->id,
            'no_batch' => Str::random(10),
            'judul_dokumen' => Str::random(10),
            'file_pendukung' => Str::random(10),
            'hasil_gambar' => Str::random(10),
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('drawings')->insert([
            'id_proyek' => App\project::all()->random()->id,
            'no_batch' => Str::random(10),
            'judul_dokumen' => Str::random(10),
            'file_pendukung' => Str::random(10),
            'hasil_gambar' => Str::random(10),
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('spesifikasi_produks')->insert([
            'id_proyek' => App\project::all()->random()->id,
            'no_batch' => Str::random(10),
            'data_spesifikasi' => Str::random(10),
            'nilai' => Str::random(10),
            'satuan' => Str::random(10),
            'ref' => Str::random(10),
            'created_at' => date("Y-m-d H:i:s"),
        ]);

    }
}
