<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Helpers\DBSequenceReset;

class ProdukMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materials')->insert([
            'id' => 1,
            'jenis_material' => 'Large Rod',
            'satuan' => 'Meter(M)',
            'keterangan' => 'Untuk membuat miniscrew',
            'id_jenis_proyek' => 1
        ]);

        DB::table('materials')->insert([
            'id' => 2,
            'jenis_material' => 'Mini Rod',
            'satuan' => 'Meter (M)',
            'keterangan' => 'Untuk membuat miniplate',
            'id_jenis_proyek' => 2
        ]);

        DB::table('materials')->insert([
            'id' => 3,
            'jenis_material' => 'Mini Rod',
            'satuan' => 'Centimeter (Cm)',
            'keterangan' => 'Untuk membuat miniplate',
            'id_jenis_proyek' => 2
        ]);

        DBSequenceReset::resetDbIncrement('materials', 'materials_id_seq');

        DB::table('produk')->insert([
            'id' => 1,
            'kode_produk' => 'MS1A1',
            'id_project' => 1,
            'jenis_material' => 1,
            'nama_produk' => 'Pecahan Miniscrew',
            'jumlah_item' => 25,
            'yang_menambahkan' => 2,
            'keterangan' => 'produk miniscrew',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('produk')->insert([
            'id' => 2,
            'kode_produk' => 'MP1A1',
            'id_project' => 2,
            'jenis_material' => 2,
            'nama_produk' => 'Pecahan Miniplate',
            'jumlah_item' => 25,
            'yang_menambahkan' => 2,
            'keterangan' => 'produk miniplate',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DBSequenceReset::resetDbIncrement('produk', 'produk_id_seq');
    }
}
