<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Helpers\DBSequenceReset;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DBSequenceReset::resetDbIncrement('users', 'users_id_seq');

        DB::table('users')->insert([
            'username' => 'siolab1',
            'password' => bcrypt('siolab1'),
            'first_name' => 'siolab1',
            'last_name' => 'siolab',
            'email' => 'siolab1@gmail.com',
            'divisi_id' => 1,
            'role_id' => 1,
            'is_approve' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'username' => 'siolab2',
            'password' => bcrypt('siolab2'),
            'first_name' => 'siolab2',
            'last_name' => 'siolab',
            'email' => 'siolab2@gmail.com',
            'divisi_id' => 2,
            'role_id' => 1,
            'is_approve' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'username' => 'siolab3',
            'password' => bcrypt('siolab3'),
            'first_name' => 'siolab3',
            'last_name' => 'siolab',
            'email' => 'siolab3@gmail.com',
            'divisi_id' => 3,
            'role_id' => 1,
            'is_approve' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'username' => 'siolab4',
            'password' => bcrypt('siolab4'),
            'first_name' => 'siolab4',
            'last_name' => 'siolab',
            'email' => 'siolab4@gmail.com',
            'divisi_id' => 4,
            'role_id' => 1,
            'is_approve' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);


        DB::table('users')->insert([
            'username' => 'siolab5',
            'password' => bcrypt('siolab5'),
            'first_name' => 'siolab5',
            'last_name' => 'siolab',
            'email' => 'siolab5@gmail.com',
            'divisi_id' => 5,
            'role_id' => 1,
            'is_approve' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'username' => 'siolab6',
            'password' => bcrypt('siolab6'),
            'first_name' => 'siolab6',
            'last_name' => 'siolab',
            'email' => 'siolab6@gmail.com',
            'divisi_id' => 6,
            'role_id' => 1,
            'is_approve' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'username' => 'siolab7',
            'password' => bcrypt('siolab7'),
            'first_name' => 'siolab7',
            'last_name' => 'siolab',
            'email' => 'siolab7@gmail.com',
            'divisi_id' => 7,
            'role_id' => 1,
            'is_approve' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'first_name' => 'Admin',
            'last_name' => 'admin',
            'email' => 'admin@gmail.com',
            'divisi_id' => 4,
            'role_id' => 5,
            'is_approve' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'username' => 'koordinatordivisi',
            'password' => bcrypt('koordinatordivisi'),
            'first_name' => 'Koordinator',
            'last_name' => 'Divisi',
            'email' => 'koordinatordivisi@gmail.com',
            'divisi_id' => 4,
            'role_id' => 3,
            'is_approve' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('users')->insert([
            'username' => 'penanggungjawab',
            'password' => bcrypt('penanggungjawab'),
            'first_name' => 'Penanggung',
            'last_name' => 'Jawaban',
            'email' => 'penanggungjawab@gmail.com',
            'divisi_id' => 4,
            'role_id' => 2,
            'is_approve' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
