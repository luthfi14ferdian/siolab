<?php

use App\Helpers\DBSequenceReset;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class NotaAntarDivisiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nota_antar_divisis')->delete();
        DBSequenceReset::resetDbIncrement('nota_antar_divisis', 'nota_antar_divisis_id_seq');

        DB::table('nota_antar_divisis')->insert([
            'id_project'=>1,
            'nomor_WO'=> '09272020',
            'nomor_NAD'=>'NAD09272020',
            'nomor_batch'=>'A1-B3-C3',
            'status'=>'Baru',
            'tanggal'=>date("Y-m-d H:i:s"),
            'id_produk'=>1,
            'nama_barang'=>'Mini Screw A1B2C4D5',
            'divisi_yang_mengajukan'=>6,
            'divisi_yang_diajukan'=>7,
            'satuan'=>'pcs',
            'kuantitas'=>20,
            'keterangan'=>'NAD untuk divisi Biomechanic',
            'dibuat_oleh'=>6,
            'created_at'=>date("Y-m-d H:i:s"),
        ]);

        DB::table('nota_antar_divisis')->insert([
            'id_project'=>1,
            'nomor_WO'=> '09272020',
            'nomor_NAD'=>'NAD09272020',
            'nomor_batch'=>'A1-B3-C3',
            'status'=>'Baru',
            'tanggal'=>date("Y-m-d H:i:s"),
            'id_produk'=>2,
            'nama_barang'=>'Mini Screw A1B2C4D5',
            'divisi_yang_mengajukan'=>5,
            'divisi_yang_diajukan'=>6,
            'satuan'=>'pcs',
            'kuantitas'=>20,
            'keterangan'=>'NAD untuk divisi Surface treatment',
            'dibuat_oleh'=>5,
            'created_at'=>date("Y-m-d H:i:s"),
        ]);
        
    }
}
