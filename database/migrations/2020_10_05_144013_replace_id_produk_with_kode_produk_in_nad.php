<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * NOTE: This migration name is reversed. It should be
 * "Replace Kode Produk with ID Produk in NAD"
 */

class ReplaceIdProdukWithKodeProdukInNad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nota_antar_divisis', function (Blueprint $table) {
            $table->dropForeign('nota_antar_divisis_kode_produk_foreign');
            $table->dropColumn('kode_produk');
            $table->bigInteger('id_produk')->unsigned()->default('1'); // TODO: Delete in production !
            $table->foreign('id_produk')
                ->references('id')
                ->on('produk')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nota_antar_divisis', function (Blueprint $table) {
            $table->dropForeign('nota_antar_divisis_id_produk_foreign');
            $table->dropColumn('id_produk');

            $table->string('kode_produk',128)->default('MS1A1'); // TODO: Delete in production !
            $table->foreign('kode_produk')
            ->references('kode_produk')
            ->on('produk')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }
}
