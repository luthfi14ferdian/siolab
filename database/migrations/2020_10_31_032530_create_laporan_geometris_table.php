<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporanGeometrisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_geometri', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_laporan')->unsigned();
            $table->foreign('id_laporan')
            ->references('id')
            ->on('laporan_batch')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('nama_sampel',128);
            $table->float('rata_rata');
            $table->float('dimensi_desain');
            $table->float('gap');
            $table->float('penyimpangan');
            $table->string('judgement',128);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_geometri');
    }
}
