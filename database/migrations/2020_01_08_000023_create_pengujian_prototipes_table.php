<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengujianPrototipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengujian_prototipes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_batch',128);
            $table->string('judul',128);
            $table->string('dokumen',225);
            $table->bigInteger('id_proyek')->unsigned();
            $table->foreign('id_proyek')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengujian_prototipes');
    }
}
