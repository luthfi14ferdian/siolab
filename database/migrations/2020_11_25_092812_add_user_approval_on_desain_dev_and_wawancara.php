<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserApprovalOnDesainDevAndWawancara extends Migration
{
    public function up()
    {
        Schema::table('desain_dev_plannings', function (Blueprint $table) {
            $table->bigInteger('disetujui_oleh')->unsigned()->nullable();
            $table->foreign('disetujui_oleh')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
        Schema::table('kebutuhan_masalahs', function (Blueprint $table) {
            $table->bigInteger('diketahui_oleh')->unsigned()->nullable();
            $table->foreign('diketahui_oleh')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('desain_dev_plannings', function (Blueprint $table) {
            $table->dropForeign('desain_dev_plannings_disetujui_oleh_foreign');
            $table->dropColumn('disetujui_oleh');
        });
        Schema::table('kebutuhan_masalahs', function (Blueprint $table) {
            $table->dropForeign('kebutuhan_masalahs_diketahui_oleh_foreign');
            $table->dropColumn('diketahui_oleh');
        });
    }
}
