<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogMesinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_mesins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_mesin',128);
            $table->date('tanggal');
            $table->string('nama_item',128);

            $table->bigInteger('material')->unsigned();
            $table->foreign('material')
            ->references('id')
            ->on('materials')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('no_batch',128);
            $table->integer('jumlah_trial');
            $table->integer('jumlah_pod');
            $table->integer('jumlah_exist');
            $table->string('aktivitas',128);

            $table->bigInteger('yang_mengerjakan')->unsigned();
            $table->foreign('yang_mengerjakan')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            
            

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_mesins');
    }
}
