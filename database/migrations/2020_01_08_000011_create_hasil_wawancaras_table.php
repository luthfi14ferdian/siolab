<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHasilWawancarasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_wawancaras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_batch',128);
            $table->string('nomor_dokumen',128);
            $table->string('nama_pewawancara',128);
            $table->string('nama_narasumber',128);
            $table->date('tgl_wawancara');
            $table->string('dok_hasil_wawancara',255);
            $table->bigInteger('id_proyek')->unsigned();
            $table->foreign('id_proyek')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil_wawancaras');
    }
}
