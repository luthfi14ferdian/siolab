<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_produk', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('kode_produk',128);
            $table->foreign('kode_produk')
            ->references('kode_produk')
            ->on('produk')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('no_batch', 128);
            $table->date('tanggal');

            $table->bigInteger('divisi_mengerjakan')->unsigned()->nullable();
            $table->foreign('divisi_mengerjakan')
            ->references('id')
            ->on('divisi')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('yang_mengerjakan')->unsigned()->nullable();
            $table->foreign('yang_mengerjakan')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('jenis_aktivitas', 128);
            $table->string('id_aktivitas', 16);
            $table->string('detail_kegiatan', 255);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_produk');
    }
}
