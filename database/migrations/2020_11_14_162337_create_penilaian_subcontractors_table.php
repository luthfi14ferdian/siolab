<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianSubcontractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_subcontractors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_subcontractor')->unsigned();
            $table->foreign('id_subcontractor')
            ->references('id')
            ->on('subcontractor_biocomps')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('kriteria_pemasok',128);
            $table->integer('nilai_pemasok');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian_subcontractors');
    }
}
