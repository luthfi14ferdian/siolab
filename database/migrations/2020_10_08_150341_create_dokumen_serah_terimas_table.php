<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDokumenSerahTerimasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_serah_terimas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_project')->unsigned();
            $table->foreign('id_project')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_produk')->unsigned();
            $table->foreign('id_produk')
            ->references('id')
            ->on('produk')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->date("tanggal");
            $table->string('nama_item',128);
            $table->integer('jumlah');
            $table->longtext('keterangan');
            $table->string('status',128);

            $table->bigInteger('divisi_yang_mengajukan')->unsigned();
            $table->foreign('divisi_yang_mengajukan')
            ->references('id')
            ->on('divisi')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('divisi_yang_diajukan')->unsigned();
            $table->foreign('divisi_yang_diajukan')
            ->references('id')
            ->on('divisi')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('yang_menyerahkan')->unsigned()->nullable();
            $table->foreign('yang_menyerahkan')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('yang_menerima')->unsigned()->nullable();
            $table->foreign('yang_menerima')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen_serah_terimas');
    }
}
