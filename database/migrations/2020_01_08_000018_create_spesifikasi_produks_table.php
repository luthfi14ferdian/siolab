<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpesifikasiProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spesifikasi_produks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("no_batch",128);
            $table->string("data_spesifikasi",64);
            $table->string("nilai",128);
            $table->string("satuan",64);
            $table->string("ref",128)->nullable();

            $table->bigInteger('id_proyek')->unsigned();
            $table->foreign('id_proyek')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spesifikasi_produks');
    }
}
