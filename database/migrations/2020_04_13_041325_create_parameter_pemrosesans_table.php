<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterPemrosesansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_pemrosesans', function (Blueprint $table) {
            $table->bigInteger('id_parameter')->unsigned();
            $table->foreign('id_parameter')
            ->references('id')
            ->on('param_alats')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_tahapan')->unsigned()->nullable();
            $table->foreign('id_tahapan')
            ->references('id')
            ->on('tahapan_iks')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_pemrosesan')->unsigned();
            $table->foreign('id_pemrosesan')
            ->references('id')
            ->on('pemrosesans')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('value',128)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_pemrosesans');
    }
}
