<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemrosesanIkBiomecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemrosesan_ik_biomecs', function (Blueprint $table) {
            $table->bigInteger('id_ik')->unsigned();
            $table->foreign('id_ik')
            ->references('id')
            ->on('instruksi_kerja_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_pemrosesan')->unsigned();
            $table->foreign('id_pemrosesan')
            ->references('id')
            ->on('pemrosesan_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('diproses_oleh')->unsigned()->nullable();
            $table->foreign('diproses_oleh')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->Integer('jumlah_sisa')->nullable()->default(null);
            $table->string('path_file_hasil', 4096)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemrosesan_ik_biomecs');
    }
}
