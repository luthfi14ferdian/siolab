<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogBookProduksiManufaktursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_book_produksi_manufakturs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal');

            $table->bigInteger('nama_item')->unsigned();
            $table->foreign('nama_item')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('no_batch',128);

            $table->bigInteger('material')->unsigned();
            $table->foreign('material')
            ->references('id')
            ->on('materials')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('mesin')->unsigned();
            $table->foreign('mesin')
            ->references('id')
            ->on('mesins')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->integer('jumlah_trial')->nullable();
            $table->integer('jumlah_prod')->nullable();
            $table->integer('jumlah_exist')->nullable();
            $table->integer('jumlah_lose')->nullable();
            $table->text('keterangan')->nullable();

            $table->bigInteger('dikerjakan_oleh')->unsigned();
            $table->foreign('dikerjakan_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('status',128);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_book_produksi_manufakturs');
    }
}
