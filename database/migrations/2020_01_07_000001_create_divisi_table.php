<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDivisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divisi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->timestamps();
        });

        DB::table('divisi')->insert([
            ['id'=>1, 'nama' => 'desain','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>2,'nama'=> 'material','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>3,'nama'=> 'manufaktur','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>4,'nama'=> 'metrologi','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>5,'nama'=> 'biocompatibility','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>6,'nama'=> 'surface_treatment','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>7,'nama'=> 'biomechanic','created_at'=> date("Y-m-d H:i:s")]
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divisi');
    }
}
