<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTahapanPengujiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahapan_pengujians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pengujian')->unsigned();
            $table->foreign('id_pengujian')
            ->references('id')
            ->on('daftar_pengujians')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->longText('uraian');
            $table->longText('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahapan_pengujians');
    }
}
