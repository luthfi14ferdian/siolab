<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterPemrosesanBiomecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_pemrosesan_biomecs', function (Blueprint $table) {
            $table->bigInteger('id_parameter')->unsigned();
            $table->foreign('id_parameter')
            ->references('id')
            ->on('param_alat_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_tahapan')->unsigned()->nullable();
            $table->foreign('id_tahapan')
            ->references('id')
            ->on('tahapan_ik_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_pemrosesan')->unsigned();
            $table->foreign('id_pemrosesan')
            ->references('id')
            ->on('pemrosesan_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('value',128)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_pemrosesan_biomecs');
    }
}
