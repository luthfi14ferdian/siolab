<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->timestamps();
        });
        DB::table('role')->insert([
            ['id'=>1,'nama' => 'Anggota','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>2,'nama' => 'Penanggung Jawab Divisi','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>3,'nama'=> 'Koordinator Divisi','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>4,'nama'=> 'Manajer Representative','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>5,'nama'=> 'Admin','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>6,'nama'=> 'Super Admin','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>7,'nama'=> 'PIC Divisi Project','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>8,'nama'=> 'Project Leader','created_at'=> date("Y-m-d H:i:s")],
            ['id'=>9,'nama'=> 'Koordinator Lab','created_at'=> date("Y-m-d H:i:s")],
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role');
    }
}
