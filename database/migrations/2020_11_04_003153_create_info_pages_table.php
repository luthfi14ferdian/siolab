<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_page', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("nama_dokumen",128);
            $table->string("kategori",128);
            $table->string("dokumen_path");
            $table->string('nama_file',128);

            $table->bigInteger('diinput_oleh')->unsigned();
            $table->foreign('diinput_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->longText('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_page');
    }
}
