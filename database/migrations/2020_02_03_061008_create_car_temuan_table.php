<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarTemuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_temuan', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_car')->unsigned();
            $table->foreign('id_car')
            ->references('id')
            ->on('dokumen_car')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->text('temuan');
            $table->text('note')->nullable();
            $table->string('status');

            $table->bigInteger('auditor')->unsigned();
            $table->foreign('auditor')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('auditee')->unsigned()->nullable();
            $table->foreign('auditee')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('manager_representative')->unsigned()->nullable();
            $table->foreign('manager_representative')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_temuan');
    }
}
