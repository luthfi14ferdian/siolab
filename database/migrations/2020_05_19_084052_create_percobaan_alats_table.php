<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePercobaanAlatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('percobaan_alats', function (Blueprint $table) {
            $table->bigInteger('id_pemrosesan')->unsigned();
            $table->foreign('id_pemrosesan')
            ->references('id')
            ->on('pemrosesans')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_alat')->unsigned();
            $table->foreign('id_alat')
            ->references('id')
            ->on('alats')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('note',128);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('percobaan_alats');
    }
}
