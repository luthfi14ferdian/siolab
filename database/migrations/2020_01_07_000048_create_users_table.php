<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username',128)->unique();
            $table->string('password',128);
            $table->string('first_name',128);
            $table->string('last_name',128);
            $table->string('email',128)->unique();

            $table->bigInteger('divisi_id')->unsigned();
            $table->foreign('divisi_id')
            ->references('id')
            ->on('divisi')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('role_id')->unsigned();
            $table->foreign('role_id')
            ->references('id')
            ->on('role')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('role_project')->unsigned()->nullable();
            $table->foreign('role_project')
            ->references('id')
            ->on('role')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->boolean('is_approve')->default(false);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
