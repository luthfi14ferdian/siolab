<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_project')->unsigned()->nullable();
            $table->foreign('id_project')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('jenis_material')->unsigned()->nullable();
            $table->foreign('jenis_material')
            ->references('id')
            ->on('materials')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('kode_produk', 128)->unique();
            $table->string('nama_produk', 128);
            $table->integer('jumlah_item')->nullable();

            $table->bigInteger('yang_menambahkan')->unsigned()->nullable();
            $table->foreign('yang_menambahkan')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk');
    }
}
