<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDaftarPengujiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftar_pengujians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_dokumen',128);
            $table->string('nomor_dokumen',128);
            $table->string('tujuan_pengujian',128);
            $table->date('tanggal');
            $table->longText('tujuan')->nullable();
            $table->longText('ruang_lingkup')->nullable();
            $table->longText('referensi')->nullable();
            $table->longText('kebijakan_umum')->nullable();
            $table->longText('tanggung_jawab')->nullable();
            $table->longText('definisi')->nullable();
            $table->string('status',128);

            $table->bigInteger('disiapkan_oleh')->unsigned()->nullable();
            $table->foreign('disiapkan_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('diperiksa_oleh')->unsigned()->nullable();
            $table->foreign('diperiksa_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('disetujui_oleh')->unsigned()->nullable();
            $table->foreign('disetujui_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftar_pengujians');
    }
}
