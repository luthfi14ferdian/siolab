<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFit3DSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fit3_d_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("no_batch",128);
            $table->string("judul_dokumen",128);
            $table->string("file_pendukung",255)->nullable();
            $table->string("hasil_gambar",255);

            $table->bigInteger('id_proyek')->unsigned();
            $table->foreign('id_proyek')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fit3_d_s');
    }
}
