<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemrosesanIksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemrosesan_iks', function (Blueprint $table) {
            $table->bigInteger('id_ik')->unsigned();
            $table->foreign('id_ik')
            ->references('id')
            ->on('instruksi_kerjas')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            
            $table->bigInteger('id_pemrosesan')->unsigned();
            $table->foreign('id_pemrosesan')
            ->references('id')
            ->on('pemrosesans')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemrosesan_iks');
    }
}
