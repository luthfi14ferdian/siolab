<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemrosesanPengujiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemrosesan_pengujians', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_project')->unsigned();
            $table->foreign('id_project')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_subcontractor')->unsigned();
            $table->foreign('id_subcontractor')
            ->references('id')
            ->on('subcontractor_biocomps')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_pengujian')->unsigned();
            $table->foreign('id_pengujian')
            ->references('id')
            ->on('daftar_pengujians')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->date('tanggal');
            $table->string('nomor_batch',128);

            $table->integer('jumlah_spesimen');
            $table->integer('jumlah_akhir_spesimen')->nullable();

            $table->integer('note_akhir')->nullable();
            $table->date('tanggal_selesai')->nullable();

            $table->boolean('is_finish')->nullable();

            $table->bigInteger('id_produk')->unsigned();
            $table->foreign('id_produk')
            ->references('id')
            ->on('produk')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('status',128);

            $table->bigInteger('diproses_oleh')->unsigned()->nullable();
            $table->foreign('diproses_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('diperiksa_oleh')->unsigned()->nullable();
            $table->foreign('diperiksa_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('disetujui_oleh')->unsigned()->nullable();
            $table->foreign('disetujui_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemrosesan_pengujians');
    }
}
