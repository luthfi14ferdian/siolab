<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_batch', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_project')->unsigned();
            $table->foreign('id_project')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('no_batch',128);
            $table->string('status',128);
            $table->integer('jumlah_laporan')->nullable();
            $table->integer('kriteria_sampel_diterima');
            $table->integer('kriteria_sampel_ditolak');
            $table->integer('jumlah_sampel');
            $table->integer('sampel_diterima');
            $table->integer('sampel_ditolak');
            $table->string('judgement',128)->nullable();
            $table->string('note_judgement',128)->nullable();

            $table->bigInteger('diperiksa_oleh')->unsigned()->nullable();
            $table->foreign('diperiksa_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('disetujui_oleh')->unsigned()->nullable();
            $table->foreign('disetujui_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->longText('alasan_penolakan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_batch');
    }
}
