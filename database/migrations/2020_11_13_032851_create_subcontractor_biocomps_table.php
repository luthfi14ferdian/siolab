<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcontractorBiocompsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcontractor_biocomps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_project')->unsigned();
            $table->foreign('id_project')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('no',128)->nullable();
            $table->date('tanggal_seleksi');
            $table->string('nama_pemasok',128);
            $table->longText('alamat');
            $table->string('kontak',128);
            $table->string('email',128);
            $table->string('jenis_jasa',128);
            $table->string('pelayanan',128)->nullable();
            $table->string('kesimpulan',128)->nullable();
            $table->integer('total_penilaian')->nullable();
            $table->string('status',128)->nullable();
            $table->string('penilaian_finish',128)->nullable();

            $table->bigInteger('disusun_oleh')->unsigned()->nullable();
            $table->foreign('disusun_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('disetujui_oleh')->unsigned()->nullable();
            $table->foreign('disetujui_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcontractor_biocomps');
    }
}
