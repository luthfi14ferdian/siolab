<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotaAntarDivisisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota_antar_divisis', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_project')->unsigned();
            $table->foreign('id_project')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string("nomor_WO",128);
            $table->string("nomor_NAD",128);
            $table->string("nomor_batch",128);
            $table->string("status",128);
            $table->date("tanggal");

            $table->string('kode_produk',128);
            $table->foreign('kode_produk')
            ->references('kode_produk')
            ->on('produk')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('nama_barang',128);

            $table->bigInteger('divisi_yang_mengajukan')->unsigned();
            $table->foreign('divisi_yang_mengajukan')
            ->references('id')
            ->on('divisi')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('divisi_yang_diajukan')->unsigned();
            $table->foreign('divisi_yang_diajukan')
            ->references('id')
            ->on('divisi')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string("satuan",128);
            $table->integer("kuantitas");
            $table->longText("keterangan")->nullable();

            $table->bigInteger('dibuat_oleh')->unsigned();
            $table->foreign('dibuat_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('diketahui_oleh')->unsigned()->nullable();
            $table->foreign('diketahui_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('disetujui_oleh')->unsigned()->nullable();
            $table->foreign('disetujui_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nota_antar_divisis');
    }
}
