<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporanBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_batch', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_batch')->unsigned();
            $table->foreign('id_batch')
            ->references('id')
            ->on('data_batch')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('nomor_dokumen',128);
            $table->string('judgement',128)->nullable();
            $table->string('status',128);
            $table->integer('finished_tahapan')->nullable();

            $table->bigInteger('divisi_id')->unsigned();
            $table->foreign('divisi_id')
            ->references('id')
            ->on('divisi')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('operator')->unsigned();
            $table->foreign('operator')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->date('tanggal_pengerjaan');
            $table->longText('alamat');
            $table->string('alat_ukur',128);
            $table->string('spesimen',128);

            $table->bigInteger('id_produk')->unsigned();
            $table->foreign('id_produk')
            ->references('id')
            ->on('produk')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('metode_pengukuran',128);

            $table->bigInteger('diperiksa_oleh')->unsigned()->nullable();
            $table->foreign('diperiksa_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('disetujui_oleh')->unsigned()->nullable();
            $table->foreign('disetujui_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_batch');
    }
}
