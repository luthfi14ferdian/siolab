<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePercobaanGeometrisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('percobaan_geometri', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_geometri_item')->unsigned();
            $table->foreign('id_geometri_item')
            ->references('id')
            ->on('laporan_geometri')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->integer('percobaan');
            $table->float('ukuran');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('percobaan_geometri');
    }
}
