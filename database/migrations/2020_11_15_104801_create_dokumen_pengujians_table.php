<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDokumenPengujiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_pengujians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pengujian')->unsigned();
            $table->foreign('id_pengujian')
            ->references('id')
            ->on('daftar_pengujians')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('nomor_dokumen',128);
            $table->longText('keterangan',128);
            $table->string('nama_file',128);
            $table->longtext('dokumen_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen_pengujians');
    }
}
