<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesainDevPlanningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desain_dev_plannings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_proyek',128);
            $table->string('nomor_dokumen',128);
            $table->date('tgl_pengerjaan');

            $table->bigInteger('id_proyek')->unsigned();
            $table->foreign('id_proyek')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('disiapkan_oleh')->unsigned();
            $table->foreign('disiapkan_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('diperiksa_oleh')->unsigned()->nullable();
            $table->foreign('diperiksa_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            
            $table->string('status',16);
            $table->string('dokumen',255);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desain_dev_plannings');
    }
}
