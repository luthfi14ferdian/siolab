<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('nama_project')->unsigned();
            $table->foreign('nama_project')
            ->references('id')
            ->on('jenis_projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('tahapan',128);
            $table->string('nomor_batch_awal',128);
            $table->string('current_nomor_batch')->nullable();

            $table->bigInteger('divisi_saat_ini')->unsigned();
            $table->foreign('divisi_saat_ini')
            ->references('id')
            ->on('divisi')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('project_leader')->unsigned();
            $table->foreign('project_leader')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('pic_desain')->unsigned();
            $table->foreign('pic_desain')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('pic_material')->unsigned();
            $table->foreign('pic_material')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('pic_manufaktur')->unsigned();
            $table->foreign('pic_manufaktur')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('pic_surface_treatment')->unsigned();
            $table->foreign('pic_surface_treatment')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('pic_metrologi')->unsigned();
            $table->foreign('pic_metrologi')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('pic_biomech')->unsigned();
            $table->foreign('pic_biomech')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('pic_biocomp')->unsigned();
            $table->foreign('pic_biocomp')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
