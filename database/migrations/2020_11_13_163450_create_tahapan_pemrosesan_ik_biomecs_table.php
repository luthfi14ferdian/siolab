<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTahapanPemrosesanIkBiomecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahapan_pemrosesan_ik_biomecs', function (Blueprint $table) {
            $table->bigInteger('id_tahapan')->unsigned();
            $table->foreign('id_tahapan')
            ->references('id')
            ->on('tahapan_ik_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_pemrosesan')->unsigned();
            $table->foreign('id_pemrosesan')
            ->references('id')
            ->on('pemrosesan_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('status',128);
            $table->string('note',128);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahapan_pemrosesan_ik_biomecs');
    }
}
