<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTahapanIksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahapan_iks', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_ik')->unsigned();
            $table->foreign('id_ik')
            ->references('id')
            ->on('instruksi_kerjas')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('uraian',128);
            $table->Integer('urutan');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahapan_iks');
    }
}
