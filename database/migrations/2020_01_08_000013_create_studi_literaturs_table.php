<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudiLiteratursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studi_literaturs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("no_batch",128);
            $table->string("no_dokumen",128);
            $table->string("judul_literatur",128);
            $table->string("link_url_source",255);
            $table->string("dok_studi_literatur",255);
            $table->bigInteger('id_proyek')->unsigned();
            $table->foreign('id_proyek')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studi_literaturs');
    }
}
