<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermintaanMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_materials', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('kode_permintaan',128);
            $table->bigInteger('nama_item')->unsigned();
            $table->foreign('nama_item')
            ->references('id')
            ->on('materials')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('no_batch',128);
            $table->string('satuan',128);
            $table->string('status',64);
            $table->integer('kuantitas');
            $table->date('tanggal');
            $table->bigInteger('diminta_oleh')->unsigned();
            $table->foreign('diminta_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('diterima_oleh')->unsigned()->nullable();
            $table->foreign('diterima_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('diserahkan_oleh')->unsigned()->nullable();
            $table->foreign('diserahkan_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_materials');
    }
}
