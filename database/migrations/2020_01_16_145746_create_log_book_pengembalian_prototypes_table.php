<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogBookPengembalianPrototypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_book_pengembalian_prototypes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->date('tanggal');
            $table->string('batch');


            $table->bigInteger('item')->unsigned();
            $table->foreign('item')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->integer('jumlah');
            $table->string('keterangan')->nullable();

            $table->bigInteger('divisi_melapor')->unsigned();
            $table->foreign('divisi_melapor')
            ->references('id')
            ->on('divisi')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('divisi_terlapor')->unsigned();
            $table->foreign('divisi_terlapor')
            ->references('id')
            ->on('divisi')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('yang_menyerahkan')->unsigned();
            $table->foreign('yang_menyerahkan')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('yang_menerima')->unsigned()->nullable();
            $table->foreign('yang_menerima')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('status_pengembalian',16);

            //harusnya ada dokumen car

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_book_pengembalian_prototypes');
    }
}
