<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDokumenCarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_car', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_pengembalian')->unsigned();
            $table->foreign('id_pengembalian')
            ->references('id')
            ->on('log_book_pengembalian_prototypes')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('nama_project')->unsigned();
            $table->foreign('nama_project')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('jenis');

            $table->bigInteger('divisi')->unsigned();
            $table->foreign('divisi')
            ->references('id')
            ->on('divisi')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('standar');
            $table->string('nomor');
            $table->date('tanggal');
            $table->string('status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen_car');
    }
}
