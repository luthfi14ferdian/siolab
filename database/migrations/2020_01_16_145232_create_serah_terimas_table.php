<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSerahTerimasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serah_terimas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_NAD')->unsigned();
            $table->foreign('id_NAD')
            ->references('id')
            ->on('nota_antar_divisis')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('item')->unsigned();
            $table->foreign('item')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->date('tanggal');

            $table->string('no_batch',128);
            $table->integer('jumlah');
            $table->string('tujuan',128);
            $table->string('status', 128);
            $table->text('keterangan')->nullable();

            $table->bigInteger('yang_menyerahkan')->unsigned();
            $table->foreign('yang_menyerahkan')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('yang_menerima')->unsigned()->nullable();
            $table->foreign('yang_menerima')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('serah_terimas');
    }
}
