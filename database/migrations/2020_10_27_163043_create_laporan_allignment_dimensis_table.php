<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporanAllignmentDimensisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_allignment_dimensi', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_laporan')->unsigned();
            $table->foreign('id_laporan')
            ->references('id')
            ->on('laporan_batch')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('nama_file',128);
            $table->longtext('gambar_path');
            $table->string('tipe',128);
            $table->string('keterangan',128);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_allignment_dimensi');
    }
}
