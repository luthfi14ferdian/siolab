<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_project')->unsigned();
            $table->foreign('id_project')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_user')->unsigned();
            $table->foreign('id_user')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_role')->unsigned();
            $table->foreign('id_role')
            ->references('id')
            ->on('role')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_projects');
    }
}
