<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlatTahapanIkBiomecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alat_tahapan_ik_biomecs', function (Blueprint $table) {
            $table->bigInteger('id_tahapan')->unsigned();
            $table->foreign('id_tahapan')
            ->references('id')
            ->on('tahapan_ik_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_alat')->unsigned();
            $table->foreign('id_alat')
            ->references('id')
            ->on('alat_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alat_tahapan_ik_biomecs');
    }
}
