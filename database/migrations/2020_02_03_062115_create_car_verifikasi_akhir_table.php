<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarVerifikasiAkhirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_verifikasi_akhir', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_car')->unsigned();
            $table->string('tindak_lanjut');

            $table->bigInteger('pic')->unsigned()->nullable();
            $table->foreign('pic')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->date('batas_waktu');

            $table->bigInteger('manager_representative')->unsigned()->nullable();
            $table->foreign('manager_representative')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_verifikasi_akhir');
    }
}
