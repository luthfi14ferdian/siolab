<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarKoreksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_koreksi', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_car')->unsigned();
            $table->text('analisis_penyebab');
            $table->text('tindakan_koreksi');

            $table->bigInteger('pic')->unsigned()->nullable();
            $table->foreign('pic')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->text('tindakan_korektif')->nullable();
            $table->date('batas_waktu')->nullable();

            $table->bigInteger('auditor')->unsigned()->nullable();
            $table->foreign('auditor')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('auditee')->unsigned()->nullable();
            $table->foreign('auditee')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('manager_representative')->unsigned()->nullable();
            $table->foreign('manager_representative')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->text('note')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_koreksi');
    }
}
