<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemrosesanAlatBiomecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemrosesan_alat_biomecs', function (Blueprint $table) {
            $table->bigInteger('id_pemrosesan')->unsigned();
            $table->foreign('id_pemrosesan')
            ->references('id')
            ->on('pemrosesan_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('tujuan',128);
            $table->string('judul',128);

            $table->bigInteger('diproses_oleh')->unsigned()->nullable();
            $table->foreign('diproses_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->Integer('jumlah_sisa')->nullable()->default(null);
            $table->datetime("tanggal_selesai")->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemrosesan_alat_biomecs');
    }
}
