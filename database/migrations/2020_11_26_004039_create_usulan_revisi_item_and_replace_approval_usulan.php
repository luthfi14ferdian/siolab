<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsulanRevisiItemAndReplaceApprovalUsulan extends Migration
{
    public function up()
    {
        // Add UsulanRevisiItems
        Schema::create('usulan_revisi_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->date('tanggal');
            $table->string('usulan_revisi', 128);
            $table->string('alasan', 128);

            $table->bigInteger('pemberi_masukan')->unsigned();
            $table->foreign('pemberi_masukan')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->bigInteger('id_usulan_revisi')->unsigned();
            $table->foreign('id_usulan_revisi')
                ->references('id')
                ->on('usulan_revisis')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // Replace approval with disetujui_oleh
        Schema::table('usulan_revisis', function (Blueprint $table) {
            $table->dropColumn('approval');
            $table->string("status", 128);

            $table->bigInteger('disetujui_oleh')->unsigned()->nullable();
            $table->foreign('disetujui_oleh')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        // Replace disetujui_oleh with approval
        Schema::table('usulan_revisis', function (Blueprint $table) {
            $table->dropForeign('usulan_revisis_disetujui_oleh_foreign');
            $table->dropColumn('disetujui_oleh');

            $table->dropColumn("status");
            $table->string('approval', 64);
        });

        // Remove UsulanRevisiItems
        Schema::dropIfExists('usulan_revisi_items');
    }
}
