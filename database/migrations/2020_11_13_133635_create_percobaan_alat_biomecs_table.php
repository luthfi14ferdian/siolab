<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePercobaanAlatBiomecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('percobaan_alat_biomecs', function (Blueprint $table) {
            $table->bigInteger('id_pemrosesan')->unsigned();
            $table->foreign('id_pemrosesan')
            ->references('id')
            ->on('pemrosesan_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('id_alat')->unsigned();
            $table->foreign('id_alat')
            ->references('id')
            ->on('alat_biomecs')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('note',128);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('percobaan_alat_biomecs');
    }
}
