<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTahapanPemrosesanIksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahapan_pemrosesan_iks', function (Blueprint $table) {
            $table->bigInteger('id_tahapan')->unsigned();
            $table->foreign('id_tahapan')
            ->references('id')
            ->on('tahapan_iks')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            
            $table->bigInteger('id_pemrosesan')->unsigned();
            $table->foreign('id_pemrosesan')
            ->references('id')
            ->on('pemrosesans')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('status',128);
            $table->string('note',128);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahapan_pemrosesan_iks');
    }
}
