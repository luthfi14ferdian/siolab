<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTahapanPemrosesanBiocompsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahapan_pemrosesan_biocomps', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_pemrosesan_pengujian')->unsigned();
            $table->foreign('id_pemrosesan_pengujian')
            ->references('id')
            ->on('pemrosesan_pengujians')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->longText('uraian');
            $table->string('nama_file',128)->nullable();
            $table->longtext('dokumen_path')->nullable();

            $table->string('judgement',128)->nullable();
            $table->longText('note')->nullable();

            $table->bigInteger('diproses_oleh')->unsigned()->nullable();
            $table->foreign('diproses_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('diperiksa_oleh')->unsigned()->nullable();
            $table->foreign('diperiksa_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('disetujui_oleh')->unsigned()->nullable();
            $table->foreign('disetujui_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('status',128);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahapan_pemrosesan_biocomps');
    }
}
