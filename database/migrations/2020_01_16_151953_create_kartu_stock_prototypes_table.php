<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKartuStockPrototypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kartu_stock_prototypes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('nama_project')->unsigned();
            $table->foreign('nama_project')
            ->references('id')
            ->on('projects')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('dikerjakan_oleh')->unsigned();
            $table->foreign('dikerjakan_oleh')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->string('kategori_prototype',128);
            $table->string('keterangan',128);
            $table->integer('keluar');
            $table->integer('masuk');
            $table->string('no_batch',128);
            $table->string('jenis_prototype',128);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kartu_stock_prototypes');
    }
}
