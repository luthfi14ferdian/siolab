<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarVerifikasiKoreksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_verifikasi_koreksi', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('id_car')->unsigned();
            $table->string('efektivitas');
            $table->text('realisasi')->nullable();

            $table->bigInteger('auditor')->unsigned()->nullable();
            $table->foreign('auditor')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->bigInteger('manager_representative')->unsigned()->nullable();
            $table->foreign('manager_representative')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_verifikasi_koreksi');
    }
}
