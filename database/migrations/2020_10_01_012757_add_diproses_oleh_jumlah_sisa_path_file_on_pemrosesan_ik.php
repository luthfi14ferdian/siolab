<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiprosesOlehJumlahSisaPathFileOnPemrosesanIk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pemrosesan_iks', function (Blueprint $table) {
            $table->bigInteger('diproses_oleh')->unsigned()->nullable();
            $table->foreign('diproses_oleh')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->Integer('jumlah_sisa')->nullable()->default(null);
            $table->string('path_file_hasil', 4096)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pemrosesan_iks', function (Blueprint $table) {
            $table->dropForeign('pemrosesan_iks_diproses_oleh_foreign');
            $table->dropColumn('diproses_oleh');
            $table->dropColumn('jumlah_sisa');
            $table->dropColumn('path_file_hasil');
        });
    }
}
