# SILAB RCBE UI Backend

## Develop locally

These are default setup to be set on `.env` file.
Please adjust your DB_USERNAME and DB_PASSWORD accordingly.

### Default setup in .env (for MySQL/MariaDB):

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=
DB_PASSWORD=
```

### Default setup in .env (for PostgreSQL):

```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=laravel
DB_USERNAME=
DB_PASSWORD=
```
