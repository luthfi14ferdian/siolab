<!DOCTYPE html>
<html>
<head>
	<title>Nota Antar Divisi</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Nota Antar Divisi</h4>
	</center>

	<table class='table table-bordered'>
		<tbody>
			<tr>
                <td>id</td>
                <td>{{$nota_antar_divisi[0]->id}}</td>
            </tr>
            <tr>
                <td>nomor WO</td>
                <td>{{$nota_antar_divisi[0]->nomor_WO}}</td>
            </tr>
            <tr>
                <td>nomor NAD</td>
                <td>{{$nota_antar_divisi[0]->nomor_NAD}}</td>
            </tr>
            <tr>
                <td>nomor Batch</td>
                <td>{{$nota_antar_divisi[0]->nomor_batch}}</td>
            </tr>
            <tr>
                <td>status</td>
                <td>{{$nota_antar_divisi[0]->status}}</td>
            </tr>
            <tr>
                <td>tanggal</td>
                <td>{{$nota_antar_divisi[0]->tanggal}}</td>
            </tr>
            <tr>
                <td>Divisi yang Mengajukan</td>
                <td>{{$nota_antar_divisi[0]->divisi_yang_mengajukan}}</td>
            </tr>
            <tr>
                <td>Divisi yang Diajukan</td>
                <td>{{$nota_antar_divisi[0]->divisi_yang_diajukan}}</td>
            </tr>
            <tr>
                <td>Nama Barang</td>
                <td>Nama Project: {{$project[0]->nama_project}}<br>
                Project Leader: {{$project[0]->project_leader}}<br>
                PIC Desain: {{$project[0]->pic_desain}}<br>
                PIC Material: {{$project[0]->pic_material}}<br>
                PIC Manufaktur: {{$project[0]->pic_manufaktur}}</td>
            </tr>
            <tr>
                <td>Satuan</td>
                <td>{{$nota_antar_divisi[0]->satuan}}</td>
            </tr>
            <tr>
                <td>Kuantitas</td>
                <td>{{$nota_antar_divisi[0]->kuantitas}}</td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>{{$nota_antar_divisi[0]->keterangan}}</td>
            </tr>
            <tr>
                <td>Dibuat Oleh</td>
                <td>{{$dibuat_oleh->first_name}} {{$dibuat_oleh->last_name}}</td>
            </tr>
            <tr>
                <td>Diketahui Oleh</td>
                <td>{{$diketahui_oleh->first_name}} {{$diketahui_oleh->last_name}}</td>
            </tr>
            <tr>
                <td>Disetujui Oleh</td>
                <td>{{$disetujui_oleh->first_name}} {{$disetujui_oleh->last_name}}</td>
            </tr>
            <tr>
                <td>Dokumen 3d Fit</td>
                <td>TBA</td>
            </tr>
            <tr>
                <td>Dokumen Drawing</td>
                <td>TBA</td>
            </tr>
            <tr>
                <td>Dokumen Spesifikasi Desing</td>
                <td>TBA</td>
            </tr>
		</tbody>
	</table>

</body>
</html>
