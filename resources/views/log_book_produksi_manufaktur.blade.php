<!DOCTYPE html>
<html>
<head>
	<title>Logbook Produksi Manufaktur</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Logbook Produksi Manufaktur</h4>
	</center>

	<table class='table table-bordered'>
		<tbody>
			<tr>
                <td>id</td>
                <td>{{$serah_terima->id}}</td>
            </tr>
            <tr>
                <td>id NAD</td>
                <td>{{$serah_terima->id_NAD}}</td>
            </tr>
            <tr>
                <td>Nama Barang</td>
                <td>Nama Project: {{$project[0]->nama_project}}<br>
                Project Leader: {{$project[0]->project_leader}}<br>
                PIC Desain: {{$project[0]->pic_desain}}<br>
                PIC Material: {{$project[0]->pic_material}}<br>
                PIC Manufaktur: {{$project[0]->pic_manufaktur}}</td>
            </tr>
            <tr>
                <td>tanggal</td>
                <td>{{$serah_terima->tanggal}}</td>
            </tr>
            <tr>
                <td>Nomor Batch</td>
                <td>{{$serah_terima->no_batch}}</td>
            </tr>
            <tr>
                <td>tujuan</td>
                <td>{{$serah_terima->tujuan}}</td>
            </tr>
            <tr>
                <td>status</td>
                <td>{{$serah_terima->status}}</td>
            </tr>
            <tr>
                <td>keterangan</td>
                <td>{{$serah_terima->keterangan}}</td>
            </tr>
            <tr>
                <td>Yang Menyerahkan</td>
                <td>{{$yang_menyerahkan->first_name}} {{$yang_menyerahkan->last_name}}</td>
            </tr>
            <tr>
                <td>Yang Menerima</td>
                <td>TBA</td>
            </tr>
		</tbody>
	</table>

</body>
</html>
